-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2019 at 12:18 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `monday`
--

-- --------------------------------------------------------

--
-- Table structure for table `--plans-columns`
--

CREATE TABLE `--plans-columns` (
  `id` int(10) UNSIGNED NOT NULL,
  `plans` int(11) NOT NULL,
  `columns` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `--plans-columns`
--

INSERT INTO `--plans-columns` (`id`, `plans`, `columns`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, NULL),
(2, 1, 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@b-mates.com', '$2y$10$zyDvyFAUp/ugs1XPPsgVAO0gPa1IYOslAV5d.412qFaKK3aDWyIZG', 'hJd9czgMbFuVI57JATLjr4UxwSw8fol3LZUXjvIYv94G13GqoRKtWRZmjRYI', '2019-01-23 21:10:35', '2019-01-23 21:10:35');

-- --------------------------------------------------------

--
-- Table structure for table `boardcolumns`
--

CREATE TABLE `boardcolumns` (
  `id` int(11) NOT NULL,
  `columnid` int(11) NOT NULL,
  `columnvalue` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `rowindex` int(11) NOT NULL,
  `boardid` int(11) NOT NULL,
  `account` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `boardcolumns`
--

INSERT INTO `boardcolumns` (`id`, `columnid`, `columnvalue`, `rowindex`, `boardid`, `account`, `status`, `created_at`, `updated_at`) VALUES
(109, 127, 'Row 1', 0, 116, 28, 1, '2019-01-24 23:01:33', '2019-01-24 23:01:33'),
(110, 128, 'Owner', 0, 116, 28, 1, '2019-01-24 23:01:33', '2019-01-24 23:01:33'),
(111, 127, 'Row 2', 1, 116, 28, 1, '2019-01-24 23:01:33', '2019-01-24 23:01:33'),
(112, 128, 'Owner', 1, 116, 28, 1, '2019-01-24 23:01:33', '2019-01-24 23:01:33'),
(113, 129, 'Row 1', 0, 116, 28, 1, '2019-01-24 23:01:34', '2019-01-24 23:01:34'),
(114, 130, 'Owner', 0, 116, 28, 1, '2019-01-24 23:01:34', '2019-01-24 23:01:34'),
(115, 129, 'Row 2', 1, 116, 28, 1, '2019-01-24 23:01:34', '2019-01-24 23:01:34'),
(116, 130, 'Owner', 1, 116, 28, 1, '2019-01-24 23:01:34', '2019-01-24 23:01:34'),
(117, 127, 'Row 3', 2, 116, 28, 1, '2019-01-24 23:01:33', '2019-01-24 23:01:33'),
(118, 128, 'Owner 3', 2, 116, 28, 1, '2019-01-24 23:01:33', '2019-01-24 23:01:33'),
(119, 131, 'Row 1', 0, 117, 28, 1, '2019-01-31 03:38:23', '2019-01-31 03:38:23'),
(120, 132, 'Owner', 0, 117, 28, 1, '2019-01-31 03:38:23', '2019-01-31 03:38:23'),
(121, 131, 'Row 2', 1, 117, 28, 1, '2019-01-31 03:38:23', '2019-01-31 03:38:23'),
(122, 132, 'Owner', 1, 117, 28, 1, '2019-01-31 03:38:24', '2019-01-31 03:38:24'),
(123, 133, 'Row 1', 0, 117, 28, 1, '2019-01-31 03:38:24', '2019-01-31 03:38:24'),
(124, 134, 'Owner', 0, 117, 28, 1, '2019-01-31 03:38:24', '2019-01-31 03:38:24'),
(125, 133, 'Row 2', 1, 117, 28, 1, '2019-01-31 03:38:24', '2019-01-31 03:38:24'),
(126, 134, 'Owner', 1, 117, 28, 1, '2019-01-31 03:38:24', '2019-01-31 03:38:24'),
(127, 135, 'Row 1', 0, 118, 28, 1, '2019-01-31 03:45:05', '2019-01-31 03:45:05'),
(128, 136, 'Owner', 0, 118, 28, 1, '2019-01-31 03:45:05', '2019-01-31 03:45:05'),
(129, 135, 'Row 2', 1, 118, 28, 1, '2019-01-31 03:45:05', '2019-01-31 03:45:05'),
(130, 136, 'Owner', 1, 118, 28, 1, '2019-01-31 03:45:05', '2019-01-31 03:45:05'),
(131, 137, 'Row 1', 0, 118, 28, 1, '2019-01-31 03:45:05', '2019-01-31 03:45:05'),
(132, 138, 'Owner', 0, 118, 28, 1, '2019-01-31 03:45:05', '2019-01-31 03:45:05'),
(133, 137, 'Row 2', 1, 118, 28, 1, '2019-01-31 03:45:05', '2019-01-31 03:45:05'),
(134, 138, 'Owner', 1, 118, 28, 1, '2019-01-31 03:45:05', '2019-01-31 03:45:05'),
(135, 139, 'another Row 1', 0, 119, 28, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(136, 140, 'another Owner', 0, 119, 28, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(137, 139, 'another Row 2', 1, 119, 28, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(138, 140, 'another Owner', 1, 119, 28, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(139, 141, 'another Row 1', 0, 119, 28, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(140, 142, 'another Owner', 0, 119, 28, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(141, 141, 'another Row 2', 1, 119, 28, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(142, 142, 'another Owner', 1, 119, 28, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(143, 143, 'Row 1', 0, 120, 28, 1, '2019-01-31 05:02:34', '2019-01-31 05:02:34'),
(144, 144, 'Owner', 0, 120, 28, 1, '2019-01-31 05:02:35', '2019-01-31 05:02:35'),
(145, 143, 'Row 2', 1, 120, 28, 1, '2019-01-31 05:02:35', '2019-01-31 05:02:35'),
(146, 144, 'Owner', 1, 120, 28, 1, '2019-01-31 05:02:35', '2019-01-31 05:02:35'),
(147, 145, 'Row 1', 0, 120, 28, 1, '2019-01-31 05:02:35', '2019-01-31 05:02:35'),
(148, 146, 'Owner', 0, 120, 28, 1, '2019-01-31 05:02:35', '2019-01-31 05:02:35'),
(149, 145, 'Row 2', 1, 120, 28, 1, '2019-01-31 05:02:35', '2019-01-31 05:02:35'),
(150, 146, 'Owner', 1, 120, 28, 1, '2019-01-31 05:02:35', '2019-01-31 05:02:35'),
(151, 147, 'Row 1', 0, 121, 28, 1, '2019-01-31 07:08:33', '2019-01-31 07:08:33'),
(152, 148, 'Owner', 0, 121, 28, 1, '2019-01-31 07:08:33', '2019-01-31 07:08:33'),
(153, 147, 'Row 2', 1, 121, 28, 1, '2019-01-31 07:08:33', '2019-01-31 07:08:33'),
(154, 148, 'Owner', 1, 121, 28, 1, '2019-01-31 07:08:33', '2019-01-31 07:08:33'),
(155, 149, 'Row 1', 0, 121, 28, 1, '2019-01-31 07:08:33', '2019-01-31 07:08:33'),
(156, 150, 'Owner', 0, 121, 28, 1, '2019-01-31 07:08:33', '2019-01-31 07:08:33'),
(157, 149, 'Row 2', 1, 121, 28, 1, '2019-01-31 07:08:33', '2019-01-31 07:08:33'),
(158, 150, 'Owner', 1, 121, 28, 1, '2019-01-31 07:08:33', '2019-01-31 07:08:33');

-- --------------------------------------------------------

--
-- Table structure for table `boardgroupcolumns`
--

CREATE TABLE `boardgroupcolumns` (
  `id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `columntype` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `colindex` int(11) NOT NULL,
  `groupid` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `boardgroupcolumns`
--

INSERT INTO `boardgroupcolumns` (`id`, `title`, `columntype`, `colindex`, `groupid`, `status`, `created_at`, `updated_at`) VALUES
(127, 'Open Projects', 'longtext', 0, 110, 1, '2019-01-24 23:01:33', '2019-01-24 23:01:33'),
(128, 'Owner', 'longtext', 1, 110, 1, '2019-01-24 23:01:33', '2019-01-24 23:01:33'),
(129, 'Closed Projects', 'longtext', 0, 111, 1, '2019-01-24 23:01:33', '2019-01-24 23:01:33'),
(130, 'Owner', 'longtext', 1, 111, 1, '2019-01-24 23:01:34', '2019-01-24 23:01:34'),
(131, 'Open Projects', 'longtext', 0, 112, 1, '2019-01-31 03:38:23', '2019-01-31 03:38:23'),
(132, 'Owner', 'longtext', 1, 112, 1, '2019-01-31 03:38:23', '2019-01-31 03:38:23'),
(133, 'Closed Projects', 'longtext', 0, 113, 1, '2019-01-31 03:38:24', '2019-01-31 03:38:24'),
(134, 'Owner', 'longtext', 1, 113, 1, '2019-01-31 03:38:24', '2019-01-31 03:38:24'),
(135, ' IT Open Projects', 'longtext', 0, 114, 1, '2019-01-31 03:45:05', '2019-01-31 03:45:05'),
(136, 'Owner', 'longtext', 1, 114, 1, '2019-01-31 03:45:05', '2019-01-31 03:45:05'),
(137, ' IT Closed Projects', 'longtext', 0, 115, 1, '2019-01-31 03:45:05', '2019-01-31 03:45:05'),
(138, 'Owner', 'longtext', 1, 115, 1, '2019-01-31 03:45:05', '2019-01-31 03:45:05'),
(139, 'Open Projects', 'longtext', 0, 116, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(140, 'Owner', 'longtext', 1, 116, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(141, 'Closed Projects', 'longtext', 0, 117, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(142, 'Owner', 'longtext', 1, 117, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(143, 'Open Projects', 'longtext', 0, 118, 1, '2019-01-31 05:02:34', '2019-01-31 05:02:34'),
(144, 'Owner', 'longtext', 1, 118, 1, '2019-01-31 05:02:34', '2019-01-31 05:02:34'),
(145, 'Closed Projects', 'longtext', 0, 119, 1, '2019-01-31 05:02:35', '2019-01-31 05:02:35'),
(146, 'Owner', 'longtext', 1, 119, 1, '2019-01-31 05:02:35', '2019-01-31 05:02:35'),
(147, 'Open Projects', 'longtext', 0, 120, 1, '2019-01-31 07:08:32', '2019-01-31 07:08:32'),
(148, 'Owner', 'longtext', 1, 120, 1, '2019-01-31 07:08:33', '2019-01-31 07:08:33'),
(149, 'Closed Projects', 'longtext', 0, 121, 1, '2019-01-31 07:08:33', '2019-01-31 07:08:33'),
(150, 'Owner', 'longtext', 1, 121, 1, '2019-01-31 07:08:33', '2019-01-31 07:08:33');

-- --------------------------------------------------------

--
-- Table structure for table `boardgroups`
--

CREATE TABLE `boardgroups` (
  `id` int(11) NOT NULL,
  `grouptitle` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `boardid` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `boardgroups`
--

INSERT INTO `boardgroups` (`id`, `grouptitle`, `boardid`, `sequence`, `status`, `created_at`, `updated_at`) VALUES
(110, 'Open Projects', 116, 0, 1, '2019-01-24 23:01:33', '2019-01-24 23:01:33'),
(111, 'Closed Projects', 116, 1, 1, '2019-01-24 23:01:33', '2019-01-24 23:01:33'),
(112, 'Open Projects', 117, 0, 1, '2019-01-31 03:38:23', '2019-01-31 03:38:23'),
(113, 'Closed Projects', 117, 1, 1, '2019-01-31 03:38:24', '2019-01-31 03:38:24'),
(114, 'IT Open Projects', 118, 0, 1, '2019-01-31 03:45:04', '2019-01-31 03:45:04'),
(115, ' IT Closed Projects', 118, 1, 1, '2019-01-31 03:45:05', '2019-01-31 03:45:05'),
(116, 'Open Projects', 119, 0, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(117, 'Closed Projects', 119, 1, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(118, 'Open Projects', 120, 0, 1, '2019-01-31 05:02:34', '2019-01-31 05:02:34'),
(119, 'Closed Projects', 120, 1, 1, '2019-01-31 05:02:35', '2019-01-31 05:02:35'),
(120, 'Open Projects', 121, 0, 1, '2019-01-31 07:08:32', '2019-01-31 07:08:32'),
(121, 'Closed Projects', 121, 1, 1, '2019-01-31 07:08:33', '2019-01-31 07:08:33');

-- --------------------------------------------------------

--
-- Table structure for table `boards`
--

CREATE TABLE `boards` (
  `id` int(11) NOT NULL,
  `boardname` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `boarddescription` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyid` int(11) NOT NULL,
  `folder` int(11) NOT NULL DEFAULT '0',
  `boardtype` int(11) NOT NULL DEFAULT '0',
  `createdby` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `boards`
--

INSERT INTO `boards` (`id`, `boardname`, `boarddescription`, `companyid`, `folder`, `boardtype`, `createdby`, `status`, `created_at`, `updated_at`) VALUES
(116, 'T5', 'T5', 28, 0, 1, 34, 1, '2019-01-24 23:01:33', '2019-01-24 23:01:33'),
(117, 'Checking new template', 'Checking new template', 28, 0, 1, 34, 1, '2019-01-31 03:38:23', '2019-01-31 03:38:23'),
(118, 'New IT', 'New IT', 28, 0, 1, 34, 1, '2019-01-31 03:45:04', '2019-01-31 03:45:04'),
(119, 'Another try', 'Another try', 28, 0, 1, 34, 1, '2019-01-31 04:26:23', '2019-01-31 04:26:23'),
(120, 'Test', 'Test', 28, 0, 1, 34, 1, '2019-01-31 05:02:34', '2019-01-31 05:02:34'),
(121, 'Johns hospitality', 'Johns hospitality', 28, 0, 1, 34, 1, '2019-01-31 07:08:32', '2019-01-31 07:08:32');

-- --------------------------------------------------------

--
-- Table structure for table `boardtypes`
--

CREATE TABLE `boardtypes` (
  `id` int(11) NOT NULL,
  `boardtypename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `boarddescription` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `boardtypes`
--

INSERT INTO `boardtypes` (`id`, `boardtypename`, `boarddescription`, `default`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Boards', 'Boards', 1, 1, '2019-01-17 07:24:34', '2019-01-21 11:53:35'),
(2, 'Private', 'Private', 0, 1, '2019-01-17 07:24:34', '2019-01-17 07:24:34'),
(3, 'Shareable', 'Shareable', 0, 1, '2019-01-17 07:24:54', '2019-01-17 07:24:54');

-- --------------------------------------------------------

--
-- Table structure for table `board_roles`
--

CREATE TABLE `board_roles` (
  `id` int(11) NOT NULL,
  `boardid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `userroles` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `board_roles`
--

INSERT INTO `board_roles` (`id`, `boardid`, `userid`, `userroles`, `status`, `created_at`, `updated_at`) VALUES
(96, 116, 34, 1, 1, '2019-01-24 23:01:33', '2019-01-24 23:01:33'),
(97, 117, 34, 1, 1, '2019-01-31 03:38:23', '2019-01-31 03:38:23'),
(98, 118, 34, 1, 1, '2019-01-31 03:45:04', '2019-01-31 03:45:04'),
(99, 119, 34, 1, 1, '2019-01-31 04:26:24', '2019-01-31 04:26:24'),
(100, 120, 34, 1, 1, '2019-01-31 05:02:34', '2019-01-31 05:02:34'),
(101, 121, 34, 1, 1, '2019-01-31 07:08:32', '2019-01-31 07:08:32');

-- --------------------------------------------------------

--
-- Table structure for table `columntypes`
--

CREATE TABLE `columntypes` (
  `id` int(11) NOT NULL,
  `columnname` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `displayname` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `columntypes`
--

INSERT INTO `columntypes` (`id`, `columnname`, `displayname`, `status`, `created_at`, `updated_at`) VALUES
(1, 'longtext', 'Long Text', 1, NULL, NULL),
(2, 'smalltext', 'Small Text', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `companyname` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `teamstrength` int(11) NOT NULL,
  `warea` int(11) NOT NULL,
  `currentplan` int(11) NOT NULL,
  `invitemember` int(11) NOT NULL DEFAULT '0',
  `logoimage` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `headerimage` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `createdby` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `companyname`, `description`, `teamstrength`, `warea`, `currentplan`, `invitemember`, `logoimage`, `headerimage`, `createdby`, `status`, `created_at`, `updated_at`) VALUES
(28, 'bmates1', 'bmates1', 2, 3, 1, 0, '0', '0', 34, 1, '2019-01-16 02:49:11', '2019-01-16 02:49:11');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_12_27_1121_create_plans_table', 1),
(2, '2018_12_27_1122_create_timezone', 1),
(3, '2018_12_28_1123_create_workarea_table', 1),
(4, '2018_12_28_1125_create_teamstrength_table', 1),
(5, '2018_12_28_1129_create_companies_table', 1),
(6, '2018_12_28_1130_create_users_table', 1),
(7, '2018_12_28_1131_create_password_resets_table', 1),
(8, '2018_12_28_1132_create_useractivitylog_table', 1),
(9, '2018_12_31_065753_create_userverification_table', 1),
(10, '2019_01_03_065615_create_teamindustries_table', 1),
(12, '2019_01_09_104328_add_reset_token_to_user', 2),
(13, '2019_01_09_115439_create_jobs_table', 3),
(14, '2019_01_09_115447_create_failed_jobs_table', 3),
(27, '2019_01_14_085547_create_teaminvitations_table', 4),
(38, '2019_01_16_043922_create_boardtypes_table', 5),
(39, '2019_01_16_112239_create_boards_table', 5),
(40, '2019_01_16_112240_create_userroles_table', 5),
(41, '2019_01_16_112241_create_board_owners_table', 5),
(44, '2019_01_21_093403_create_themecategories_table', 6),
(45, '2019_01_21_093404_create_themes_table', 6),
(46, '2019_01_21_094315_create_themegroups_table', 6),
(48, '2019_01_23_070400_create_boardgroups_table', 7),
(49, '2019_01_23_120538_create_boardgroupcolumns_table', 8),
(51, '2019_01_24_050414_boardcolumn', 9),
(52, '2019_01_24_080511_create_admin_table', 10),
(53, '2019_01_24_122706_create_sessions_table', 11),
(54, '2019_02_01_050711_create_columntypes_table', 12),
(55, '2019_02_01_051335_plantocolumnsrelation', 13);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('liyashereef@gmail.com', '$2y$10$Stqv3YeAo238ApihqKMB7enQHIKQwG0wfO/iFuVgei7xSLXaX0UV6', '2019-01-09 05:34:20');

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int(11) NOT NULL,
  `planname` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `noofusers` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `planname`, `noofusers`, `price`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Basic', 5, 0, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('2ebKMI62tKnHVoQSwyU2t91f6Qqv757nuWWGobfE', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 'ZXlKcGRpSTZJa05LVTFNclVEaEJOWFpYWVN0Wll5dDBjSGR6WEM5QlBUMGlMQ0oyWVd4MVpTSTZJa053WVZSaVRUVTBNWFZDV0RKMmJGSlJNSGxvTUd4MFZIVlBhbEppTkdwRWFWQkJRa3RxZEV0VVZuSTVZVzVFT0hsd01pdHdYQzlpWVdWY0wwRk9jMWg0T1V3NGJYQnFWVk5uU1hSd2QyVXpVbWd5Vld3MFJHOTVaVGhTUVhjNFJHeFllSG8wTm0xc1JYQlVablJQU3pSS1RtWmFZV2RyUldGTFJrdGNMelJzUTJ4cFpEUjNRMlprVVhKcWN6SlVWVGxqZUdKc01tOXhZVEUzZEhSdU4wWmNMM2xNWWxaeFdVNURSa2hjTDJWeU5ITmFRMmhwUkhkaGNtcGNMMDU2V25kTE9FSlRUR2hsWEM5bk5YWnJTbEYyYUU5NlNHZG5YQzlQZUZGT1NrZFBlWFY2VGxCWU1ISk9aazVwZGtnMVREZzNhbXBZZVZoMVVWd3ZkV0l6VDBVNFMxcFJRWG8wYm5WeVYyOU1TR2x3UVhCelRESmFkWEUyWTBKUE9GbFJQVDBpTENKdFlXTWlPaUkzWkRVd00yVm1Nell6TWpFd1pEazRZMlU1TUdVeU5qbG1NbU13TkdNNU5HSm1Nakk1TlRsaE16RTBPVEEzT1dSbU0yRTJaR00xWkRjd1pqZGtPV00zSW4wPQ==', 1548387863),
('bpLJpZ8m1WJzOkoNd4CMQ7NNm5g0svUrjtUUY0Qd', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 'ZXlKcGRpSTZJa0puV1RsYWIyd3pUSFJDV0VkSlhDOW9UVE0zTlRKQlBUMGlMQ0oyWVd4MVpTSTZJbnBtZWxnNFVFcHRkRTVVT0hsMFJWZzNLME5aTkRWa2VYTXdaSEZxVjNSS2RrbEVjM2hTWjFJNFdtSmhUMmxyY21rMWRERnFZbGRyYldkUVpVUlBiRUp1YWxkNlJreHNjVzFuZUVWeWNXNXNaM2RaZVVWT2RtUm5kRVJtVG5weWNWWkhRamt4UnpjelRVMXlPVlpUYWxwM2VGazBUVEIxVkUxYVEwMWpTRnBHV0ZVMFNVMVFiems0T1drcmEwTk1Uek5YWkc1VWFrNVhNR2M0UlZoVk1GcDFWMXd2VUc1WmJIQXpZM2M5SWl3aWJXRmpJam9pWmprd056VTBOVE0zTlRjeVpUY3pPR1E1TldFME1XUTFPRGhpWkROa09EaGhNalJoT1RRM1pEWmxaVGsxTTJRM09EVTJaVFF3WVRJelpURXhZekV5WlNKOQ==', 1548387869),
('IwDa9KtSl8rqEBREpChqHI0kSLzgDznzevEFRmRv', 34, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 'ZXlKcGRpSTZJa0pUWmtaVFdrOXNkRk5KWVZkSldqWnBOazlJSzNjOVBTSXNJblpoYkhWbElqb2llVlZKU0hGa2J6ZFhPRlJtZWtzelJXWjNhbU5pVWx3dk0ydHVhamQ0VVVkMk9XRmFjbEpjTDF3dldqVktWVUpJWEM5V2VYbzRYQzk2WVZsRFkyUlNka05KUlVsM2JrMWtRelZ6VUdGR2NIbDRWMmd4TlhRNWRUUmFSVk00VW1GQmRXbGpRWEpNUmtJclZEWm1YQzlTZFRkd2JITjZXa2syZFdoUmNqZGNMMlZHYldkeWJXNXhWVFJWY2tOTE1HMXBWbWQ1VW1Jd1dXaHJkMjF1TURBNFYxWjBNMU5yTmxSQldrUkZTSEZDZGpZelhDOVRTMmxKZVUxRFRUVkpla1YzVEhacVRFczBZalk0UlRaWlQyVXJiREJrWm5oeFprOXpjMVV6ZWt3NGVrMWFhV0ZhTUZGdFJHTkNNV05RWkN0dU1pdGhkbTlHTUhSU2NXbDBla1pXVEhoaGVWUjZlR0pIYkVaTFEzRlpTSGh5VW5weFRGd3ZjRXNyUlRsd1MyVkNha05KTmtobllXNXBRMHA2YkVjeE4ycHZjbUZNV0dwTllrOWlPWFowTTBwMVFpdDNlbXBNWkd0Y0wybFNZWE5WWVhweFdEWXpabmRtUmpSbFVDczRWbFp4TW13NFRUQlNhbE5IZUc1aGVWSklaMDFKUlQwaUxDSnRZV01pT2lKbVlUWmpPREJrT0RRM1ltVm1aV013WldKaE56VmlaREE0Wm1abU1UZzBNV0U1TlRZME1UWTBZV1ZrTVRSaE1EWmhZMlJrT0RKbVlXWTNZV0poWTJJd0luMD0=', 1548387887),
('NqrgApb3pHhBMhBwSNNDAHHazLPFiOjVmwovSRpN', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 'ZXlKcGRpSTZJalI2VDJGMmFrRnVibEJIZGpkdlJsWkNVVmRoZUVFOVBTSXNJblpoYkhWbElqb2lZV0ZEVm5OTFZrNVJUMVV6TnpFNWVUTkJTSHBwWkdsV2NIRnlXR1puWlhwelFYTkRXWFFyZUU5aWIyaE9SMU4zTldvemFWTlZkVnBTU0ZWM1JtdDZWbFZtYUdOemRIaHhVbVIyV0VKSlN6UkRhazFjTDNGcmIwOVhlVm8wUW1JMGEyOWlaMU42WmtwWEsya3JXVGRLTjA5MVdtdEhVekYwVkRkb1ZGRkxZVWcxUXpkWlJXZDJXbWhyVGtNd00wWlpZbGc1WVZoQlVqbGNMMmhaUzNsQ1dsd3ZVVTlPZUdKVFYxd3ZWM1ZaVmtoQ1puSlpZa2xIUVdkeFF6UnZjbWRtZERsT1dVOUhhekJNZFdkQ1EzSjVWR2N4YTAxblIxcEdTVFJSVDBJclFrSjFjbTB4TmtwYVpVdDZXRTAyYUVGMFpYcDFUbGw0VkhCRk1IQkdUMmc0Y1hVNVNucHRNRVZaYkRBNGVXRjJPVk5GVm1wTU1tRjFNWEZuUFQwaUxDSnRZV01pT2lJMk5UTTNaRE5rTjJGak1qRTRNV1UxWlRVNE1tSTFZalU0TURjMFlXSTJOV0V4TlRCbU1qQTBaV1E1TTJVeVptRXlZV0pqTURVd1ltSTRZVFJsWm1NeUluMD0=', 1548387894),
('S15F8n1JDHtsGciKX0GQj7HdygMDoo9bhN0mH3vb', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 'ZXlKcGRpSTZJbHBYVm5kYVJteHpNRGhuWkUxUWVFRjNTMjlWUjJjOVBTSXNJblpoYkhWbElqb2lZbU5yWTBwSVhDOUVhR2hpVUVSelZtaDBZVlkwVFhSd2VHSXdObGhvYm5jMVNEVmFNbkEyZFhvMmVrdENPV2gxT0ZBeU5ucHNTbGhqVG5oc09FODJOMWhKWVhkY0wzZzJXVk5uVjBoMU1YWkZabWhxYW5sbmNWVlFaWGx6VDBneFVVTlRjR3BaZERkU2MzVk5OR05oWW1SaGIzTlRTRnd2VW5sWFpFUmhNbm8wV2t0MFp6UlNVVXhXVUU5NmRERlpZM016TXpNM1kwOURRbmw1ZEhSWE9WVktlSHBaWEM5c1pIaFVXblJ4Wkd4a2FGRnBhWGxZU1VoNVFtWktPSEpXY1dFeFIyZGlORFZDUVRWMWNrTkhkVEV5UTJSQ2NVdHJiMHhCY2tSMVVHdHRZMG9yVGtWQ1RISlZXRkE1ZEZCVlZTdGpjazEwUlZneVJscGhOMjlDWW5wbU5FNUpiREpHTWxKY0wxQjRibVJOWWxKemEyVXhkMVJvUVQwOUlpd2liV0ZqSWpvaU1UUXpZV1l5TVRrNU1UUTJPV014Tm1Nd05UZ3dZbUUyTlRjMU9USTVPR1ZpTUdJM04ySXlOekJqWlRsaFkyVmlOalJsT1dOaE1EYzVZamcxTmpSa05TSjk=', 1548387884),
('tHZSMDuGOK7E31n9NsuOwPeDqB6DaKyoVx7qGwM6', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 'ZXlKcGRpSTZJbTgwZEdKdFlqRldhVXR6U25RMWJHd3hRMHRsTjJjOVBTSXNJblpoYkhWbElqb2laMG93Ym1wRGIzb3lORk0zVkVGcVJuTnFkMEk1T0RaaVpGVmxSa1pHWlhFeVZqQnlTRnAwTlVwV1RYbFZaR1ZtVW0xa01tTk1lSEpUZERWdGJqVlljbGRDWkRBMWRYaE9jMXd2TmxaeFltVXhZMDh4YWxGaVJYaEtkRFpQU2pkSFVISkRSa2hXYVdzeGRERmpaemx2T1U5QlZWZ3hVMWRHVFVOdVpFdFNiMVl3YjBOTFoxZDZUMVpqV2pKMWRWd3ZXREE0VWpkR1RtRnVjREZSVmpaS1FXbFVWelpFTmtsVGNYcEVNVms5SWl3aWJXRmpJam9pTlRNM05EUTFOemMyWXpFMFl6UTFNamxtTVdOak56UXdaR1UyTmpjMFlXTmlNR0ppTVdNM01UQTJZVEZsTjJJd1pqYzNOalUwWVRBd01EZzFOekppTVNKOQ==', 1548387898),
('Xf3pFuC1j4VJVZjgxmWW8i8yd0lcG9G71rfiWKzr', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 'ZXlKcGRpSTZJbk0yTW1Wc2FGUTVla1JOSzJkd01XYzJLMHRTTkZFOVBTSXNJblpoYkhWbElqb2lTR0YyY0hSVk4zVjBiamhXZUVoUWFHSnRUVUV6VWtKclQzRlFWVGQwVWs5blpqWndNbGRuZFhoMWVHSkxVelI0V0VkTk9HZzNTWEZ6Wm1WVE1XeERLelJxZUU4ek5HaFVkSEl4WTNSMGJXWkZaMU0xVm01RE1uQkJXVnd2VWxkdVJIRllkSGsxWTNSMFhDOHdlVTF5VlhOWFQwTTNaalZGV0VncmQxZHVaVVE0UzAxU2EzQldXVmMwVWxkTFdHVnhiWE5aYzNKVU5HNVlaV1V6TkZKTE9GSmtRa0V5YkU5WlhDOVlOMHBaUFNJc0ltMWhZeUk2SW1FME16WXhOakF4TVRGbFpESXhaVE5rTVdFME5UWTRNV1ZrWldNeU1qVTJaRFZqTnpreU5EQmlNV00xTkRKaE1HSTRZekJrWVdVM05UVmhaVFJpTmpNaWZRPT0=', 1548387888);

-- --------------------------------------------------------

--
-- Table structure for table `teamindustries`
--

CREATE TABLE `teamindustries` (
  `id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teamindustries`
--

INSERT INTO `teamindustries` (`id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Real Estate', 1, NULL, NULL),
(2, 'Trading', 1, NULL, NULL),
(3, 'IT', 1, NULL, NULL),
(4, 'Marketing', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teaminvitations`
--

CREATE TABLE `teaminvitations` (
  `id` int(10) UNSIGNED NOT NULL,
  `inviter` int(11) NOT NULL,
  `invitee` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `companyid` int(11) NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `teamstrength`
--

CREATE TABLE `teamstrength` (
  `id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teamstrength`
--

INSERT INTO `teamstrength` (`id`, `title`, `count`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Only me', 1, 1, NULL, NULL),
(2, '2-5', 5, 1, NULL, NULL),
(3, '6-10', 10, 1, NULL, NULL),
(4, '11-15', 15, 1, NULL, NULL),
(5, '16-25', 25, 1, NULL, NULL),
(6, '26-50', 50, 1, NULL, NULL),
(7, '51-100', 100, 1, NULL, NULL),
(8, '101-500', 500, 1, NULL, NULL),
(9, '500', 5000, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `themecategories`
--

CREATE TABLE `themecategories` (
  `id` int(11) NOT NULL,
  `themecategory` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `themecategories`
--

INSERT INTO `themecategories` (`id`, `themecategory`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Premium', 1, '2019-01-21 04:31:43', '2019-01-21 04:31:43'),
(2, 'IT', 1, '2019-01-21 04:31:43', '2019-01-21 04:31:43');

-- --------------------------------------------------------

--
-- Table structure for table `themegroups`
--

CREATE TABLE `themegroups` (
  `id` int(11) NOT NULL,
  `themeid` int(11) NOT NULL,
  `sequence` int(11) NOT NULL DEFAULT '0',
  `groupname` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` int(11) NOT NULL,
  `themename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `themecategory` int(11) NOT NULL,
  `thumbnails` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `themetemplate` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `themename`, `description`, `themecategory`, `thumbnails`, `themetemplate`, `status`, `created_at`, `updated_at`) VALUES
(1, 'start from scratch', 'start from scratch', 1, NULL, '[{\"title\":\"Open Projects\",\"sequence\":\"1\",\"columns\":[\"Open Projects\",\"Owner\"],\"columnstype\":[\"longtext\",\"longtext\"],\"columnsheading\":[\"longtext\",\"longtext\"],\"columnssample\":[{\"Open Projects\":\"Row 1\",\"Owner\":\"Owner\"},{\"Open Projects\":\"Row 2\",\"Owner\":\"Owner\"}],\"col\":{\"index\":\"1\",\"value\":\"title\",\"type\":\"longtext\",\"heading\":\"\"},\"0\":{\"title\":\"Owner\",\"index\":\"2\",\"value\":\"owner\",\"type\":\"longtext\",\"heading\":\"0\"}},{\"title\":\"Closed Projects\",\"index\":\"1\",\"sequence\":\"2\",\"columns\":[\"Closed Projects\",\"Owner\"],\"columnstype\":[\"longtext\",\"longtext\"],\"columnsheading\":[\"longtext\",\"longtext\"],\"columnssample\":[{\"Open Projects\":\"Row 1\",\"Owner\":\"Owner\"},{\"Open Projects\":\"Row 2\",\"Owner\":\"Owner\"}],\"col\":{\"value\":\"title\",\"type\":\"longtext\",\"heading\":\"1\"},\"0\":{\"title\":\"Owner\",\"index\":\"2\",\"value\":\"owner\",\"type\":\"longtext\",\"heading\":\"0\"}}]\r\n', 1, '2019-01-21 04:33:36', '2019-01-21 04:33:36'),
(2, 'Basic', 'Basic\r\n', 1, NULL, '[{\"title\":\"Open Projects\",\"sequence\":\"1\",\"columns\":[\"Open Projects\",\"Owner\"],\"columnstype\":[\"longtext\",\"longtext\"],\"columnsheading\":[\"longtext\",\"longtext\"],\"columnssample\":[{\"Open Projects\":\"Row 1\",\"Owner\":\"Owner\"},{\"Open Projects\":\"Row 2\",\"Owner\":\"Owner\"}],\"col\":{\"index\":\"1\",\"value\":\"title\",\"type\":\"longtext\",\"heading\":\"\"},\"0\":{\"title\":\"Owner\",\"index\":\"2\",\"value\":\"owner\",\"type\":\"longtext\",\"heading\":\"0\"}},{\"title\":\"Closed Projects\",\"index\":\"1\",\"sequence\":\"2\",\"columns\":[\"Closed Projects\",\"Owner\"],\"columnstype\":[\"longtext\",\"longtext\"],\"columnsheading\":[\"longtext\",\"longtext\"],\"columnssample\":[{\"Open Projects\":\"Row 1\",\"Owner\":\"Owner\"},{\"Open Projects\":\"Row 2\",\"Owner\":\"Owner\"}],\"col\":{\"value\":\"title\",\"type\":\"longtext\",\"heading\":\"1\"},\"0\":{\"title\":\"Owner\",\"index\":\"2\",\"value\":\"owner\",\"type\":\"longtext\",\"heading\":\"0\"}}]\r\n', 1, '2019-01-21 04:33:36', '2019-01-21 04:33:36'),
(3, 'IT1', 'Basic\r\n', 2, NULL, '[{\"title\":\"Open Projects\",\"sequence\":\"1\",\"columns\":[\"Open Projects\",\"Owner\"],\"columnstype\":[\"longtext\",\"longtext\"],\"columnsheading\":[\"longtext\",\"longtext\"],\"columnssample\":[{\"Open Projects\":\"Row 1\",\"Owner\":\"Owner\"},{\"Open Projects\":\"Row 2\",\"Owner\":\"Owner\"}],\"col\":{\"index\":\"1\",\"value\":\"title\",\"type\":\"longtext\",\"heading\":\"\"},\"0\":{\"title\":\"Owner\",\"index\":\"2\",\"value\":\"owner\",\"type\":\"longtext\",\"heading\":\"0\"}},{\"title\":\"Closed Projects\",\"index\":\"1\",\"sequence\":\"2\",\"columns\":[\"Closed Projects\",\"Owner\"],\"columnstype\":[\"longtext\",\"longtext\"],\"columnsheading\":[\"longtext\",\"longtext\"],\"columnssample\":[{\"Open Projects\":\"Row 1\",\"Owner\":\"Owner\"},{\"Open Projects\":\"Row 2\",\"Owner\":\"Owner\"}],\"col\":{\"value\":\"title\",\"type\":\"longtext\",\"heading\":\"1\"},\"0\":{\"title\":\"Owner\",\"index\":\"2\",\"value\":\"owner\",\"type\":\"longtext\",\"heading\":\"0\"}}]\r\n', 1, '2019-01-21 04:33:36', '2019-01-21 04:33:36'),
(4, 'IT2', 'Basic\r\n', 2, NULL, '[{\"title\":\"Open Projects\",\"sequence\":\"1\",\"columns\":[\"Open Projects\",\"Owner\"],\"columnstype\":[\"longtext\",\"longtext\"],\"columnsheading\":[\"longtext\",\"longtext\"],\"columnssample\":[{\"Open Projects\":\"Row 1\",\"Owner\":\"Owner\"},{\"Open Projects\":\"Row 2\",\"Owner\":\"Owner\"}],\"col\":{\"index\":\"1\",\"value\":\"title\",\"type\":\"longtext\",\"heading\":\"\"},\"0\":{\"title\":\"Owner\",\"index\":\"2\",\"value\":\"owner\",\"type\":\"longtext\",\"heading\":\"0\"}},{\"title\":\"Closed Projects\",\"index\":\"1\",\"sequence\":\"2\",\"columns\":[\"Closed Projects\",\"Owner\"],\"columnstype\":[\"longtext\",\"longtext\"],\"columnsheading\":[\"longtext\",\"longtext\"],\"columnssample\":[{\"Open Projects\":\"Row 1\",\"Owner\":\"Owner\"},{\"Open Projects\":\"Row 2\",\"Owner\":\"Owner\"}],\"col\":{\"value\":\"title\",\"type\":\"longtext\",\"heading\":\"1\"},\"0\":{\"title\":\"Owner\",\"index\":\"2\",\"value\":\"owner\",\"type\":\"longtext\",\"heading\":\"0\"}}]\r\n', 1, '2019-01-21 04:33:36', '2019-01-21 04:33:36');

-- --------------------------------------------------------

--
-- Table structure for table `timezone`
--

CREATE TABLE `timezone` (
  `id` int(11) NOT NULL,
  `countryname` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `utcoffsetvalue` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `useractivity`
--

CREATE TABLE `useractivity` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `companyid` int(11) NOT NULL,
  `activity` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `useremailverification`
--

CREATE TABLE `useremailverification` (
  `id` int(11) NOT NULL,
  `emailaddress` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `validfor` int(11) NOT NULL DEFAULT '600',
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `useremailverification`
--

INSERT INTO `useremailverification` (`id`, `emailaddress`, `password`, `token`, `validfor`, `status`, `created_at`, `updated_at`) VALUES
(22, 'liyashereef@b-mates.com', '123456', '627339', 600, 1, '2019-01-16 02:48:01', '2019-01-16 02:48:50'),
(23, 'liyashereef@gmail.com', '123456', '859486', 600, 1, '2019-01-16 02:52:20', '2019-01-16 02:53:04');

-- --------------------------------------------------------

--
-- Table structure for table `userroles`
--

CREATE TABLE `userroles` (
  `id` int(11) NOT NULL,
  `rolename` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `owner` int(2) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `userroles`
--

INSERT INTO `userroles` (`id`, `rolename`, `description`, `owner`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 1, 1, '2019-01-17 07:21:57', '2019-01-17 09:36:06'),
(2, 'Member', 'Member', 0, 1, '2019-01-17 07:21:57', '2019-01-17 07:21:57'),
(3, 'Viewer', 'Viewer', 0, 1, '2019-01-17 07:22:15', '2019-01-17 07:22:15'),
(4, 'Guest', 'Guest', 0, 1, '2019-01-17 07:22:15', '2019-01-17 07:22:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `companyid` int(11) NOT NULL,
  `phoneno` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobno` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skype` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jobtitle` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` int(11) DEFAULT NULL,
  `resettoken` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastlogin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `referredby` int(11) DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `companyid`, `phoneno`, `mobno`, `skype`, `jobtitle`, `timezone`, `resettoken`, `lastlogin`, `email_verified_at`, `password`, `referredby`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(34, 'Liya shereef bmates id', 'liyashereef@b-mates.com', 28, '', NULL, NULL, NULL, NULL, '560683', '2019-02-01 06:41:18', NULL, '$2y$10$/gQeSTrIWh4N6uBmt6t5P.mVkChlLwAW5oMmCvLRNpLA/9VMTR/y2', 0, 1, '7Fh9brsd2GJqLqvyHqFsk2BCGxguQ45BavBrP8O9XcM7ddEDQN9iBUrHM7ot', '2019-01-16 02:49:11', '2019-01-17 03:56:00'),
(35, 'Liya shereef gmail', 'liyashereef@gmail.com', 28, '', NULL, NULL, NULL, NULL, NULL, '2019-01-16 08:23:05', NULL, '$2y$10$i.lVE8NaBy8wfzQR.fzmCu2WOtkCJmfDH41nGvkC9EcbLu3drHOU2', 0, 1, NULL, '2019-01-16 02:53:05', '2019-01-16 02:53:05');

-- --------------------------------------------------------

--
-- Table structure for table `workarea`
--

CREATE TABLE `workarea` (
  `id` int(11) NOT NULL,
  `workarea` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `workareaicon` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `workarea`
--

INSERT INTO `workarea` (`id`, `workarea`, `workareaicon`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Real Estate', '', 1, NULL, NULL),
(2, 'Trading', '', 1, NULL, NULL),
(3, 'IT', '', 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `--plans-columns`
--
ALTER TABLE `--plans-columns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `__plans_columns_plans_foreign` (`plans`),
  ADD KEY `__plans_columns_columns_foreign` (`columns`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_email_unique` (`email`);

--
-- Indexes for table `boardcolumns`
--
ALTER TABLE `boardcolumns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `boardcolumns_columnid_foreign` (`columnid`),
  ADD KEY `boardcolumns_boardid_foreign` (`boardid`),
  ADD KEY `boardcolumns_account_foreign` (`account`);

--
-- Indexes for table `boardgroupcolumns`
--
ALTER TABLE `boardgroupcolumns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `boardgroupcolumns_groupid_foreign` (`groupid`);

--
-- Indexes for table `boardgroups`
--
ALTER TABLE `boardgroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `boardgroups_boardid_foreign` (`boardid`);

--
-- Indexes for table `boards`
--
ALTER TABLE `boards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `boards_boardtype_foreign` (`boardtype`);

--
-- Indexes for table `boardtypes`
--
ALTER TABLE `boardtypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `board_roles`
--
ALTER TABLE `board_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `board_roles_userid_foreign` (`userid`),
  ADD KEY `board_roles_boardid_foreign` (`boardid`),
  ADD KEY `board_roles_userroles_foreign` (`userroles`);

--
-- Indexes for table `columntypes`
--
ALTER TABLE `columntypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `companies_companyname_unique` (`companyname`),
  ADD KEY `companies_warea_foreign` (`warea`),
  ADD KEY `companies_currentplan_foreign` (`currentplan`),
  ADD KEY `companies_teamstrength_foreign` (`teamstrength`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `teamindustries`
--
ALTER TABLE `teamindustries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teaminvitations`
--
ALTER TABLE `teaminvitations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `teaminvitations_invitee_companyid_unique` (`invitee`,`companyid`);

--
-- Indexes for table `teamstrength`
--
ALTER TABLE `teamstrength`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themecategories`
--
ALTER TABLE `themecategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themegroups`
--
ALTER TABLE `themegroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `themegroups_themeid_foreign` (`themeid`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `themes_themecategory_foreign` (`themecategory`);

--
-- Indexes for table `timezone`
--
ALTER TABLE `timezone`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `timezone_countryname_unique` (`countryname`);

--
-- Indexes for table `useractivity`
--
ALTER TABLE `useractivity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `useremailverification`
--
ALTER TABLE `useremailverification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userroles`
--
ALTER TABLE `userroles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `compositeuniquekey` (`email`,`companyid`),
  ADD KEY `users_email_companyid_index` (`email`,`companyid`),
  ADD KEY `users_companyid_foreign` (`companyid`),
  ADD KEY `users_timezone_foreign` (`timezone`);

--
-- Indexes for table `workarea`
--
ALTER TABLE `workarea`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `--plans-columns`
--
ALTER TABLE `--plans-columns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `boardcolumns`
--
ALTER TABLE `boardcolumns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `boardgroupcolumns`
--
ALTER TABLE `boardgroupcolumns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `boardgroups`
--
ALTER TABLE `boardgroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `boards`
--
ALTER TABLE `boards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `boardtypes`
--
ALTER TABLE `boardtypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `board_roles`
--
ALTER TABLE `board_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `columntypes`
--
ALTER TABLE `columntypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `teamindustries`
--
ALTER TABLE `teamindustries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `teaminvitations`
--
ALTER TABLE `teaminvitations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teamstrength`
--
ALTER TABLE `teamstrength`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `themecategories`
--
ALTER TABLE `themecategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `themegroups`
--
ALTER TABLE `themegroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `timezone`
--
ALTER TABLE `timezone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `useractivity`
--
ALTER TABLE `useractivity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `useremailverification`
--
ALTER TABLE `useremailverification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `userroles`
--
ALTER TABLE `userroles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `workarea`
--
ALTER TABLE `workarea`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `--plans-columns`
--
ALTER TABLE `--plans-columns`
  ADD CONSTRAINT `__plans_columns_columns_foreign` FOREIGN KEY (`columns`) REFERENCES `columntypes` (`id`),
  ADD CONSTRAINT `__plans_columns_plans_foreign` FOREIGN KEY (`plans`) REFERENCES `plans` (`id`);

--
-- Constraints for table `boardcolumns`
--
ALTER TABLE `boardcolumns`
  ADD CONSTRAINT `boardcolumns_account_foreign` FOREIGN KEY (`account`) REFERENCES `companies` (`id`),
  ADD CONSTRAINT `boardcolumns_boardid_foreign` FOREIGN KEY (`boardid`) REFERENCES `boards` (`id`),
  ADD CONSTRAINT `boardcolumns_columnid_foreign` FOREIGN KEY (`columnid`) REFERENCES `boardgroupcolumns` (`id`);

--
-- Constraints for table `boardgroupcolumns`
--
ALTER TABLE `boardgroupcolumns`
  ADD CONSTRAINT `boardgroupcolumns_groupid_foreign` FOREIGN KEY (`groupid`) REFERENCES `boardgroups` (`id`);

--
-- Constraints for table `boardgroups`
--
ALTER TABLE `boardgroups`
  ADD CONSTRAINT `boardgroups_boardid_foreign` FOREIGN KEY (`boardid`) REFERENCES `boards` (`id`);

--
-- Constraints for table `boards`
--
ALTER TABLE `boards`
  ADD CONSTRAINT `boards_boardtype_foreign` FOREIGN KEY (`boardtype`) REFERENCES `boardtypes` (`id`);

--
-- Constraints for table `board_roles`
--
ALTER TABLE `board_roles`
  ADD CONSTRAINT `board_roles_boardid_foreign` FOREIGN KEY (`boardid`) REFERENCES `boards` (`id`),
  ADD CONSTRAINT `board_roles_userid_foreign` FOREIGN KEY (`userid`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `board_roles_userroles_foreign` FOREIGN KEY (`userroles`) REFERENCES `userroles` (`id`);

--
-- Constraints for table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `companies_currentplan_foreign` FOREIGN KEY (`currentplan`) REFERENCES `plans` (`id`),
  ADD CONSTRAINT `companies_teamstrength_foreign` FOREIGN KEY (`teamstrength`) REFERENCES `teamstrength` (`id`),
  ADD CONSTRAINT `companies_warea_foreign` FOREIGN KEY (`warea`) REFERENCES `workarea` (`id`);

--
-- Constraints for table `themegroups`
--
ALTER TABLE `themegroups`
  ADD CONSTRAINT `themegroups_themeid_foreign` FOREIGN KEY (`themeid`) REFERENCES `themes` (`id`);

--
-- Constraints for table `themes`
--
ALTER TABLE `themes`
  ADD CONSTRAINT `themes_themecategory_foreign` FOREIGN KEY (`themecategory`) REFERENCES `themecategories` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_companyid_foreign` FOREIGN KEY (`companyid`) REFERENCES `companies` (`id`),
  ADD CONSTRAINT `users_timezone_foreign` FOREIGN KEY (`timezone`) REFERENCES `timezone` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
