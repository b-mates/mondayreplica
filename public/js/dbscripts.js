 function openCity(evt, cityName) {
     var i, tabcontent, tablinks;
     tabcontent = document.getElementsByClassName("tabcontent");
     for (i = 0; i < tabcontent.length; i++) {
         tabcontent[i].style.display = "none";
     }
     tablinks = document.getElementsByClassName("tablinks");
     for (i = 0; i < tablinks.length; i++) {
         tablinks[i].className = tablinks[i].className.replace(" active", "");
     }
     document.getElementById(cityName).style.display = "block";
     evt.currentTarget.className += " active";
 }



 // if user clicked on button, the overlay layer or the dialogbox, close the dialog	





 $('#menu li').on('click', function() {
     $(this).addClass('current').siblings().removeClass('current');
 });





 /* When the user clicks on the button,
 toggle between hiding and showing the dropdown content */
 function myFunction() {
     document.getElementById("myDropdown").classList.toggle("show");
 }

 function filterFunction() {
     var input, filter, ul, li, a, i;
     input = document.getElementById("myInput");
     filter = input.value.toUpperCase();
     div = document.getElementById("myDropdown");
     a = div.getElementsByTagName("a");
     for (i = 0; i < a.length; i++) {
         if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
             a[i].style.display = "";
         } else {
             a[i].style.display = "none";
         }
     }
 }





 function validateEmail(email) {
     var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
     return re.test(email);
 }
 (function($) {

     $.fn.multipleInput = function() {
         return this.each(function() {
             // list of email addresses as unordered list
             $list = $('<ul/>');
             // input
             var $input = $('<input type="email" id="email_search" class="email_search multiemail"/>').keyup(function(event) {
                 if (event.which == 13 || event.which == 32 || event.which == 188) {
                     if (event.which == 188) {
                         var val = $(this).val().slice(0, -1); // remove space/comma from value
                     } else {
                         var val = $(this).val(); // key press is space or comma                        
                     }
                     if (validateEmail(val)) {
                         // append to list of emails with remove button
                         $list.append($('<li class="multipleInput-email"><span>' + val + '</span></li>')
                             .append($('<a href="#" class="multipleInput-close" title="Remove"><i class="glyphicon glyphicon-remove-sign"></i></a>')
                                 .click(function(e) {
                                     $(this).parent().remove();
                                     e.preventDefault();
                                 })
                             )
                         );
                         $(this).attr('placeholder', '');
                         // empty input
                         $(this).val('');
                     } else {
                         alert('Please enter valid email id, Thanks!');
                     }
                 }
             });
             // container div
             var $container = $('<div class="multipleInput-container" />').click(function() {
                 $input.focus();
             });
             // insert elements into DOM
             $container.append($list).append($input).insertAfter($(this));
             return $(this).hide();
         });
     };
 })(jQuery);
 $('#recipient_email').multipleInput();

 $("#invite").click(function() {
     $data = $('.multipleInput-container').html();
     $emaillist = (extractEmails($data));
     $("#invite").hide();
     if ($emaillist == null) {
         alert("Please input some email and click Invite");
         $("#invite").show();
     } else {
         $.ajax({
             type: "post",
             async: true,
             url: "/profile/inviteteam",
             data: { emaillist: $emaillist },
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             success: function(response) {
                 var obj = jQuery.parseJSON(response);
                 if (obj.success == 1) {
                     $("#invite").show();
                     alert("Invited successfully");
                     closeNavinvite();
                 } else if (obj.success == 0) {
                     $("#invite").show();
                     alert("One or more email addressed failed" + obj.failedemails);
                     closeNavinvite();
                 }
             }
         });

     }
 });

 function addPulse(content) {
     var inputid = content.id;
     var inputvalue = inputid.replace("btn", "");
     $boardid = $("#boardhid").val();
     var input = document.getElementById(inputvalue).value;
     if (input != "") {
         $.ajax({
             type: "post",
             url: "/template/addPulse",
             data: { boardid: $boardid, groupid: inputvalue, pulse: input },
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             success: function(response) {
                 document.getElementById(inputvalue).value = "";
                 chooseBoard($boardid);
             }
         });
     }


 }

 function useTemplate(templateid) {
     $("#thm-" + templateid).hide();
     $boardtype = $("#boardtypehidden").val();
     $.ajax({
         type: "post",
         async: true,
         url: "/template/createtemplate",
         data: { boardtype: $boardtype, templateid: templateid },
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(response) {
             var obj = jQuery.parseJSON(response);
             var newboardid = obj.board;
             chooseBoard(newboardid);
             document.getElementById("themeoverlay").style.display = "none";
             $("#thm-" + templateid).show();
         }
     });

 }

 function extractEmails(text) {
     return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
 }

 function openCity(evt, cityName) {
     var i, tabcontent, tablinks;
     tabcontent = document.getElementsByClassName("tabcontent");
     for (i = 0; i < tabcontent.length; i++) {
         tabcontent[i].style.display = "none";
     }
     tablinks = document.getElementsByClassName("tablinks");
     for (i = 0; i < tablinks.length; i++) {
         tablinks[i].className = tablinks[i].className.replace(" active", "");
     }
     document.getElementById(cityName).style.display = "block";
     evt.currentTarget.className += " active";
 }






 /* When the user clicks on the button,
 toggle between hiding and showing the dropdown content */

 /*
 New Board creation
 */

 function newBoard(id) {
     document.getElementById("boardtypehidden").value = id;
     document.getElementById("newboard").style.width = "100%";
 }



 function closenewboard() {
     document.getElementById("newboard").style.width = "0%";
 }


 $("#newboardsave").click(() => {
     $boardname = $("#boardname").val();
     $boardtype = $("#boardtypehidden").val();
     if ($boardname != "") {
         $.ajax({
             type: "post",
             async: true,
             url: "/profile/createboard",
             data: { boardname: $boardname, boardtype: $boardtype },
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             success: function(response) {
                 var obj = jQuery.parseJSON(response);
                 if (obj.success == 1) {
                     closenewboard();
                     openThemeselection();


                 } else {
                     alert("There is some issue");
                 }
             }
         });
     }

 });
 /*
 Theme selection
 */

 $("#newthemebutton").click(function() {
     $("#boardname").val("");
     document.getElementById("themeoverlay").style.display = "none";
     $("#accordion").html("");
 });


 /*
 Place Boards and rows while calling template from left menu in home page
 */

 function setBoardandColumns() {
     alert("Hi");
 }

 function openThemeselection() {

     $.ajax({
         type: "get",
         async: true,
         url: "profile/getThemeslist",
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(response) {
             $("#accordionthememenu").html(response).after(function() {
                 $("#accordion").accordion({ autoHeight: false, collapsible: true, active: true, header: 'h3', content: 'div' });
                 document.getElementById("themeoverlay").style.display = "block";
                 // 

             });


             //set state
             // $("#accordion").accordion( "option" );


         },
         complete: function(data) {
             $('#accordion').accordion('destroy').accordion({ autoHeight: false, collapsible: true, active: false, header: 'h3', active: 0 });
         }
     });
 }

 function openThemeselectionbck() {


     var category = [];


     $("#boardname").val("");
     var html = "";
     var i = 0;
     var firsttheme = 0;
     $.ajax({
         type: "get",
         async: true,
         url: "profile/Themescategorylist",
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(response) {
             i++;


             document.getElementById("themeoverlay").style.display = "block";




             var result = $.parseJSON(response);



             $.each(result, function(k, v) {
                 category.push(k);
                 html = "<h4>" + k + "-" + v + "</h4><div id='c-" + k + "'><div></div></div>";


                 //save state
                 var state = $("#accordion").accordion("option", "active");
                 //add accordion item, destroy then re-create
                 $("#accordion").append(html).accordion("destroy").accordion();
                 //set state
                 $("#accordion").accordion("option", "active", state);

             });


             // 


         }
     }).done(function() {
         var len = category.length;
         for (i = 0; i < len; i++) {
             var percateg = "";
             $.get("profile/Themeslist", { category: i })
                 .done(function(data) {


                     var themeresult = $.parseJSON(data);
                     $.each(themeresult, function(kt, vt) {


                         // html+="<div><p>Content</p> </div>";alert(html);
                         percateg += "<div>" + vt + "</div>";
                         $("#c-" + i).html(percateg);

                     });
                 });
             alert(percateg);
         }

         var state2 = $("#accordion").accordion("option", "active");
         $("#accordion").accordion("destroy").accordion();
         $("#accordion").accordion("option", "active", state2);

     });




 }



 /*
 Get Board details
 */

 function chooseBoard(boardid) {
     var Body = $('body');
     //$(".preloader-wrapper").show();
     Body.addClass('preloader-site');
     $("#inbox").html(" ");


     $(function() {
         $("#inbox").html(" ").after(function() {
             $.ajax({
                 type: "get",
                 async: true,
                 url: "profile/BoardDetails",
                 data: { boardid: boardid },
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                 success: function(response) {
                     $("#inbox").html(response).after(function() {


                         $.ajax({
                             type: "get",
                             async: true,
                             url: "template/populatedata",
                             data: { boardid: boardid },
                             headers: {
                                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                             },
                             success: function(response) {
                                 $(response).insertAfter($("#searchbox"));



                             }
                         });




                     }).after(function() {
                         // $('.preloader-wrapper').fadeOut();
                         $('body').removeClass('preloader-site');
                     });


                 }
             });
         });
     });
 }


 function editBoardheadlabel(ths, boarheadid, grouphead) {
     var dad = $(ths).parent();
     $(ths).hide();
     $("#head-" + grouphead + "-" + boarheadid).show().focus();
 }

 function editBoardheadblur(ths, boarheadid, grouphead) {
     var dad = $(ths).parent();
     $("#" + boarheadid + "-" + grouphead + "-" + "span").show();
     $("#head-" + grouphead + "-" + boarheadid).hide();
 }


 function editPulselabel(ths, pulseid) {
     var dad = $(ths).parent().parent();
     dad.find('span').hide();
     dad.find('input[type="text"]').show().focus();
 }

 function editpulseblur(ths, pulseid) {

     var dad = $(ths).parent().parent();
     dad.find('span').show();
     dad.find('img').show().focus();
     dad.find('input[type="text"]').hide();
 }

 $(document).on('click', '.component', function(event) {
     $boardid = $("#boardhid").val();
     $component = $(this).attr('component').replace("component-", "");
     $componentvalueid = $(this).attr('componentvalueid');

     $componentheadvalueid = $(this).attr('componenthead');


     if ($component == "longtext") {
         $(this).removeClass("component");
         var self = $(this);

         $.ajax({
             type: "post",
             url: "widget/whichinput",
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             data: { "boardid": $boardid, "component": $component, "componentvalueid": $componentvalueid, 'headid': $componentheadvalueid },

             success: function(response, event) {

                 $(self).html(response).focusout(function() {

                     $(self).addClass("component");


                 });

                 $('#' + $componentvalueid).focus();
             }
         });
     } else if ($component == "statuscolumn") {

         /*
        
         */


         var self = $(this);

         $.ajax({
             type: "post",
             url: "widget/whichinput",
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             data: { "boardid": $boardid, "component": $component, "componentvalueid": $componentvalueid, 'headid': $componentheadvalueid },

             success: function(response, event) {





             }
         });

     }






     event.stopPropagation();
 });




 $(document).on('click', '.comments', function(e) {
     var parent = $(this).parent();
     var headid = parent.attr("componenthead");
     $currpulse = $(this).parent().attr('pulse');

     $componentvalueid = $(this).parent().attr('componentvalueid');

     /*

         
         if($('#newDiv1').is(':visible'))
         {
             $("#newDiv1").hide().remove();
         }
         */
     var self = this;
     $.ajax({
         type: "post",
         url: "widget/getStatuscolumns",
         data: { "headid": headid },
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(response) {
             /*
                 $(self).parent().append(
                     $('<div/>')
                     .attr("id", "newDiv1")
                     .addClass("popupdiv")
                     .html(response).focus()
                 ); 
             */

             $("#currentpulse").val($currpulse);
             $("#currcomponentval").val($componentvalueid);

             var offset = $(self).offset();

             $('.greet').html(response);

             $('.greet')
                 .show()
                 .css({
                     top: offset.top - $('.greet').height(),
                     left: offset.left + 50,
                     opacity: 1,
                 })
                 .stop()
                 .delay(200);
         }
     });





     var parent = $(this).parent();





     e.stopPropagation();
 });
 //Long text widget

 $(document).on('keyup', '.component-textbox-longtext', function(event) {

     $boardid = $("#boardhid").val();
     $pulseitemid = $(this).attr("id");
     $pulsevalue = $(this).val();
     $.ajax({
         type: "post",
         url: "widget/saveContent",
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         data: { "boardid": $boardid, "pulseitemid": $pulseitemid, "pulsevalue": $pulsevalue, "component": "longtext" },

         success: function(response, event) {

         }
     });

 });

 $(document).on('keydown', '.component-textbox-longtext', function(event) {
     var keycode = (event.keyCode ? event.keyCode : event.which);

     if (keycode == '13') {
         $value = $(this).val();
         var dad = $(this).parent();
         $(this).hide();
         dad.html($value);
     }
 });

 $(document).on('blur', '.component-textbox-longtext', function(event) {
     var dad = $(this).parent();
     $value = $(this).val();
     dad.html($value);

 });

 $(document).on('click', 'body', function(event) {

     if ($('#newDiv1').is(':visible')) {

         $('#newDiv1').remove();


     }


 });

 $(document).on('click', '.boardlabels', function() {
     var boardid = $('#boardhid').val();
     var pulseid = $("#currentpulse").val().replace("pulse-", "");
     var componentid = $("#currcomponentval").val();


     $choosenstatus = $(this).html();

     var self = this;
     $.ajax({
         type: "post",
         url: "template/boardStatuschange",
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         data: { "boardid": boardid, "pulseid": pulseid, "componentid": componentid, "choosenstatus": $choosenstatus },

         success: function(response) {
             //alert(response);  
             //$(self).parent().html(response);
             //alert($(self).parent().parent().html());

             var color = $(self).css("background-color");


             var style = $(self).attr("style");
             $("#component" + componentid).css("background-color", color);

             // $(self).parent().parent().html('<a style="cursor: pointer" class="comments" rel="comments" title="Enter comments">'+response+'</a>');
             $("#component" + componentid).html('<a style="cursor: pointer;" class="comments" rel="comments" title="Enter comments">' + response + '</a>');
             $(".greet").hide();
         }
     });
 })



 //Long text widget





 function updatePulse(ths, pulseid, groupid) {
     $boardid = $("#boardhid").val();
     $value = $(ths).val();
     $.ajax({
         type: "post",
         url: "template/postEditpulse",
         data: { board: $boardid, pk: pulseid, groupid: groupid, value: $value },
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(response) {
             var dad = $(ths).parent().parent();
             dad.find('span').html($value);

             $(ths).keypress(function(ev) {
                 var keycode = (ev.keyCode ? ev.keyCode : ev.which);
                 if (keycode == '13') {
                     editpulseblur(ths, pulseid);
                 }
             });

         }
     });
 }

 function remPulse(pulseid) {
     var boardid = $("#boardhid").val();
     $.ajax({
         type: "post",
         url: "template/managePulse",
         data: { boardid: boardid, pulseid: pulseid, process: "delete" },
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(response) {
             chooseBoard(boardid);
         }
     });
 }


 function setBoardRowsandColumns(groupid) {

     /*
         $.ajax({
             type: "get",
             async: true,
             url: "template/setBoardRowsandColumns",
             data:{groupid:groupid},
             headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             success:function(response)
             { 

             }});
     */

 }


 function addBoardcolumn(columnid) {
     $boardid = $("#boardhid").val();
     $.ajax({
         type: "post",
         async: true,
         url: "columns/newBoardColumn",
         data: { columnid: columnid, boardid: $boardid },
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(response) {
             chooseBoard($boardid);
         }
     });
 }


 /**------------------------------Athira's Works-------------------------------------------  */
 // function toggle(modalstate, id) {
 //     alert(modalstate);
 //     if (modalstate == "add") {
 //         $('#myModal').modal('show');
 //     }
 // }
 function getremainingColumns(crntBoard, selectedBoard, gpid) {
     $.ajax({
         type: "get",
         async: true,
         url: "template/getremainingColumns",
         data: { boardid: crntBoard, selectedboard: selectedBoard, gpid: gpid },
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(data) {
             if (data) {
                 //boardid
                 document.getElementById("result").style.display = "block";
                 //console.log('Here');
                 $('#result').html('');
                 var columns = data['additionalColums']

                 document.getElementById("groupid").value = data['gpid'];
                 document.getElementById("boardid").value = data['boardid'];
                 document.getElementById("txtselboard").value = data['selboard'];
                 $('#result').append('<h5>Move group to ' + data['boardname'] + '</h5>');
                 if (columns.length > 0) {
                     for (i = 0; i < columns.length; i++) {
                         $('#result').append('<div><input type=text name=columnname id=columnname style=border:none value=' + columns[i] + '></div>');
                     }
                 } else {
                     $('#result').append('<div><input type=text name=columnname id=columnname style=border:none value=""></div>');
                 }
             }

             $('#myModal').modal('show');
         }
     });
 }

 function cancelmovement(boardid) {
     $('#myModal').modal('hide');
 }

 function moveGrouptoBoard(boardid, selectedboardid, group) {
     //alert(boardid + "," + selectedboardid + "," + columns + "," + group)
     if (boardid && selectedboardid && group) {
         $.ajax({
             type: "post",
             async: true,
             url: "template/moveGrouptoBoard",
             data: { boardid: boardid, selectedboardid: selectedboardid, group: group },
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             success: function(response) {
                 chooseBoard(boardid);

                 $('#myModal').modal('hide');
                 $('body').removeClass().removeAttr('style');
                 $('.modal-backdrop').remove();
             }
         });
     }

 }

 function addGroup(boardid) {

     $.ajax({
         type: "post",
         async: true,
         url: "template/addGroup",
         data: { boardid: boardid },
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(response) {
             chooseBoard(boardid);
         }
     });
 }

 function duplicateGroup(boardid, groupid) {
     //alert(boardid + "," + groupid);
     $.ajax({
         type: "post",
         async: true,
         url: "template/duplicateGroup",
         data: { boardid: boardid, groupid: groupid },
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(response) {
             chooseBoard(boardid);
             //  location.reload();
         }
     });
 }

 function addcolumn(columntext, columnid, catId, themeId) {
     //alert(columntext + "," + columnid + "," + catId + "," + themeId);
     $.ajax({
         type: "post",
         async: true,
         url: "/mondaymaster/admin/newThemeColumn",
         data: { id: columnid, columntext: columntext, catId: catId, themeId: themeId },
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(response) {
             location.reload();
         }
     });
 }

 function updateTemplateColumns(themeid, catId, oldColValue, columntext) {
     // alert(themeid + "," + oldColValue + "," + columntext);
     $.ajax({
         type: "post",
         async: true,
         url: "/mondaymaster/admin/updateTemplateColumn",
         data: { themeid: themeid, catId: catId, oldColValue: oldColValue, columntext: columntext },
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(response) {
             location.reload();
         }
     });
 }

 function updateTemplateColRows(themeid, catid, rowvalue, oldrowvalue, row, gpname) {
     // alert(themeid + ',' + catid + ',' + rowvalue + ',' + oldrowvalue + "," + row + "," + gpname);
     $.ajax({
         type: "post",
         async: true,
         url: "/mondaymaster/admin/updateTemplateRowValues",
         data: { themeid: themeid, catid: catid, oldrowvalue: oldrowvalue, rowvalue: rowvalue, row: row, gpname: gpname },
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function(response) {
             //  location.reload();
         }
     });
 }