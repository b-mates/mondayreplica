<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminLoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
   
    public function test_user_can_view_a_login_form()
    {
        $response = $this->get('/mondaymaster/login');
        $response->assertSuccessful();
        $response->assertViewIs('mondaymaster.pages.login');
    }
}
