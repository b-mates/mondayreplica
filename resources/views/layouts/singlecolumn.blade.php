<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
  <head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>77</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}"/>

    <!-- Bootstrap -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <![endif]-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" >
    <link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet">
     <link href="{{ asset('css/mystyle.css') }}" type="text/css" rel="stylesheet">

      <script src="{{ asset('js/jquery.min.js') }}"  type="text/javascript"></script> 
      <script src="{{asset('js/Popper.js')}}" type="text/javascript"></script>
      <script src="{{ asset('js/bootstrap.min.js') }}"  type="text/javascript"></script>
      <script src="{{ asset('js/app.js') }}"  type="text/javascript"></script> 
      <script src="{{ asset('js/bootstrap.min.js') }}"  type="text/javascript"></script>  
      <script src="{{ asset('js/app.js') }}"  type="text/javascript"></script>  
      <script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
      <script src="{{ asset('js/scripts.js') }}"  type="text/javascript"></script>  
      <script src="{{asset('js/custom.js')}}" type="text/javascript"></script> 

      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     

  </head>

</head>
<body>
<div class="container-fluid menu">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light p-0"> <a class="navbar-brand" href="/"><img src="{{ asset('images/Logo.jpg')}}" alt=""/></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse Menu-content" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto ">
          <li class="nav-item"> <a class="nav-link" href="#">Products</a> </li>
          <li class="nav-item "> <a class="nav-link" href="#">Stories</a> </li>
          <li class="nav-item"> <a class="nav-link" href="#">Pricing</a> </li>
          <li class="nav-item"> <a class="nav-link" href="#">Careers</a> </li>
              @if (Auth::check())
                <li class="nav-item"> <a class="nav-link" href="profile">Home</a> </li>
              @else
                @if($subdomainname!="")
                <li class="nav-item"> <a class="nav-link" href="/login">Login</a> </li>
                @else
                 <li class="nav-item"> <a class="nav-link" href="http://{{env('APP_URL')}}/signup/step1">Register</a> </li>
                @endif
              @endif
          
        </ul>
      </div>
    </nav>
  </div>
</div>

<div id="main" class="row">
    
    @yield('content')
    @yield('script')
</div>
<div class="container-fluid footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 mt-4">
          <ul class="list-unstyled f-logo">
            <li><img src="{{ asset('images/Logo.png') }}" width="167" height="61" alt=""/></li>
            <li class="mb-2"><img src="{{ asset('images/google-play.jpg') }}" width="110" height="36" alt=""/></li>
            <li><img src="{{ asset('images/Applestore.jpg') }}" width="111" height="39" alt=""/></li>
          </ul>
        </div>
        <div class="col-sm-9 mt-5 f-content">
          <div class="row">
            <div class="col-md-3 col-6">
              <ul class="list-unstyled">
                <li>Our product</li>
                <li>Product</li>
                <li>Stories</li>
                <li>Pricing</li>
                <li>Partners/Affiliates</li>
                <li>Find a partner</li>
                <li>Templates</li>
                <li>Integrations</li>
                <li>Formerly dapulse</li>
                <li>Developers</li>
              </ul>
            </div>
            <div class="col-md-3 col-6">
              <ul class="list-unstyled">
                <li>About Us</li>
                <li>Contact Us</li>
                <li>Careers</li>
                <li>In The News</li>
                <li>Press Kit</li>
                <li>Blog</li>
              </ul>
            </div>
            <div class="col-md-3 col-6">
              <ul class="list-unstyled">
                <li>Client management</li>
                <li>Bug tracking</li>
                <li>Project design</li>
                <li>Lead tracking</li>
                <li>Task management</li>
                <li>All use cases</li>
              </ul>
            </div>
            <div class="col-md-3 col-6">
              <ul class="list-unstyled">
                <li>Daily Webinars</li>
                <li>Guides</li>
                <li>Support</li>
                <li>Security</li>
                <li>ISO 27001 / 27018</li>
                <li>SOC 2</li>
                <li>GDPR Ready</li>
                <li>Legal, Security & Privacy</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid s-footer">
    <div class="container">
      <div class="row social">
        <div class="col-sm-6 ">
          <p>2018 Seventyseven, Inc. All rights reserved.</p>
        </div>
        <div class="col-sm-6">
          <ul class="list-inline float-right">
            <li class="float-left pr-2"><i class="fab fa-facebook-square"></i></li>
            <li class="float-left pr-2"><i class="fab fa-twitter-square"></i></li>
            <li class="float-left pr-2"><i class="fab fa-linkedin"></i></li>
            <li class="float-left pr-2"><i class="fab fa-google-plus-square"></i></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  
  
 
  </body>
  </html>


