<div class="fix-header fix-sidebar card-no-border">
    <div id="main-wrapper">
       
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <h5>All Templates</h5>
                    <ul id="sidebarnav">
                        <li class="">
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                               
                                <span class="hide-menu">Featured Templates </span>
                            </a>
                            <ul aria-expanded="false" class="collapse" style="height: 10px;">
                            
                            @if(isset($themes)) 
                            @foreach($themes as $themes)
                                <li>
                                <a href="{{route('themetemplate',$themes->id)}}" class="waves-effect waves-dark"  aria-expanded="false"><span class="hide-menu">{{ $themes->themename }}  </span></a>
                                </li>
                            @endforeach
                            @endif
                                <!-- <li>
                                    <a href="#">Main</a>
                                </li>
                                <li>
                                    <a href="#">Private</a>
                                </li>
                                <li>
                                    <a href="#">Shareable</a>
                                </li> -->
                            </ul>
                        </li>
                    </ul>

                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <!-- item-->
                <!-- <a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a> -->
                <!-- item-->
                <!-- <a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a> -->
                <!-- item--><a href="{{ route('logout') }}" class="link" data-toggle="tooltip" title="Logout"><i
                        class="mdi mdi-power"></i></a> </div>
            <!-- End Bottom points-->
        </aside>
    </div>
</div>
