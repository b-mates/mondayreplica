<div class="fix-header fix-sidebar card-no-border">
    <div id="main-wrapper">
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a class="waves-effect waves-dark" href="{{route('masterDashboard')}}"
                                aria-expanded="false"><i class="mdi mdi-gauge"></i><span
                                    class="hide-menu">Dashboard</span></a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="{{route('companyList')}}" aria-expanded="false"><i
                                    class="mdi mdi-account-check"></i><span class="hide-menu">Company</span></a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="{{route('themeCategory')}}"
                                aria-expanded="false"><i class="mdi mdi-table"></i><span
                                    class="hide-menu">Themes</span></a>
                        </li>
                       
                       

                       





                    </ul>

                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
                <!-- item-->
                <!-- <a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a> -->
                <!-- item-->
                <!-- <a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a> -->
                <!-- item--><a href="{{ route('logout') }}" class="link" data-toggle="tooltip" title="Logout"><i
                        class="mdi mdi-power"></i></a> </div>
            <!-- End Bottom points-->
        </aside>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">New Board</h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form role="form" id="boards">
                    @csrf
                    <div class="form-group">
                        <label for="inputName"></label>
                        <input type="text" class="form-control" id="newboard" placeholder="Enter Board name" />
                    </div>
                    <div class="form-group">
                        <select name="options" class="form-control" id="options">
                            <option value="2 " selected=selected">Main</option>
                            <option value="3">Private</option>
                            <option value="4">Shareable</option>
                        </select>
                    </div>
                </form>
            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary submitBtn" name="newboardsave_admin"
                    id="newboardsave_admin">Create Board</button>
            </div>
        </div>
    </div>
</div>
<script>

</script>