<header>
    <div class="container-fluid sec1-temp">
        <div class="row">
            <div class="col-sm-2"><a class="navbar-brand" href="/"><img src="{{ asset('images/Logo.jpg')}}"
                        alt="" /></a></div>
            <div class="col-sm-6 mt-4 mb-3">

            </div>
            <div class="col-sm-4 mt-4 mb-3 right-top-main">
                <ul class="list-inline float-right">
                    <li class="user-drop">
                        <div class="dropdown">
                            <button class=" dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img
                                    src="{{ asset('images/Profile.jpg')}}" alt=""> </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#"><i class="fa fa-user"></i> My Profile</a>
                                <a class="dropdown-item" href="{{ route('logout') }}"><i
                                        class="fas fa-sign-out-alt"></i> Logout</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>