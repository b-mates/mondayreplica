<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
#myUL {
    margin: 0;
    padding: 0;
}

.caret {
    cursor: pointer;
    -webkit-user-select: none;
    /* Safari 3.1+ */
    -moz-user-select: none;
    /* Firefox 2+ */
    -ms-user-select: none;
    /* IE 10+ */
    user-select: none;
    font-family: sans-serif;
    font-style: normal;
}

.caret::before {
    content: "\25B6";
    color: black;
    display: inline-block;
    margin-right: 8px;

}

.caret-down::before {
    -ms-transform: rotate(90deg);
    /* IE 9 */
    -webkit-transform: rotate(90deg);
    /* Safari */
    '
transform: rotate(90deg);
}

.nested {
    display: none;
}

.active {
    display: block;
}
</style>

<div class="side-left">
    <ul class="nav nav-pills flex-column" id="myUL">
        <li class="nav-item sub-item">
            <a class="nav-link" href="{{route('masterDashboard')}}" data-type="0">
                <i class="fa fa-folder fa-fw"></i>Dashboard
            </a>
            <a class="nav-link" href="{{route('companyList')}}" data-type="0">
                <i class="fa fa-folder fa-fw"></i>Company
            </a>
            <a class="nav-link" href="{{route('themeCategory')}}" data-type="0">
                <i class="fa fa-folder fa-fw"></i>Themes
            </a>
            <a class="nav-link" data-type="0">
                <i class="caret">Master Entry
                    <ul class="nested">
                        <li>Water</li>
                        <li>Coffee</li>
                    </ul>
                </i>
            </a>
        </li>
    </ul>
</div>
</div>
<script>
var toggler = document.getElementsByClassName("caret");
var i;
for (i = 0; i < toggler.length; i++) {
    toggler[i].addEventListener("click", function() {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
    });
}
</script>