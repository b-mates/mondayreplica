@extends('mondaymaster.layouts.submain')
@section('content')
@extends('mondaymaster.layouts.sidebar')
@extends('mondaymaster.layouts.footer')
<div class="inbox" id="inbox">
    <div class="sec-inbox">
        <div class="col-xs-12 ">
            <div class="border-inbox" style="width:1080px;">
                <div class="tabs" style="width:100%">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#Companies" role="tab"
                                aria-controls="Companies">Companies</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#Users" role="tab" aria-controls="Users"
                                id="li2">Users</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="Companies" role="tabpanel">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Company</th>
                                            <th>Strength</th>
                                            <th>Workarea</th>
                                            <th>Plan</th>
                                            <th>Created by</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ($companyDatas)
                                        @foreach($companyDatas as $index => $companyDatas)
                                        <tr>
                                            <td>{{ $index +1 }}</td>
                                            <td><a
                                                    href="{{route('usersList',$companyDatas->id)}}">{{ $companyDatas->companyname }}</a>
                                            </td>
                                            <td>{{ $companyDatas->teamstrength }}</td>
                                            <td>{{ $companyDatas->workarea }}</td>
                                            <td>{{ $companyDatas->planname }}</td>
                                            <td>{{ $companyDatas->name }}</td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="Users" role="tabpanel">
                            <h4>Users</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Company</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($userList))
                                        @foreach($userList as $index => $userList)
                                        <tr>
                                            <td>{{ $index +1 }}</td>
                                            <td>{{ $userList->name }}</td>
                                            <td>{{ $userList->email }}</td>
                                            <td>{{ $userList->companyname }}</td>
                                            <td>@if($userList->status =='1') Active @else Inactive @endif</td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
    @section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">   
    $(document).ready(function() {   
        if (window.location.href.indexOf("Users") > -1) {
            target = 'Users';
            $('.nav a').filter('[href="#' + target + '"]').class = 'nav-link';
            $('.nav a').filter('[href="#' + target + '"]').tab('show');
            $("#li2").removeClass("disabled"); // for 3rd li enable 
        } else {
            target = 'Users';
            $('.nav a').filter('[href="#' + target + '"]').class = 'nav-link disabled';
            $("#li2").addClass("disabled"); // for 2nd li disable  
        }
        $('#mytab').on("click", "li", function(event) {
            var activeTab = $(this).find('a').attr('href').split('-')[1];
            alert(activeTab);
            FurtherProcessing(activeTab);
        });
    });
    </script>
    @stop