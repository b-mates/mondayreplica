@extends('mondaymaster.layouts.submain')
@section('content')
@extends('mondaymaster.layouts.sidebar')
@extends('mondaymaster.layouts.footer')
@php
if(isset($group))
{
$rowCnt=count($group);
//echo $rowCnt;
}
@endphp
<div class="inbox" id="inbox">
    <div class="sec-inbox">
        <div class="col-xs-12 ">
            <nav id="myNavbar" class="navbar navbar-default" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <a class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
                            href="#">New</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav ">
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">Columns</a>
                                <ul class="dropdown-menu">
                                    @if(isset($colTypes))
                                    @foreach($colTypes as $cols)
                                    <li onclick="addcolumn('{{$cols["columnname"]}}','{{$boardid}}','{{$cols["id"]}}','{{$rowCnt}}')"
                                        style="cursor: pointer;color:red;">
                                        {{$cols["columnname"]}}</li>
                                    </p>
                                    @endforeach
                                    @endif
                                </ul>
                            </li>
                            <!-- <li onclick="addRows('{{$boardid}}','{{$rowCnt}}')" style="cursor:pointer;">Rows</li> -->
                        </ul>
                    </div>
                </div>
            </nav>

            <!-- @foreach ($boardgroups as $boardgroupsitem)
            <div id="{{$boardgroupsitem["id"]}}" class="table-t">
                <h2>{{$boardgroupsitem["grouptitle"]}}<span>

                    </span></h2>
            </div>
            <div class="table-t">

                <div class="row" id="{{$boardgroupsitem["id"]}}-row">
                    <div class="col-sm"></div>
                    @php
                    $i=0;
                    foreach($group as $subgroup)
                    {
                    echo '<div class="col-sm ">
                        <h5>'.$subgroup[$i]->title.'</h5>
                    </div>';
                    $i++;
                    }
                    @endphp
                    <div class="col-sm">
                        <div class="btn-group">
                            <input type="hidden" name="boardhid" id="boardhid" value="{{$boardid}}" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm"></div>
                    @php
                    $i=0;
                    $mainid=$boardgroupsitem["id"];
                    $i=0;
                    foreach($grouprows[$mainid] as $grouprw)
                    {
                    $i++;
                    @endphp
                    <div class="col-sm">{{$grouprw["title"]}}</div>
                    @php
                    if($i%2==0)
                    {
                    echo '<div class="col-sm"></div>
                </div>
                <div class="row">
                    <div class="col-sm"></div>';
                    }
                    }
                    @endphp
                </div>
            </div>
            @endforeach -->



            <table align="center" cellpadding="0" cellspacing="0" width="100%;" border="1">
                <tr>
                    <td>
                        <table style="width:100%;">
                           
                            @foreach ($boardgroups as $boardgroupsitem)
                            <tr>
                                <td>
                                    <h2>{{$boardgroupsitem["grouptitle"]}} {{$boardgroupsitem["id"]}}</h2>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" id="tbl_table" name="tbl_table"
                                        class=" table order-list">
                                        <thead>
                                            @php
                                            foreach($group as $subgroup){
                                             
                                            @endphp

                                            <tr class="tr_Col">
                                                @php
                                                for($j=0;$j < count($subgroup);$j++){
                                                    echo $boardgroupsitem["id"].",".$subgroup[$j]->groupid."<br/>";
                                                    if($boardgroupsitem["id"]==$subgroup[$j]->groupid){ @endphp
                                                    <td style="width:180px;text-align:center;">
                                                        {{$subgroup[$j]->title}}{{$subgroup[$j]->groupid}}
                                                    </td>
                                                    @php }
                                                    }
                                                    @endphp
                                            </tr>
                                            @php
                                            }
                                            @endphp
                                        </thead>
                                        <tbody>
                                            <tr class="tr_row">
                                                @php
                                                $i=0;
                                                $mainid=$boardgroupsitem["id"];
                                                
                                                foreach($grouprows[$mainid] as $grouprw)
                                                { $i++;
                                                @endphp
                                                <td id="div_row" style="text-align:center;">{{$grouprw["title"]}}
                                                    {{$boardgroupsitem["id"]}}</td>
                                                @php

                                                if($i%2==0)
                                                {
                                                echo '<div class="col-sm"></div>
                                            </tr>
                                            <div class="row">
                                                <div class="col-sm"></div>';
                                                }

                                                }
                                                @endphp
                                            </div>    
                                        </tbody>
                                        </thead>
                                    </table>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    @endsection
    @section('script')
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript">
    </script>
    @stop