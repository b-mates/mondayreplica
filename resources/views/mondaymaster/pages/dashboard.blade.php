@extends('mondaymaster.layouts.submain')
@section('content')
@extends('mondaymaster.layouts.sidebar')
@extends('mondaymaster.layouts.footer')
<h5>Dashboard</h5>
<div class="inbox" id="inbox">
    <div class="sec-inbox">
        <div class="col-xs-12">
            <div class="border-inbox">
                <div class="col-xs-12 intro-hd">
                    <ul class="list-inline">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                        Welcome to Admin Dashboard ...
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="preloader-wrapper">
    <div class="preloader">
        <img src="{{asset('images/preloader.gif')}}" alt="preloader()">
    </div>
</div>
@stop
@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script>

</script>
@endsection