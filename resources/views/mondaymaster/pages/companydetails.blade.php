@extends('mondaymaster.layouts.app')
@include('mondaymaster.layouts.doublecolumn')
@section('content')
@include('mondaymaster.layouts.dashheader')
@include('mondaymaster.layouts.sidemenu')

<div class="inbox" id="inbox">
    <div class="sec-inbox">
        <div>
            <ul>
                <h3>Company Details </h3>
            </ul>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST"  action="{{ route('updateCompany') }}">
                                @csrf
                                <div class="form-group row">
                                <input type="hidden" name="hdncompanyid" id="hdncompanyid" value="{{$companyDatas[0]->id}}"/>
                                    <label for="companyname"
                                        class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="companyname" name="companyname" type="text" 
                                            class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" 
                                            value="{{$companyDatas[0]->companyname}}"
                                            required autofocus>

                                        @if ($errors->has('companyname'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('companyname') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="totalstrength"
                                        class="col-md-4 col-form-label text-md-right">{{ __('Total Strength') }}</label>

                                    <div class="col-md-6">
                                        <input id="totalstrength" name="totalstrength" type="text" 
                                            class="form-control{{ $errors->has('companyname') ? ' is-invalid' : '' }}"
                                            value="{{$companyDatas[0]->teamstrength}}" required>

                                        @if ($errors->has('totalstrength'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('totalstrength') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="workarea"
                                        class="col-md-4 col-form-label text-md-right">{{ __('Work Area') }}</label>

                                    <div class="col-md-6">
                                        <select class="form-control" name="workarea" id="workarea">
                                            <option value="" selected>What does your team do?</option>
                                            @foreach ($workarea as $workarea)
                                            <option <?php if($companyDatas[0]->workid == $workarea->id) { echo "selected";} ?> value="{{ $workarea->id }}">
                                                {{ $workarea->workarea }}</option>
                                            @endforeach
                                        </select>

                                        @if ($errors->has('workarea'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('workarea') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="plan"
                                        class="col-md-4 col-form-label text-md-right">{{ __('Plan') }}</label>

                                    <div class="col-md-6">
                                        <select class="form-control" name="plan" id="plan">
                                            <option value="" selected>What does your team do?</option>
                                            @foreach ($plans as $plans)
                                            <option value="{{ $plans->id }}" <?php if($companyDatas[0]->planid == $plans->id) { echo "selected";} ?>>
                                                {{ $plans->planname }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('plan'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('plan') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Update Company Details') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection