@extends('mondaymaster.layouts.submain')
@section('content')
@extends('mondaymaster.layouts.sidebar')
@extends('mondaymaster.layouts.footer')
<div class="inbox" id="inbox">
    <div class="sec-inbox">
        <div class="col-xs-12 ">
            <div class="border-inbox" style="width:1100px;">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Featured Templates</a></li>
                            <li class="breadcrumb-item active">{{$theme->themename}}</li>
                        </ol>
                    </div>

                </div>
                <div class="col-xs-12 intro-hd">
                    <ul class="list-inline">
                       {{$theme->themetemplate}}
                    </ul>
                </div>
            </div>
        </div>
        @endsection