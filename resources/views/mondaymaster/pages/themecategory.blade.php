@extends('mondaymaster.layouts.submain')
@section('content')
@extends('mondaymaster.layouts.sidebar')
@extends('mondaymaster.layouts.footer')

<div class="inbox" id="inbox">
    <h5>Themes</h5>
    <div class="sec-inbox">
        <div class="col-xs-12 ">
            <div class="border-inbox" style="width:1080px;">
                <div class="tabs" style="width:80%">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#categories" role="tab"
                                aria-controls="categories">Category</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#themes" role="tab" aria-controls="themes"
                                id="li2">
                                Themes</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="categories" role="tabpanel">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Category</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($themecategory))
                                        @foreach($themecategory as $index => $themecategory)
                                        <tr>
                                            <td>{{ $index +1 }}</td>
                                            <td>
                                                <a href="{{route('themeList',$themecategory->id)}}">
                                                    {{$themecategory->themecategory}}</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="themes" role="tabpanel">
                            <div class="tab-pane" id="themes" role="tabpanel">
                                @if(isset($themeid))
                                <nav id="myNavbar" class="navbar navbar-default" role="navigation">
                                    <div class="container">
                                        <div class="navbar-header">
                                            <a class="navbar-toggle" data-toggle="collapse"
                                                data-target="#bs-example-navbar-collapse-1" href="#">New</a>
                                        </div>
                                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                            <!-- <ul class="nav navbar-nav ">
                                                <li class="dropdown"> -->
                                                    <!-- <a href="#" data-toggle="dropdown"
                                                        class="dropdown-toggle">Columns</a> -->
                                                
                                                    <ul class="nav navbar-nav">
                                                        @if(isset($colTypes))
                                                        @foreach($colTypes as $cols)
                                                        <li onclick="addcolumn('{{$cols ["columnname"]}}','{{$cols["id"]}}','{{$catId}}','{{$themeid}}')" style="cursor: pointer;color:red;">
                                                            {{$cols["columnname"]}}</li>
                                                        </p>
                                                        @endforeach
                                                        @endif
                                                    </ul>
                                                <!-- </li>
                                            </ul> -->
                                        </div>  
                                    </div>
                                </nav>
                                <table>
                                        <tr>
                                            <td>
                                                <table>
                                                    @php
                                                  //  dd($themeDesign);
                                                    foreach($groupsarray as $gpvalues)
                                                    {
                                                        $groupnames=$gpvalues;
                                                        $columnsValue=(array)$themeDesign[$groupnames]["pulses"];
                                                        $headingarray=$themeDesign['heading'];
                                                        $colcount=count($headingarray);
                                                    @endphp
                                                    <tr>
                                                            <td>
                                                                <h2>{{$groupnames}}</h2>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <thead>
                                                                            <tr class="colTr">
                                                                                <td style="width:214px;"></td>
                                                                                @php
                                                                                $index=0;
                                                                                $divCnt=count($headingarray);
                                                                                foreach($headingarray as $columsTitle)
                                                                                {
                                                                                @endphp
                                                                                <td style="width:180px;text-align:center;" contenteditable="true">
                                                                                <input type="text" name="txtCol_{{$index}}" id="txtCol_{{$index}}" value="{{$columsTitle}}" style="border:none;text-align:center;"
                                                                                onchange="updateTemplateColumns('{{$themeid}}','{{$catId}}','{{$columsTitle}}',this.value)"/>                                                            
                                                                                </td>
                                                                                @php
                                                                                $index++;
                                                                                }
                                                                                @endphp
                                                                            </tr>
                                                                            <tr class="rowTr">
                                                                                <td style="width:214px;"></td>
                                                                                @php
                                                                                $coli=0;
                                                                                $colcount=2;
                                                                                for($i=0;$i<$colcount;$i++)
                                                                                {
                                                                                   // echo $i."<br/>";
                                                                                    $pulses=$columnsValue[$i];
                                                                                    $pulsescontent=(array)$themeDesign[$groupnames]["pulses"][$pulses];
                                                                                    $colcnt=count($pulsescontent);
                                                                               // $valarray=(array)$sampleval;
                                                                               for($j=0;$j<$colcnt;$j++)
                                                                                {
                                                                                   // echo $i."<br/>";
                                                                                    $colcontent=$pulsescontent[$j];
                                                                                $coli++;
                                                                                @endphp
                                                                                <td style="text-align:center;" contenteditable="true">
                                                                                <input type="text" name="txtrow_{{$coli}}" id="txtrow_{{$coli}}" value="{{$colcontent}}" style="border:none;text-align:center;"
                                                                                onchange="updateTemplateColRows('{{$themeid}}','{{$catId}}','{{$colcontent}}',this.value,{{$i}},'{{$groupnames}}')"/>
                                                                                </td>
                                                                                @php
                                                                                if($coli%$divCnt==0)
                                                                                {
                                                                                echo '
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                ';
                                                                                }
                                                                                }
                                                                                }
                                                                                @endphp
                                                                            </tr>
                                                                        </thead>
                                                                    </table>
                                                                </td>
                                                            </tr>                                                       
                                                       
                                                    @php
                                                    }
                                                    @endphp
                                                </table>
                                            </td>
                                        </tr>
                                </table>
                                @else
                                <div class="rTable table table-bordered">
                                    <div class="table-responsive">

                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Description</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(isset($themes))
                                                @foreach($themes as $index => $themes)
                                                <tr>
                                                    <td>{{ $index +1 }}</td>
                                                    <td>{{ $themes->themename }}</td>
                                                    <td>{{ $themes->description }}</td>
                                                    <td>
                                                        <a
                                                            href="{{route('themeLists',['catId'=>$catId,'theme'=>$themes->id])}}">
                                                            <img src="{{asset('admin/images/view.gif')}}" /></a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
    @section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        var url = window.location.href;
        var segment = url.split("/").length - 1 - (url.indexOf("http://") == -1 ? 0 : 2);
        //  alert(segment);
        if (window.location.href.indexOf("themes") > -1) {
            target = 'themes';
            $('.nav a').filter('[href="#' + target + '"]').class = 'nav-link';
            $('.nav a').filter('[href="#' + target + '"]').tab('show');
            $("#li2").removeClass("disabled"); // for 3rd li enable 
        } else if (window.location.href.indexOf("themes") == -1) {
            if (segment >= 4) { //alert('111');
                target = 'themes';
                $('.nav a').filter('[href="#' + target + '"]').class = 'nav-link';
                $('.nav a').filter('[href="#' + target + '"]').tab('show');
                $("#li2").removeClass("disabled"); // for 3rd li enable 
            } else {
                //alert('2222);')
                target = 'themes';
                $('.nav a').filter('[href="#' + target + '"]').class = 'nav-link disabled';
                $("#li2").addClass("disabled"); // for 2nd li disable           
            }

        } else {
            target = 'themes';
            $('.nav a').filter('[href="#' + target + '"]').class = 'nav-link disabled';
            $("#li2").addClass("disabled"); // for 2nd li disable           
        }


        // $('#mytab').on("click", "li", function(event) {
        //     var activeTab = $(this).find('a').attr('href').split('-')[1];
        //     alert(activeTab);
        //     FurtherProcessing(activeTab);
        // });
    });
    </script>
    @stop