@extends('mondaymaster.layouts.submain')
@section('content')
@extends('mondaymaster.layouts.sidebar')
@extends('mondaymaster.layouts.footer')
<div class="inbox" id="inbox">
    <h5>Template Design</h5>
    <div class="sec-inbox">
        <div class="col-xs-12 ">
            <div class="border-inbox" style="width:1080px;">
                <nav id="myNavbar" class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <a class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
                                href="#">New</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav ">
                                <li class="dropdown">
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Columns</a>
                                    <ul class="dropdown-menu">
                                        @if(isset($colTypes))
                                        @foreach($colTypes as $cols)
                                        <li onclick="" style="cursor: pointer;color:red;">
                                            {{$cols["columnname"]}}</li>
                                        </p>
                                        @endforeach
                                        @endif
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <table align="center" cellpadding="0" cellspacing="0" width="100%;">
                    <tr>
                        <td>
                            <table style="width:100%;">
                                @php
                                // dd($MainTemplateTitle);
                                for($i=0;$i<$tempDesignCount;$i++) { $colarray=(array)$themeDesign[$i];
                                    $columnsValue=(array)$colarray["columnssample"];
                                    $columsarray=$colarray["columns"];
                                  
                                     @endphp <tr>
                        <td>
                            <h2>{{$colarray["title"]}}</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <td style="width:214px;"></td>
                                        @php
                                        //dd($groupcoltitle);
                                        foreach($columsarray as $columsTitle)
                                        {
                                        @endphp
                                        <td style="width:180px;text-align:center;">
                                            {{$columsTitle}}
                                        </td>
                                        @php
                                        }
                                        @endphp
                                    </tr>

                                    <tr>
                                        <td style="width:214px;"></td>
                                        @php
                                        $coli=0;
                                        foreach($columnsValue as $sampleval)
                                        {
                                        $valarray=(array)$sampleval;
                                        foreach($valarray as $clvalue)
                                        {
                                        $coli++;
                                        @endphp
                                        <td style="text-align:center;">{{$clvalue}}</td>
                                        @php
                                        if($coli%2==0)
                                        {
                                        echo '
                                    </tr>
                                    <tr>
                                        <td></td>
                                        ';
                                        }
                                        }
                                        }
                                        @endphp
                                    </tr>
                                </thead>
                            </table>
                        </td>
                    </tr>
                    @php
                    }
                    @endphp
                </table>
                </td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection