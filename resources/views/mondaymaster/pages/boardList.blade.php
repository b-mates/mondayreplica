@extends('mondaymaster.layouts.submain')
@section('content')
@extends('mondaymaster.layouts.sidebar')
@extends('mondaymaster.layouts.footer')
<div class="inbox" id="inbox">
    <div class="sec-inbox">
        <div class="col-xs-12 ">
            <div class="border-inbox" style="width:1100px;">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Boards Types</a></li>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>

                </div>
                <div class="col-xs-12 intro-hd">
                    <ul class="list-inline">
                        <div class="table-responsive">
                            <table class="table" style ="width:80%;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Board Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($boardList))
                                    @foreach($boardList as $index => $value)
                                    <tr>
                                        <td>{{ $index +1 }}</td>
                                        <td><a href="{{ route('boardlists', [$value->id, 0]) }} "> {{ $value->boardname }}</a></td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
        @endsection