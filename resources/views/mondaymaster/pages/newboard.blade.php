@extends('mondaymaster.layouts.submain')
@section('content')
@extends('mondaymaster.layouts.sidebar')
@extends('mondaymaster.layouts.footer')

<div class="inbox" id="inbox">
    <h5>New Board</h5>
    <div class="sec-inbox">
        <div class="col-xs-12 ">
            <div class="border-inbox" style="width:1080px;">
                <form method="POST" action="{{ route('saveboard') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Board Name</label>

                        <div class="col-md-6">
                            <input id="boardname" name="boardname" type="text"
                                class="form-control{{ $errors->has('boardname') ? ' is-invalid' : '' }}" name="name"
                                value="{{ old('boardname') }}" required autofocus style="width:214px;">

                            @if ($errors->has('boardname'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('boardname') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right"></label>

                        <div class="col-md-6">
                            <!-- <div><input type="radio" name="rdo_option" value="2">Main</div>
                            <div><input type="radio" name="rdo_option" value="3">Private</div>
                            <div><input type="radio" name="rdo_option" value="4">Shareable</div> -->
                            <select name="options" name="options"  id="options" class="form-control{{ $errors->has('options') ? ' is-invalid' : '' }}"
                                    value="{{ old('options') }}" required style="width:214px;">
                                <option value="2 "selected=selected">Main</option>
                                <option value="3">Private</option>
                                <option value="4">Shareable</option>
                            </select>

                            @if ($errors->has('options'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('options') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Create Board
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection