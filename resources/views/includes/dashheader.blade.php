<header>
    <div class="container-fluid sec1-temp">
      <div class="row">
        <div class="col-sm-2"><a class="navbar-brand" href="/"><img src="images/Logo.png" alt=""/></a></div>
        <div class="col-sm-6 mt-4 mb-3">
          <ul class="list-inline ico-top-head">
            <li class="float-left"><i class="fas fa-bell"></i></li>
            <li class="float-left"><i class="fas fa-user-alt"></i></li>
            <li class="float-left" style="width:50%;">
              <input type="text" placeholder="Search Everything.." name="search">
              <span><i class="fas fa-search"></i></span></li>
          </ul>
        </div>
        <div class="col-sm-4 mt-4 mb-3 right-top-main">
          <ul class="list-inline float-right">
            <li class="float-left"><a  data-toggle="modal" onclick="openNavinvite()"><i class="fas fa-plus"></i>Invite Team Members</a></li>
            <li class="float-left">
              <button type="button" class="btn btn-primary">Upgrade</button>
            </li>
            <li class="user-drop">
              <div class="dropdown">
                <button class=" dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img src="images/5257404-dapulse_black.png" alt=""> </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> <a class="dropdown-item" href="#"><i class="fa fa-user"></i> My Profile</a> <a class="dropdown-item" href="#"><i class="fa fa-arrow-up"></i> Upgrade Account</a> <a class="dropdown-item" href="#"><i class="fa fa-wrench"></i>Admin</a> <a class="dropdown-item" href="#"><i class="fa fa-plug"></i> Intergration</a> <a class="dropdown-item" href="#"><i class="fas fa-recycle"></i>Recycle Bin</a> <a class="dropdown-item" href="#"><i class="fas fa-file-archive"></i>Archived Boards</a> <a class="dropdown-item" href="javascript:openNavinvite()"><i class="fa fa-plus"></i> Invite Team Members</a> <a class="dropdown-item" href="#"><i class="fas fa-mobile-alt"></i>Get Monday Mobile App</a> <a class="dropdown-item" href="#"><i class="fa fa-question-circle"></i> Help Center</a> <a class="dropdown-item" href="#"><i class="fas fa-keyboard"></i>Keyboard Shortcuts</a> <a class="dropdown-item" href="/logout"><i class="fas fa-sign-out-alt"></i> Logout</a> </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    
    </header>