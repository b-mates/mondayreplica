<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>77 Tech project management collaboration</title>
<meta name="csrf-token" content="{{ csrf_token() }}">


<link href="{{ asset('css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" >
<link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet">


<script src="{{ asset('js/jquery.min.js') }}"  crossorigin="anonymous"></script> 
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

</head>
<body>
<div class="container-fluid menu">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light p-0"> <a class="navbar-brand" href="/"><img src="{{ asset('images/Logo.jpg')}}" alt=""/></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse Menu-content" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto ">
          <li class="nav-item"> <a class="nav-link" href="#">Products</a> </li>
          <li class="nav-item "> <a class="nav-link" href="#">Stories</a> </li>
          <li class="nav-item"> <a class="nav-link" href="#">Pricing</a> </li>
          <li class="nav-item"> <a class="nav-link" href="#">Careers</a> </li>
              @if (Auth::check())
                <li class="nav-item"> <a class="nav-link" href="/profile">Home</a> </li>
              @else
                @if($subdomainname!="")
                <li class="nav-item"> <a class="nav-link" href="/login">Login</a> </li>
                @else
                 <li class="nav-item"> <a class="nav-link" href="http://{{env('app_url')}}/signup/step1">Register</a> </li>
                @endif
              @endif
          
        </ul>
      </div>
    </nav>
  </div>
</div>
