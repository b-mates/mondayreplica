@extends('layouts.singlecolumn')
@section('content')
<div class="container-fluid banner-sec">
    <div class="container">

      <form name="loginform" id="loginform" method="POST" action="{{route('postresetpassword')}}">
      @csrf
      <div class="row">
        <div class="col-sm-4 login mt-5">
            <h2>Forgot Password</h2>
            <p>Email address:</p>
                @if ($errors->any())
                    <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                    </div>
                  @endif

                  @if(session()->has('message'))
                      <div class="alert alert-danger">
                          {{ session()->get('message') }}
                      </div>
                  @endif
            <div class="input-group mb-3">
              <input type="text" class="form-control" id="email" name="email" value="{{$emailadd}}" placeholder="E-Mail Address"  aria-label="Recipient's username" aria-describedby="button-addon2">
            </div>
            <div class="input-group mb-3">
                <input type="password" class="form-control" id="newpassword" name="newpassword" placeholder="New password"  aria-label="Recipient's username" aria-describedby="button-addon2">
            </div>
            <div class="input-group mb-3">
                <input type="number" class="form-control" id="token" name="token" placeholder="Token"  aria-label="Recipient's username" aria-describedby="button-addon2">
            </div>
           
            <button type="button" class="btn btn-primary" name="login" id="login">Reset password</button><input type="hidden" name="sub" id="sub" value="{{$subdomain}}" />
        </div>
        <div class="col-sm-8 mr-auto mb-5 imp "><img src="{{ asset('images/desktop-banner.jpg')}}" class="w-100 mt-4" alt=""/></div>
      </div>
      </form>

    </div>
  </div>
  @section('script')
<script type="text/javascript">
  
      $("#login").click(function(){
        $("#login").attr('disabled','disabled');
        $("#loginform").submit();
      });
 
    
</script>
@stop
@endsection
