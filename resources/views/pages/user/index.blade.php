@extends('layouts.doublecolumn')
@section('content')
@include('includes.dashheader')
@include('includes.dcsm')
@include('includes.newboard')
<div class="inbox" id="inbox">

    <div class="sec-inbox">
        <div class="col-xs-12  main-title">
            <ul class="list-inline">
                <h3>Inbox </h3>
                <li><a href="#">Open(1)</a>/<a href="#">All Updates</a></li>
            </ul>
        </div>
        <div class="col-xs-12">

            <div class="border-inbox">
                <div class="col-xs-12 intro-hd">
                    <ul class="list-inline">
                        <li><img src="images/5257404-dapulse_black.png"></li>
                        <li>
                            <p>Roy Mann</p>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12 box-txt-in">
                    <p>Hi <b>{{Auth::user()->name}}</b>, </p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s</p>
                    <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It
                        has survived not only five centuries, but also the leap into electronic typesetting, remaining
                        essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
                        containing Lorem </p>

                    <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It
                        has survived not only five centuries, but also the leap into electronic typesetting, remaining
                        essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
                        containing Lorem</p>
                    <p>Roy</p>
                    <p>Sincerely</p>
                </div>
            </div>


        </div>
    </div>

    <div class="sec-inbox2">
        <div class="col-xs-12 inbox2-b">
            <div class="col-xs-12">
                <h4>Complete Your Profile</h4>
                <ul class="list-unstyled">
                    <li><span><i class="fas fa-check"></i></span>Setup Account</li>
                    <li><span><i class="fas fa-check"></i></span>Upload Your Photo</li>
                    <li><span><i class="fas fa-check"></i></span>Enable Desktop Notfications</li>
                    <li><span><i class="fas fa-check"></i></span>Invite Team Member(0/1)</li>
                    <li><span><i class="fas fa-check"></i></span>Complete Profile</li>
                    <li><span><i class="fas fa-check"></i></span>Install Our Mobile App</li>
                    <li>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                                aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="sec-inbox2">
        <div class="col-xs-12 inbox2-b">
            <div class="col-xs-12">
                <h4>Filter By Board</h4>
                <ul class="list-unstyled">
                    <li><span><i class="fas fa-check"></i></span>Vishnu</li>
                    <li><span><i class="fas fa-check"></i></span>All Uploades of vishnu</li>
                    <li><span><i class="fas fa-check"></i></span>Bookmarked Updates</li>
                </ul>
            </div>
        </div>
    </div>

</div>
<div class="preloader-wrapper">
    <div class="preloader">
        <img src="{{asset('images/preloader.gif')}}" alt="preloader()">
    </div>
</div>

@include('includes.thememenu')


@include('includes.invitemember')

@stop

@section('script')
<script type="text/javascript">
  var url = "{{asset('js/dbscripts.js')}}";
  $.getScript(url);
</script>

<script>
$(function() {
    $.get("LeftMenu", function(data) {
        $("#boardgroups").html(data);

    });

});
</script>





@endsection