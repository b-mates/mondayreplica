

<!--

<div class="main-t">

    <div class="add-new">
      <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
        <a href="#"><i class="fas fa-plus-circle" style="color:#F00;"></i></a>
        </button>
        <ul class="dropdown-menu">
          <li><a href="#"><i class="fas fa-thermometer-three-quarters"></i>Status</a></li>
          <li><a href="#"><i class="fas fa-text-width"></i>Text</a></li>
          <li><a href="#"><i class="fas fa-sort"></i>Person</a></li>
          <li><a href="#"><i class="fab fa-creative-commons-sampling"></i>Timeline</a></li>
          <li><a href="#"><i class="fas fa-calendar-alt"></i>Date</a></li>
          <li><a href="#"><i class="fas fa-hashtag"></i>Tags</a></li>
          <li><a href="#"><i class="fas fa-list-ol"></i>Numbers</a></li>
        </ul>
      </div>
    </div>
    <div class="row-file">
      <div class="row-drop">
        <div class="dropdown drop-2">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="padding:0 !important;" aria-expanded="false"> <i class="fas fa-angle-down"></i></button>
          <ul class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 34px, 0px);">
            <li><a href="#"><i class="fas fa-pencil-alt"></i>Rename Pulse</a></li>
            <li><a href="#"><i class="fas fa-arrows-alt-h"></i>Move to group</a></li>
            <li><a href="#"><i class="fas fa-sort"></i>Move to board</a></li>
            <li><a href="#"><i class="fas fa-copy"></i>Duplicate this Pluse</a></li>
            <li><a href="#"><i class="fas fa-copy"></i>Create new Pulse Above</a></li>
            <li><a href="#"><i class="fas fa-plus"></i>Create new Pulse below</a></li>
            <li><a href="#"><i class="fas fa-trash-alt"></i>Delete</a></li>
            <li><a href="#"><i class="fas fa-file-archive"></i>Archive</a></li>
          </ul>
        </div>
      </div>
      <div id="raw1" class="text-row">
        <p><a href="#">Row 1 <i class="fas fa-pencil-alt"></i></a></p>
      </div>
    </div>
    <div class="person">
      <div class="stus-head">
        <div class="dropdown">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
          <a href="#">Person</a>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fas fa-pencil-alt"></i>Edit Title</a></li>
            <li><a href="#"><i class="fas fa-arrows-alt-h"></i>Resize Column</a></li>
            <li><a href="#"><i class="fas fa-sort"></i>Sort Column</a></li>
            <li><a href="#"><i class="fas fa-draw-polygon"></i>Drag Column</a></li>
            <li><a href="#"><i class="fas fa-copy"></i>Duplicate Column</a></li>
            <li><a href="#"><i class="fas fa-plus"></i>Add Column to the Right &gt;</a></li>
            <li><a href="#"><i class="fas fa-trash-alt"></i>Delete Column</a></li>
          </ul>
        </div>
      </div>
      <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
        <span><a href="#"><img src="Images/U.jpg" alt="" width="28" height="28"></a></span>
        </button>
        <ul class="dropdown-menu">
          <li>
            <input type="text" placeholder="Search..">
          </li>
          <li style="padding-bottom:20px;"><a href="#"><img src="Images/U.jpg" alt="" width="28" height="28">vishnu</a></li>
          <li style="padding-bottom:20px;"><a href="#"><i class="fas fa-envelope"></i> Invite new team member by email</a></li>
        </ul>
      </div>
    </div>
    <div class="status">
      <div class="stus-head">
        <div class="dropdown">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
          <a href="#">Status</a>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fas fa-pencil-alt"></i>Edit Title</a></li>
            <li><a href="#"><i class="fas fa-arrows-alt-h"></i>Resize Column</a></li>
            <li><a href="#"><i class="fas fa-sort"></i>Sort Column</a></li>
            <li><a href="#"><i class="fas fa-draw-polygon"></i>Drag Column</a></li>
            <li><a href="#"><i class="fas fa-copy"></i>Duplicate Column</a></li>
            <li><a href="#"><i class="fas fa-plus"></i>Add Column to the Right &gt;</a></li>
            <li><a href="#"><i class="fas fa-trash-alt"></i>Delete Column</a></li>
          </ul>
        </div>
      </div>
      <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
        <a href="#">Status</a>
        </button>
        <ul class="dropdown-menu">
          <li class="Wornt"><a href="#">Working on it</a></li>
          <li class="pending"><a href="#">Done</a></li>
          <li class="done"><a href="#">Pending</a></li>
          <li class="Wornt"><a href="#">Stuck</a></li>
        </ul>
      </div>
    </div>
    <div class="timeline">
      <div class="stus-head">
        <div class="dropdown">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
          <a href="#">Timeline</a>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fas fa-pencil-alt"></i>Edit Title</a></li>
            <li><a href="#"><i class="fas fa-arrows-alt-h"></i>Resize Column</a></li>
            <li><a href="#"><i class="fas fa-sort"></i>Sort Column</a></li>
            <li><a href="#"><i class="fas fa-draw-polygon"></i>Drag Column</a></li>
            <li><a href="#"><i class="fas fa-copy"></i>Duplicate Column</a></li>
            <li><a href="#"><i class="fas fa-plus"></i>Add Column to the Right &gt;</a></li>
            <li><a href="#"><i class="fas fa-trash-alt"></i>Delete Column</a></li>
          </ul>
        </div>
      </div>
      <div class="date-time"> <a href="#">Set-details</a> </div>
    </div>
    <div class="date">
      <div class="stus-head">
        <div class="dropdown">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
          <a href="#">Date</a>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fas fa-pencil-alt"></i>Edit Title</a></li>
            <li><a href="#"><i class="fas fa-arrows-alt-h"></i>Resize Column</a></li>
            <li><a href="#"><i class="fas fa-sort"></i>Sort Column</a></li>
            <li><a href="#"><i class="fas fa-draw-polygon"></i>Drag Column</a></li>
            <li><a href="#"><i class="fas fa-copy"></i>Duplicate Column</a></li>
            <li><a href="#"><i class="fas fa-plus"></i>Add Column to the Right &gt;</a></li>
            <li><a href="#"><i class="fas fa-trash-alt"></i>Delete Column</a></li>
          </ul>
        </div>
      </div>
      <input type="text">
    </div>
    <div class="text">
      <div class="stus-head">
        <div class="dropdown">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
          <a href="#">Text</a>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fas fa-pencil-alt"></i>Edit Title</a></li>
            <li><a href="#"><i class="fas fa-arrows-alt-h"></i>Resize Column</a></li>
            <li><a href="#"><i class="fas fa-sort"></i>Sort Column</a></li>
            <li><a href="#"><i class="fas fa-draw-polygon"></i>Drag Column</a></li>
            <li><a href="#"><i class="fas fa-copy"></i>Duplicate Column</a></li>
            <li><a href="#"><i class="fas fa-plus"></i>Add Column to the Right &gt;</a></li>
            <li><a href="#"><i class="fas fa-trash-alt"></i>Delete Column</a></li>
          </ul>
        </div>
      </div>
      <input type="text">
    </div>
    <div class="tag">
      <div class="stus-head">
        <div class="dropdown">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
          <a href="#">Tag</a>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fas fa-pencil-alt"></i>Edit Title</a></li>
            <li><a href="#"><i class="fas fa-arrows-alt-h"></i>Resize Column</a></li>
            <li><a href="#"><i class="fas fa-sort"></i>Sort Column</a></li>
            <li><a href="#"><i class="fas fa-draw-polygon"></i>Drag Column</a></li>
            <li><a href="#"><i class="fas fa-copy"></i>Duplicate Column</a></li>
            <li><a href="#"><i class="fas fa-plus"></i>Add Column to the Right &gt;</a></li>
            <li><a href="#"><i class="fas fa-trash-alt"></i>Delete Column</a></li>
          </ul>
        </div>
      </div>
      <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
        <span>
        <input type="text">
        </span>
        </button>
        <ul class="dropdown-menu">
          <li>
            <input type="text" placeholder="Search..">
          </li>
          <li style="padding-bottom:20px;padding-left:10px;"><a href="#">Manage Tag</a></li>
        </ul>
      </div>
    </div>
    <div class="number"></div>
  </div>
  


-->



<table class="table table-responsive table-bordered">
<thead>
  <th>

    

  </th>
 

      <th></th> 
      
 
  
</thead>
<tbody>
  
  <tr>
    
  </tr>

</tbody>
</table>