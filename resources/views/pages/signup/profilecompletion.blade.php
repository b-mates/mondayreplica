@extends('layouts.singlecolumn')
@section('content')

<div class="container">
    <form name="step4" id="step4" method="POST" action="{{route('poststep4')}}">
        @csrf
        <div class="row">
            <div class="col-sm-4 login mt-6 check-s">
              <h2>Your Account details</h2>
              <p>Please remember the following details</p>
              <p>This is the link to your profile</p>

              
              <div class="input-group mb-2 "> 
                <span>
                  <div class="input-group ">
                    <a class="form-control" href="http://{{$linkurl}}">{{$linkurl}}</a>
                    
                  </div>
                </span> 
              </div>
                
              
              
            </div>
            <div class="col-sm-8 mr-auto mb-5 imp "><img src="{{ asset('images/desktop-banner.jpg')}}" class="w-100 mt-4" alt=""/></div>
          </div>
    </form>
  </div>

@stop
@section('script')
<script type="text/javascript">
  $('#next').unbind("click");
    $("#next").click(function()
    {
    
   $accounturl=$("#Account").val();
   if($accounturl=="")
   {
     $("#Account").addClass("error");
   }
   else if($("Agreed").checked==false)
   {
     alert("Please check user agreement checkbox");
   }
   else{
    $(function(){
        
      $.ajax({
        type: "post",
        url: "/signup/checkaccounturl",
		async: true,
        data: {accounturl:$accounturl},
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(response)
        {
          var obj = jQuery.parseJSON( response );
          if(obj.count<1)
          {
            $( "#step4" ).submit();
          }
          else{
            $("#Account").addClass("error");
            alert("Account url of similar name exist");
          }
        }
        });


    });
    
    }
    });
 
</script>
@endsection