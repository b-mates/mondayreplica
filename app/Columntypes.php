<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Columntypes extends Model
{
    protected $fillable=['columnname','displayname','status'];
   
    public function getColumnTypes()
    {
        return $this->all()->sortBy("id");
    }
    public function getColumn($columnid)
    {
        return $this->where('id',$columnid)->first();
    }

}
