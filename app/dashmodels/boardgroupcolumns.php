<?php

namespace App\dashmodels;

use Illuminate\Database\Eloquent\Model;

class boardgroupcolumns extends Model
{
    //
    protected $fillable=['title','columntype','groupid','status'];

    public function addGroupcolumns($title,$columntype,$groupid,$index)
    {
        $this->title=$title;
        $this->columntype=$columntype;
        $this->groupid=$groupid;
        $this->colindex=$index;
        $this->status=1;
        $this->save();
        return $this;
    }

    public function getCollection($groupid)
    {
        return $this->where('groupid',$groupid)->orderby('colindex','asc')->get();
    }

    function lastColumn($boardgroupid)
    {
       return $this->where('groupid',$boardgroupid)->orderby('colindex','desc')->first();
    }

    
}
