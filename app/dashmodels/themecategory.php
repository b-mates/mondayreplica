<?php

namespace App\dashmodels;

use Illuminate\Database\Eloquent\Model;

class themecategory extends Model
{
    //

    protected $table="themecategories";
    protected $fillable=['themecategory','status'];

    public function getList()
    {
        $list=$this->all();
        return $list;
    }

    public function themes()
    {
        return $this->hasMany('App\dashmodels\themes', 'themecategory', 'id');
    }
}
