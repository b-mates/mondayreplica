<?php

namespace App\dashmodels;

use Illuminate\Database\Eloquent\Model;
use App\Boardgrouppulse;
use App\Boardpulseitem;
class boardgroup extends Model
{
    //

    protected $fillable=['grouptitle','sequence'];

    protected $hidden=['id','boardid','status'];
     
    public function addGroup($colstitle,$boardid,$sequence)
    {
        $this->grouptitle=$colstitle;
        $this->boardid=$boardid;
        $this->sequence=$sequence;
        $this->status=1;
        $this->save();
        return $this;
    }

    public function getCollection($boardid)
    {
        return $this->where('boardid',$boardid)->orderby('sequence','asc')->get();
    }

    public function groupcolumns()
    {
        
        return $this->hasMany('App\dashmodels\boardgroupcolumns','groupid','id');
    }

    public function addPulsevaluestoHead($newheadid,$boardid,$defaultvalue)
    {
        $groupsarray=$this->getCollection($boardid);
        foreach($groupsarray as $groups)
        {
            $groupid=$groups->id;
            $Boardgrouppulse=new Boardgrouppulse;
            $pulsecount=$Boardgrouppulse->getPulseCount($groupid);
            

            $pulsesarray=$Boardgrouppulse->getPulses($boardid,$groupid);

            foreach ($pulsesarray as $pulses) {
                # code...
                $Boardpulseitem=new Boardpulseitem;
                $pulseid=$pulses->id;
                $Boardpulseitem->addPulseItem($groupid,$newheadid,$pulsecount,$pulseid,$defaultvalue);
            }

            
        }
    }
    public function getGroupDetails($boardid,$groupid)
    {
        return $this->where(['boardid'=>$boardid,'id'=>$groupid])->get();
    }
    public function lastGroupSequence($boardid)
    {
        $groupSeq=$this->where('boardid',$boardid)->orderby('sequence','desc')->first();
        return $groupSeq;
    }
    public function deleteBoardGroup($boardid,$groupid)
    {
     
        $updateGroup=$this->where(['boardid'=>$boardid,'id'=>$groupid]);
        
        $updateGroup->delete();
        return $updateGroup;
    }
}
