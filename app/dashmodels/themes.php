<?php

namespace App\dashmodels;

use Illuminate\Database\Eloquent\Model;

class themes extends Model
{
    //
    protected $table="themes";
    protected $fillable=['themename','description','thumbnails','status'];

    public function getThemes($categoryid)
    {
        $themes=$this->where('themecategory',$categoryid)->get();
        return $themes;
    }

    public function getTheme($themeid)
    {
        $themes=$this->where('id',$themeid)->first();
        return $themes;
    }
    public function getThemesList()
    {
        $themelist=$this->all();
        return $themelist;
    }
}
