<?php

namespace App\dashmodels;

use Illuminate\Database\Eloquent\Model;

class themegroups extends Model
{
    //

    protected $fillable=['themeid','maingroup','groupname','description','status'];
}
