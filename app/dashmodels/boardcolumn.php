<?php

namespace App\dashmodels;

use Illuminate\Database\Eloquent\Model;
use Auth;

class boardcolumn extends Model
{
    //

    public $table="boardcolumns";
    protected $fillable=['columnid','columnvalue','rowindex','boardid','account','status'];

    public function addBoardcolumns($columnid,$columnvalue,$boardid,$index)
    {

        $account=Auth::User()->companyid;
        $status=true;

        $this->columnid=$columnid;
        $this->columnvalue=$columnvalue;
        $this->boardid=$boardid;
        $this->rowindex=$index;
        $this->account=$account;
        $this->status=true;
        $this->save();
        return $this;

    }

    public function getColumncollection($groupid)
    {
        $account=Auth::User()->companyid;
        return $this->where('columnid',$groupid)->orderby('rowindex','asc')->get();
    }

    
}
