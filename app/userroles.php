<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userroles extends Model
{
    //
    protected $fillable=['rolename','description','status'];

    //get Default value of user roles in a Board like whether the person who creates is an owner,admin or just a member
    public function getDefaultvalues()
    {    
           
        return $this->where(['owner'=>1,'status'=>1])->first()->id;
    }
}
