<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class teaminvitation extends Model
{
    //
    protected $fillable=['inviter','invitee','token','status'];
}
