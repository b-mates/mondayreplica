<?php

namespace App\boardmodels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class statuslabel extends Model
{
    //
    use SoftDeletes;

    protected $fillable=['label','colorcode','done','status'];
    protected $dates = ['deleted_at'];

    public function getLabels()
    {
        $boardlabels=$this->where(['status'=>'1'])->get();
        return $boardlabels;
    }

}   
