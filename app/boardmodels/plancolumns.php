<?php

namespace App\boardmodels;

use Illuminate\Database\Eloquent\Model;

class plancolumns extends Model
{
    //
    protected $table="--plans-columns";
    protected $fillable=['plans','columns','status'];
}
