<?php

namespace App\boardmodels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Boardcolumnlabels extends Model
{
    //
    use SoftDeletes;

    protected $fillable=['headid','label','colorcode','done','status'];
    protected $dates = ['deleted_at'];

    public function addBoardcolumnlabels($labelsarray)
    {
        $this->insert($labelsarray);
    }

    public function getLabels($boardheads)
    {
        return $this->where('headid',$boardheads)->get();
    }

    public function getDefaultvalue($headingid)
    {
        return $this->where(['headid'=>$headingid,'defaultvalue'=>"1"])->first()->label;
    }
}
