<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    public $table = "admin";
    protected $guard = 'admin';
    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */
    // protected $table = 'admins';
    protected $fillable = ['email','password',];

    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = ['password', 'remember_token',];
}
