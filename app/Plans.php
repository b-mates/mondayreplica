<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plans extends Model
{
    //

    protected $fillable=['planname','noofusers','price','status'];

    public function plancolumns()
    {
        return $this->hasManyThrough('App\boardmodels\columntypes', 'App\boardmodels\plancolumns', 'plans', 'id');
    }  
    
}
