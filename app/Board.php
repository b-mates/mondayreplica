<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Boardrole;
use App\Boardtype;
use Auth;
use App\userroles;
use App\User;

class Board extends Model
{
    //
    protected $fillable=['boardname','boarddescription','companyid','folder','boardtype','createdby','status'];

    public function createBoard($boardname,$boardtype)
    {
        $companyid=Auth::user()->companyid;
        $userid=Auth::user()->id;
        $createboard=$this->create(['boardname'=>$boardname,'boarddescription'=>$boardname,'companyid'=>$companyid,'boardtype'=>$boardtype,'createdby'=>$userid]);
        return $createboard;
    }

    public function deleteBoard($boardid)
    {
        $this->where('id',$boardid)->delete();
    }

    public function getBoard($boardid)
    {
        return $this->where('id',$boardid)->first();
    }

    public function getPermittedBoards($boardtype)
    {
        $userid=Auth::user()->id;
        $companyid=Auth::user()->companyid;

        $user=new User;
        
        $myboards=$user->find(34)->boardrole;

        return($myboards);


    }



    
}
