<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boardtype extends Model
{
    //
    protected $fillable=['boardtypename','boarddescription','default','status'];

    public function getDefaultvalues()
    {       
        return $this->where(['default'=>1,'status'=>1])->first()->id;
    }

    public function getBoardtypes()
    {
        return $this->all()->sortBy("id");
    }

    public function getMainBoard()
    {
        return $this->all()->sortBy("id");
    }
}
