<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workarea extends Model
{
    protected $table="workarea";
    protected $fillable=['workarea','active'];
}
