<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Boardgrouppulse extends Model
{
    protected $table="boardgroup-pulses";
    //
    public $fillable=['pulsename','groupid','sequence','status'];

    public function getPulses($boardid,$groupid)
    {
       $pulses= $this->where(['groupid'=>$groupid])->orderby('sequence')->get();
       return $pulses;
    }


    public function getPulseCount($groupid)
    {
        $pulsecount=$this->where('groupid',$groupid)->count();
        return $pulsecount;
    }

    public function addPulses($groupid,$pulsename,$seq)
    {
       $this->pulsename=$pulsename;
       $this->groupid=$groupid;
       $this->sequence=$seq;
       $this->status=1;
       $this->save();
        
        
       $lastid=$this->id;

      

       return $lastid;
    }

    public function lastPulseofGroup($groupid)
    {
        $group=$this->where('groupid',$groupid)->orderby('sequence','desc')->first();
        return $group;
    }

    public function updatePulse($pulseid,$groupid,$newpulsevalue)
    {
        $updatepulse=$this->find($pulseid);
        $updatepulse->pulsename=$newpulsevalue;
        $updatepulse->save();
        return $updatepulse;
    }

    public function deletePulse($pulseid)
    {
        $updatepulse=$this->find($pulseid);
        
        $updatepulse->delete();
        return $updatepulse;
    }
}
