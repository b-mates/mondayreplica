<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoardHeading extends Model
{
    //
    protected $table="board_headings";
    public $fillable=['headingname','boardid','columntype','status'];

    public function getBoardHeading($boardid)
    {
        $boardheading=$this->where(['boardid'=>$boardid])->orderby('sequence')->get();
        return $boardheading;
    }

    public function getBoardHeadingid($boardid)
    {
        $boardheading=$this->where(['boardid'=>$boardid])->orderby('sequence')->pluck('id')->toArray();
        return $boardheading;
    }
    
public function getColumnCount($boardid)
{
    $columncount=$this->where('boardid',$boardid)->count();
    return $columncount;
}
    public function addNewHead($boardid,$boardcolumnname,$displayname)
    {
        $newsequence=$this->getLastsequence($boardid)+1;
        $this->headingname=$displayname;
        $this->boardid=$boardid;
        $this->columntype=$boardcolumnname;
        $this->sequence=$newsequence;
        $this->status=1;
        $this->save();
        return $this->id;

    }

    public function addNewHeadfromtheme($boardid,$boardcolumnname,$datatype,$seq)
    {
        //$newsequence=$this->getLastsequence($boardid)+1;+1
        $seq=$seq+1;
        $this->headingname=$boardcolumnname;
        $this->boardid=$boardid;
        $this->columntype=$datatype;
        $this->sequence=$seq;
        $this->status=1;
        $this->save();
        return $this->id;

    }

    public function getLastsequence($boardid)
    {
        $seqarray=$this->where('boardid',$boardid)->orderby('sequence','desc')->first();
        $sequence=$seqarray->sequence;
        return $sequence;
    }

    public function getBoardHeadingDetails($boardid)
    {
        $boardheading=$this->where(['boardid'=>$boardid])->orderby('sequence')->pluck('headingname','columntype')->toArray();
        return $boardheading;
    }
    
}
