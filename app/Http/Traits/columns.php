<?php
namespace App\Http\Traits;
use App\userroles;
use App\Boardtype;
use App\Board;
use App\Boardrole;
use Auth;
use DB;
use \App\dashmodels\boardgroup;

use App\dashmodels\boardgroupcolumns;
use App\dashmodels\boardcolumn;
use App\Plans;

use App\boardmodels\Boardcolumnlabels;

/**
 * Left menu inside the user profile
 */
trait columns
{
    /*
    Function will give UL List of columns against plans
    */
    public function getColumnUlList($planid)
    {
        $Plans=new Plans;
        
        $ulbock="";
      

        $list=$Plans->find($planid)->plancolumns;
        
        return $list;
    }
 /*
    ----Function will give UL List of columns against plans----
    */

    public function addNewcolumn($boardid,$boardcoltype)
    {

        
        $boardgroup=new boardgroup;

        $boardgroupcolumns=new boardgroupcolumns;
        $boardgroups=$boardgroup->where('boardid',$boardid)->get();
        $columnname=$boardcoltype->columnname;


        foreach ($boardgroups as $bgvalue) 
        {            
            $lastindexvalues=$boardgroupcolumns->lastColumn($bgvalue->id);
            $id=$lastindexvalues->id;
            $groupid=$lastindexvalues->groupid;

            $boardcolumn=new boardcolumn;
            $groupcount=$boardcolumn->where('columnid',$id)->get()->count();

            $newindex=$lastindexvalues->colindex+1;
            $this->addColumnhead($boardid,$groupid,$newindex,$columnname,$groupcount);

        }


    }

    function addColumnhead($boardid,$groupid,$newindex,$columnname,$groupcount)
    {
        $boardgroupcolumns=new boardgroupcolumns;
        $boardgroupcolumns->title=$columnname;
        $boardgroupcolumns->columntype=$columnname;
        $boardgroupcolumns->colindex=$newindex;
        $boardgroupcolumns->groupid=$groupid;
        $boardgroupcolumns->status=1;
        $ins=$boardgroupcolumns->save();


        $lastid=$boardgroupcolumns->id;

        $colvalue=$this->getColumnvalueset($columnname);

        $this->addBoardcolumns($boardid,$lastid,$colvalue,$groupcount);

    }


    function getColumnvalueset($columnname)
    {
        return "";
    }

    function addBoardcolumns($boardid,$ins,$colvalue,$groupcount)
    {
        
        
        for($i=0;$i<$groupcount;$i++)
        {
            $boardcolumn=new boardcolumn;
            $boardcolumn->columnid=$ins;
            $boardcolumn->columnvalue=$colvalue;
            $boardcolumn->rowindex=$i;
            $boardcolumn->boardid=$boardid;
            $boardcolumn->account=Auth::user()->companyid;
            $boardcolumn->status=1;
            $boardcolumn->save();
        }
    }


    /*Column center */

    function getControl($componentvalue,$componentname,$Boardpulsevalue,$headid)
    {
        $control="";
        if($componentname=="longtext")
        {
            $control='<span class="controls"><input  type="text" class="component-textbox-longtext" id="'.$componentvalue.'"  value="'.$Boardpulsevalue.'" /></span>';
        }
        else if($componentname=="smalltext")
        {
            $control='<span class="controls"><input  type="text"  class="component-textbox-smalltext" id="'.$componentvalue.'" value="'.$Boardpulsevalue.'" /></span>';
        }
        else if($componentname=="statuscolumn")
        {
            $Boardcolumnlabels=new Boardcolumnlabels;
            $boardlabels=$Boardcolumnlabels->getLabels($headid);
            $html="<div id='statusdiv'>";
            foreach ($boardlabels as $boardlabelsarray) {
                $html.='<p style="background-color:'.$boardlabelsarray->colorcode.'">'.$boardlabelsarray->label.'</p>';
            }
            $html.="</div>";
            $control=$html;
        }

        return $control;
    }

    /*Column center */
}