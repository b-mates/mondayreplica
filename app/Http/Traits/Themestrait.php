<?php
namespace App\Http\Traits;
use App\userroles;
use App\Boardtype;
use App\Board;
use App\Boardrole;
use Auth;
use DB;
/**
 * 
 */
trait Themestrait
{
    public function getThemes($themecategory)
    {
        
        $themes=DB::table('themes')
                    ->where('themecategory',$themecategory)->get();
    
        $themearray=array();
        foreach($themes as $thm)
        {
            $themearray+=array($thm->id=>$thm->themename);
            
        }
        return $themearray;
    }
}
