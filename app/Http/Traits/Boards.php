<?php
namespace App\Http\Traits;
use App\userroles;
use App\Boardtype;
use App\Board;
use App\Boardrole;
use Auth;
use DB;
use \App\dashmodels\boardgroup;

use App\dashmodels\boardgroupcolumns;
use App\dashmodels\boardcolumn;
use App\Boardgrouppulse;

use App\Boardpulseitem;

/**
 * Left menu inside the user profile
 */
trait Boards
{
    public function getLeftMenu()
    {

        

        $htmlblock='<input type="hidden" name="boardtypehidden" id="boardtypehidden" value="" />';
        $boardtype=new Boardtype;
        $boardval=$boardtype->getMainBoard();
        foreach($boardval as $boardtype)
        {
            
            $id=$boardtype->id;
            $boardtypename=$boardtype->boardtypename;

            $htmlblock.='<div class="panel-group menu-left">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a data-toggle="collapse" href="#collapse'.$id.'"><i class="fas fa-bars"></i> '.$boardtypename.'</a> </h4>
              </div>
              <div id="collapse'.$id.'" class="panel-collapse collapse">';

               $htmlblock.=$this->getSubmenu($id);
              $htmlblock.='</div>
            </div>
            </div>';

            //echo $this->getSubmenu($id);
            //
            //$this->getSubmenu($id);
        }
         $htmlblock.=' ';
        echo $htmlblock;
    }
    

    public function getSubmenu($boardtype)
    {
        $companyid=Auth::User()->companyid;
        $boards=DB::table('boards')
                    ->where(['boardtype'=>$boardtype,'companyid'=>$companyid])->get();
    
        $submenu='';
        foreach($boards as $sub)
        {
         
        $submenu.=' <div class="panel-body"><a  href="javascript:chooseBoard('.$sub->id.')"> '.$sub->boardname.' </a><i class="fas fa-angle-double-right"></i></div>';
        }
        $submenu.='<div class="panel-footer"><span><a onclick="newBoard('.$boardtype.')" href="#" data-toggle="modal" data-target=".exampleModal"><i class="fas fa-plus"></i> New</a></span></div>';

        return $submenu;
    }

/**
 * Left menu inside the user profile
 */


    function array_push_assoc($array, $key, $value)
    {
    $array[$key] = $value;
    return $array;
    }



        
    public function boardgrouprelation($groupid)
    {
        
        $boardgroup=new boardgroup;

        $result=$boardgroup::find($groupid)->groupcolumns;

        return $result;
    }



    public function getBoardarray($boardid)
    {
        $grouparray=[];
        $tophead=[];
        $grouparray['board']=[];
        $grouparray['boardrows']=[];
        $grouparray['groupheading']=[];
        try
        {
            $board=new Board;
       
            $boardcollection=$board->getBoard($boardid);

                $boardgroupcolumns=new boardgroupcolumns;

                $boardname=$boardcollection->boardname;
                $boarddescription=$boardcollection->boarddescription;
                $companyid=$boardcollection->companyid;
                $folder=$boardcollection->folder;
                $boardtype=$boardcollection->boardtype;
                
                
                $grouparray['board']=['board'=>$boardid,'boardname'=>$boardname,'boarddescription'=>$boarddescription,'companyid'=>$companyid,'folder'=>$folder,'boardtype'=>$boardtype];
 
                $grouparray['board']['cols']=[];



                $boardgroup=new boardgroup;
                $boardgroupcollection=$boardgroup->getCollection($boardid);
                $i=0;
                
                foreach($boardgroupcollection as $boardgroupvalues)
                {
                    $boardgroupsid=$boardgroupvalues->id;
                    $boardgroupstitle=$boardgroupvalues->grouptitle;

                    $boardgroupssequence=$boardgroupvalues->sequence;
                    $boardgroupsstatus=$boardgroupvalues->status;

                    $newrow=["id"=>$boardgroupsid,"title"=>$boardgroupstitle,"sequence"=>$boardgroupssequence,"status"=>$boardgroupsstatus];

                    $grouparray['board']['cols'][$i]=$boardgroupvalues->id;
                    
                    $grouparray['boardrows'] = $this->array_push_assoc($grouparray['boardrows'], $boardgroupsid, $newrow);

                    $boardgroupvalues=$boardgroupcolumns->getCollection($boardgroupsid);

                   //echo $boardgroupsid;

                    $groupharray=$boardgroupcolumns->getCollection($boardgroupsid);
                    

                    foreach($groupharray as $gparray)
                    {
                        $grpheadid=$gparray->id;
                        $grpheadtitle=$gparray->title;
                        $grpheadcolumntype=$gparray->columntype;
                        $grpheadcolindex=$gparray->colindex;
                        $grpheadgroupid=$gparray->groupid;
                        $grpheadstatus=$gparray->status;

                        $grouparray['groupheading']=$this->array_push_assoc($grouparray['groupheading'],$grpheadid,['id'=>$grpheadid,'title'=>$grpheadtitle,'columntype'=>$grpheadcolumntype,'colindex'=>$grpheadcolindex,'groupid'=>$grpheadgroupid,'status'=>$grpheadstatus]);

                    }
                    

                    $ind1=0;
                    
                    $i++;

                }

                

                $coltitlearray=$grouparray['board']['cols'];


                $innerarray=array();

                $boardcolumnvalue=array();

                foreach($coltitlearray as $colharray)
                {
                       $titleid=$colharray;

                       array_push($tophead,$titleid);

                       $boardgroupcolumns=new boardgroupcolumns;

                       $boardgroupvalues=$boardgroupcolumns->getCollection($titleid);
                       $i=0;

                       $colval=array();
                       $headingarray=array();
                       
                       foreach($boardgroupvalues as $titlearray)
                       {

                           $innerarray[$titleid][$i]=$titlearray->id;

                           $headingarray[$titlearray->id]=['id'=>$titlearray->id,'title'=>$titlearray->title];

                           

                           $i++;

                       }

                       $innerarray[$titleid]["heading"]=$headingarray;
                       $boardcolumn=new boardcolumn;                     

                }

              
            
                return $grouparray;

        }
        catch(Exception $ex)
        {
            echo $ex;
        }
        

    }



    function getRows($maingroup,$colarray)
    {
     
        
        $colcount=count($colarray);
        $unionarray=array();
      

        $rows=array();
        $i=0;
        $query="";

        $idsImploded = implode(',',$colarray);
        $boardcolumns = DB::table('boardcolumns')->whereIn('columnid',$colarray)->orderby('rowindex','asc')->get();
       
        //echo json_encode($boardcolumns);

        $boardrows=[];
        $i=0;
        foreach($boardcolumns as $boardcol)
        {
            
            $boardrows[$boardcol->id]=['id'=>$boardcol->id,'columnid'=>$boardcol->columnid,'title'=>$boardcol->columnvalue,'rowindex'=>$boardcol->rowindex,'boardid'=>$boardcol->boardid,'account'=>$boardcol->account,'status'=>$boardcol->status];
            
            
        }

        return $boardrows;

    
       // return "Hi";
    }





    function boardcolumnsresult($res,$groupid)
    {
        
        $boardrows=array();
        $i=0;
        foreach($res as $boardgroupcolumns)
        {
          $boardgroupcolumnsid= $boardgroupcolumns->id;
          $boardgroupid= $boardgroupcolumns->groupid; 
          
          $i++;
        }

        return $boardrows;
    }

    
    public function getPulsesandvalues($boardid,$groupid,$headarray)
    {
        $valarray=array();
        $Boardgrouppulse=new Boardgrouppulse;
        $pulses=$Boardgrouppulse->getPulses($boardid,$groupid);

        foreach($pulses as $pulse)
        {
            $pulseid=$pulse->id;          
            $valarray[$pulseid]=$this->getPulseitems($pulseid,$headarray);

            //
        }
        //echo json_encode($valuearray);
        return $valarray;
    }


    public function getPulseitems($pulseid,$headingarray)
    {
        $Boardpulseitem=new Boardpulseitem;
        $items=$Boardpulseitem->getPulseitems($pulseid,$headingarray);

        return $items;
    }

    
}
