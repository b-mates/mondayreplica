<?php
namespace App\Http\Controllers;
use Auth;
use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use App\company;
use App\User;
use App\Plans;
use App\workarea;
use \App\dashmodels\themecategory;
use \App\dashmodels\themes;
use App\Boardtype;
use App\Board;
use \App\dashmodels\boardgroup;
use \App\dashmodels\boardgroupcolumns;
use \App\Http\Traits\Boards;
use App\Columntypes;
use \App\dashmodels\boardcolumn;


class AdminRegisterController extends Controller
{
  use \App\Http\Traits\Boards;
  use \App\Http\Traits\columns;
   public function showRegistrationForm(Request $request)
   {
    return View("mondaymaster.pages.register");
   }
   public function register(Request $request)
   {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6'
        ]);

        $name               =   $request->get('name');
        $email              =   $request->get('email');
        $password           =   $request->get('password');

        $user =new Admin();
        $user->name=$name;
        $user->email=$email;
        $user->password=Hash::make($password);
      
        $user->save();
        $lastid=$user->id;
        $request->session()->flash('userid', $lastid);
        return redirect()->intended('mondaymaster/login');
   }
   public function showDashboard(Request $request)
     { 

      $boardtype=new Boardtype();
      $boards=$boardtype->getBoardtypes();
      $data['boards']=$boards;    

      $themes=new themes;    
      $themeList=$themes->getThemesList();   
      $data['themes']=$themeList;
     
     
      if(session('loginId'))
      {
        return view('mondaymaster.pages.dashboard')->with($data);
      }
      else
      {
        return redirect()->intended('mondaymaster/login');
      }     
    }
    public function showCompany()    
    {     
      $themes=new themes;    
      $themeList=$themes->getThemesList();   
      $data['themes']=$themeList;
     
      $boardtype=new Boardtype();
      $boards=$boardtype->getBoardtypes();
      $data['boards']=$boards;
         
     

     if(!(session('loginId')))
     {
       return redirect()->intended('mondaymaster/login');
     }
      $companyDatas = User
      ::join('companies', 'companies.createdby', '=', 'users.id')
      ->join ('plans','plans.id', '=', 'companies.currentplan')
      ->join('workarea','workarea.id','=','companies.warea')
      ->select('companies.id','users.name','companies.companyname','plans.planname','workarea.workarea','companies.teamstrength')
      ->get();
      //print_r($companyDatas); exit;
      $data['companyDatas']=$companyDatas;

      return View("mondaymaster.pages.company")->with($data);
    }
    public function showUsers($id)
    {      
      if(!(session('loginId')))
     {
       return redirect()->intended('mondaymaster/login');
     }
     $themes=new themes;    
     $themeList=$themes->getThemesList();   
     $data['themes']=$themeList;

     $boardtype=new Boardtype();
      $boards=$boardtype->getBoardtypes();
      $data['boards']=$boards;

      

      $companyDatas = User
      ::join('companies', 'companies.createdby', '=', 'users.id')
      ->join ('plans','plans.id', '=', 'companies.currentplan')
      ->join('workarea','workarea.id','=','companies.warea')
      ->select('companies.id','users.name','companies.companyname','plans.planname','workarea.workarea','companies.teamstrength')
      ->get();
      //print_r($companyDatas); exit;
      $data['companyDatas']=$companyDatas;

      //echo "bbb".$id; exit;
       $userList=  User
       ::join('companies', 'companies.createdby', '=', 'users.id')
       ->select('users.id','users.name','users.email','companies.companyname','users.status')
       ->where('companies.id','=',$id)
       ->get();
      $data['userList']=$userList;

      return View("mondaymaster.pages.company")->with($data);
    }
    public function getCompany($id)
    {    
      if(!Auth::guard('admin')->user())
      {
        return redirect('mondaymaster/login');
      } 
       $themes=new themes;    
      $themeList=$themes->getThemesList();   
      $data['themes']=$themeList;

      $boardtype=new Boardtype();
      $boards=$boardtype->getBoardtypes();
      $data['boards']=$boards;

      $companyId=$id;

      $data['id']=$companyId;
      $companyDatas = User
            ::join('companies', 'companies.createdby', '=', 'users.id')
            ->join ('plans','plans.id', '=', 'companies.currentplan')
            ->join('workarea','workarea.id','=','companies.warea')
            ->select('companies.id','users.name','companies.companyname','plans.planname','workarea.workarea','companies.teamstrength')
            ->where('companies.id', $companyId)
            ->get();
      $data['companyDatas']=$companyDatas;

      //user List
      $userList=  User
              ::join('companies', 'companies.createdby', '=', 'users.id')
              ->select('users.id','users.name','users.email','companies.companyname','users.status')
              ->where('companies.id',$companyId)
              ->get();
       $data['userList']=$userList;
      return View("mondaymaster.pages.company")->with($data);
    }
   
    public function showthemeCategory()
    {
      
      $boardtype=new Boardtype();
      $boards=$boardtype->getBoardtypes();
      $data['boards']=$boards;

      

      
      $themes=new themes;    
      $themeList=$themes->getThemesList();   
     // $data['themes']=$themeList;


      $themecategories=new themecategory;
      $themecategorylist=$themecategories->getList();
      $data['themecategory']  = $themecategorylist;
      return view('mondaymaster.pages.themecategory')->with($data);
    }
    public function showThemes($id)
    {
      $catId    = $id;
      $data['catId']  =$catId;
      $boardtype=new Boardtype();
      $boards=$boardtype->getBoardtypes();
      $data['boards']=$boards;



      $themecategories=new themecategory;
      $themecategorylist=$themecategories->getList();
      $data['themecategory']  = $themecategorylist;

      $themes=new themes;     
      $themesList=$themes->getThemes($id);
      $data['themes']=$themesList;    

      return view('mondaymaster.pages.themecategory')->with($data);
    }

    public function showBoardlist($typeid='',$boardid='',Request $request)
    {
      $request->session()->put('typeid',$typeid);
      $typeid=$request->session()->get('typeid');
      // echo $typeid; exit;
      $boardtype=new Boardtype();
      $boards=$boardtype->getBoardtypes();
      $data['boards']=$boards;
     // print_r($boards); exit;

      $themes=new themes;    
      $themeList=$themes->getThemesList();   
      $data['themes']=$themeList;  

      $boardList = board
            ::join('boardtypes', 'boardtypes.id', '=', 'boards.boardtype')            
            ->select('boards.id','boards.boardname','boardtypes.boardtypename')
            ->where('boards.boardtype', $typeid)
            ->get();
      $data['boardList']=$boardList;
      return View("mondaymaster.pages.boardList")->with($data);
    }

    public function showboardtemplate($id,Request $request)
    {
      $boardid=$id;
      $data=array();
      $grouprows=array();
      $maingroup=array();
      $group=[];
      $boardgroupcol=array();
      $groupcolarray=array();

      $columnType=new Columntypes();
      $colTypes=$columnType->getColumnTypes();
      $data['boardid']=$boardid;

      $data['colTypes']=$colTypes;


      


        $board=new Board;
        $boarddata=$board->where('id',$boardid)->firstorfail();
        $boardname= $boarddata->boardname;
        $boarddesc= $boarddata->boarddescription;       
              
        $boardcontent=$this->getBoardarray($boardid);

        $boardgroup=new boardgroup;
        $boardgroupcollection=$boardgroup->getCollection($boardid);
        $data["mainboard"]=$boardcontent["board"];
        $data["boardrows"]=$boardcontent["boardrows"];
        $data["groupheading"]=$boardcontent["groupheading"];
        
      // print_r($boardgroupcollection); exit;
        $data["boardname"]=$boardname;
        $data["boarddesc"]=$boardname;

        $data["boardgroups"]=$boardgroupcollection;
        $data["bcontent"]=$boardcontent;
        $boardgroupidarray="";
        foreach($boardgroupcollection as $boardgroupvalues)
        {
            $boardgroupidarray.=$boardgroupvalues->id.",";
        }        
        $request->session()->flash('grouparray', $boardgroupidarray);
        $request->session()->flash('step', '1');
        
        $data["boardgroupsid"]=$boardgroupidarray;
        
        $boardgroup=new boardgroup;
        $boardgroupcollection=$boardgroup->getCollection($boardid);
        $data["boardgroups"]=$boardgroupcollection; //gettingmain title

      //print_r($data["boardgroups"]); exit;
        foreach ($boardgroupcollection as $grp) 
        {  
          $res=$this->boardgrouprelation($grp->id);
          $boardgroupcol[$grp->id]=$this->boardgrouprelation($grp->id);

          array_push($maingroup,$grp->id);
          $group[$grp->id]=$this->boardgrouprelation($grp->id);                     
          session(["maingroup"=>$maingroup]);
        }    
      
        foreach ($boardgroupcollection as $grp) 
        {
            foreach($boardgroupcol[$grp->id] as $groupcolobj)
            {
                $groupcolarray[$grp->id][]=$groupcolobj->id;
            } 
        }    
        //print_r($group[$grp->id]); exit;    
        $maingroup= $request->session()->get('maingroup', 'default');
        //print_r($maingroup); exit;
        foreach($maingroup as $headgroup)
        {            
            $groupcols=$groupcolarray[$headgroup];     
            $grouprows[$headgroup]=$this->getRows($headgroup,$groupcols);
        } 
   
    
      // dd($grouprows); 
       $data["group"]= $group;   
       $data["boardid"]=$boardid;
       $data["grouprows"]=$grouprows;  
        return View("mondaymaster.pages.boardtemplate")->with($data);
    } 
    public static function getboardgroupcolumns($boardGpId1)
    {
      $boardGpColumns =  new boardgroupcolumns();
      $group=$boardGpColumns->getCollection($boardGpId1);
      return $group;
     // print_r($group); 
      //$res=$this->boardgrouprelation(boardGpId1);
      //print_r($res);
    }
   
    
    public function newThemeColumn(Request $request)
    {
         $columnid     =   $request->get('id');
         $columnname   =   $request->get('columntext');        
         $catId        =   $request->get('catId');
         $themeId      =   $request->get('themeId');
      
         $themecategories=new themecategory;
         $themecategorylist=$themecategories->getList();
         $data['themecategory']  = $themecategorylist;
   
         $themes=new themes;     
         $themesList=$themes->getThemes($catId);
         $data['themes']=$themesList;    
   
           $columnType=new Columntypes();
           $colTypes=$columnType->getColumnTypes();
          // dd( $colTypes);
           $data['colTypes']=$colTypes; //getting all column types

         $theme=new themes;
         $themeDetails=$theme->getTheme($themeId);
         $themeDesign=$themeDetails->themetemplate;

         $themeDesign=(array)json_decode($themeDesign,true);      

         //dd($themeDesign);
          $data['themeDesign'] = $themeDesign;
          
         $arrColCount=count($themeDesign);
         $newColumns     = $columnname ;
         $newColType     = $columnname;
         $newColHeading  = $columnname;
    
        $templatearray=[];
        $index=0;

        $groupsarray=$themeDesign['groups'];
        $headingarray=$themeDesign['heading'];
        $datatypearray=$themeDesign['datatype'];

        $colcount=count($headingarray);

        $templatearray['groups']=$groupsarray;

        array_push($headingarray,$newColumns);  // Added new Columns in heading 
        $templatearray['heading']=$headingarray;

        array_push($datatypearray,$newColumns); 
        $templatearray['datatype']=$datatypearray;

        foreach($groupsarray as $gpvalues)
        {
          $groupnames=$gpvalues;          
          $pulsesarray=(array)$themeDesign[$groupnames]["pulses"];  
          $colcount=2;
          for($i=0;$i<$colcount;$i++)
          {
            $pulses=$pulsesarray[$i];  
            array_push($pulsesarray[$pulses], 'New'); 
            $templatearray[$groupnames]["pulses"]=$pulsesarray;  
          }
        }
     
         $new_json_data = json_encode($templatearray); // json array merging
        // print_r($new_json_data); exit;
         
         $themes_update=new themes;    
         $upd_themes=$themes_update::where('id', $themeId)
                    ->update(['themetemplate' => $new_json_data]);      
    }
    public function updateTemplateColumn(Request $request)
    {
      $themeId        =   $request->get('themeid');
      $oldColValue    =   $request->get('oldColValue');        
      $columntext     =   $request->get('columntext');
      $catId          =   $request->get('catId');
      $themecategories=   new themecategory;
      $themecategorylist  = $themecategories->getList();
      $data['themecategory']  = $themecategorylist;

      $themes             =   new themes;     
      $themesList         =   $themes->getThemes($catId);
      $data['themes']     =   $themesList;    

        $columnType=new Columntypes();
        $colTypes=$columnType->getColumnTypes();
       // dd( $colTypes);
        $data['colTypes']=$colTypes; //getting all column types
        $arrUpdateTemplate=array();
      $theme=new themes;
      $themeDetails=$theme->getTheme($themeId);
      $themeDesign=$themeDetails->themetemplate;
      $arrthemeDesign=json_decode($themeDesign,true);

      $data['themeDesign'] = $arrthemeDesign;

      $groupsarray=$arrthemeDesign['groups'];

      $headingarray=$arrthemeDesign['heading'];
      $headingarray=str_replace("Text",$columntext,$headingarray);        


      $datatypearray=$arrthemeDesign['datatype'];

      $colcount=count($headingarray);
      $arrUpdateTemplate['groups']=$groupsarray;     
      $arrUpdateTemplate['heading']=$headingarray;     
      $arrUpdateTemplate['datatype']=$datatypearray;

      foreach($groupsarray as $gpvalues)
      {
        $groupnames=$gpvalues;          
        $pulsesarray=(array)$arrthemeDesign[$groupnames]["pulses"];  
        $colcount=2;
        for($i=0;$i<$colcount;$i++)
        {
          $pulses=$pulsesarray[$i];  
          $arrUpdateTemplate[$groupnames]["pulses"]=$pulsesarray;  
        }       
       
      }   
      //print_r($arrUpdateTemplate); exit;
         $update_json_data = json_encode($arrUpdateTemplate); 
       //  print_r($update_json_data); exit;
         $themes_update=new themes;    
         $upd_themes=$themes_update::where('id', $themeId)
                    ->update(['themetemplate' => $update_json_data]);  
    }
    public function showTemplate(Request $request,$catId,$themeid)
    {
      $data['catId']  =$catId;
      $data['themeid']  =$themeid;
      $boardtype=new Boardtype();
      $boards=$boardtype->getBoardtypes();
      $data['boards']=$boards;



      $themecategories=new themecategory;
      $themecategorylist=$themecategories->getList();
      $data['themecategory']  = $themecategorylist;

      $themes=new themes;     
      $themesList=$themes->getThemes($catId);
      $data['themes']=$themesList;    

        $columnType=new Columntypes();
        $colTypes=$columnType->getColumnTypes();
       // dd( $colTypes);
        $data['colTypes']=$colTypes; //getting all column types

        $theme=new themes;
        $themeDetails=$theme->getTheme($themeid);
        $themeDesign=$themeDetails->themetemplate;

       
        $themeDesign=(array)json_decode($themeDesign,true);      

        // dd($themeDesign);
          $data['themeDesign'] = $themeDesign;

          $groupsarray=$themeDesign['groups'];

          $headingarray=$themeDesign['heading'];
          $datatypearray=$themeDesign['datatype'];

          $data['groupsarray'] = $groupsarray;
          $data['headingarray'] = $headingarray;
          $data['datatypearray'] = $datatypearray;

          foreach ($headingarray as $boardcolumnname) 
          {
            $colcount=count($headingarray);
            $data['colcount'] = $colcount;
          }
       return view('mondaymaster.pages.themecategory')->with($data);
    }

    public function updateTemplateColumnRowvalues(Request $request)
    {
      $themeId        =   $request->get('themeid');
      $oldrowvalue    =   $request->get('oldrowvalue');        
      $rowvalue     =   $request->get('rowvalue');
      $catId          =   $request->get('catid');
      $row            =  $request->get('row');
      $gpname            =  $request->get('gpname');
      
     // echo $gpname ; exit;
      $themecategories=   new themecategory;
      $themecategorylist  = $themecategories->getList();
      $data['themecategory']  = $themecategorylist;

      $themes             =   new themes;     
      $themesList         =   $themes->getThemes($catId);
      $data['themes']     =   $themesList;    

        $columnType=new Columntypes();
        $colTypes=$columnType->getColumnTypes();
        $data['colTypes']=$colTypes; //getting all column types

      $theme=new themes;
      $themeDetails=$theme->getTheme($themeId);
      $themeDesign=$themeDetails->themetemplate;
      $arrthemeDesign=json_decode($themeDesign,true);
     
      $data['themeDesign'] = $arrthemeDesign;

      $groupsarray=$arrthemeDesign['groups'];

      $headingarray=$arrthemeDesign['heading'];
      $datatypearray=$arrthemeDesign['datatype'];

      $colcount=count($headingarray);
      $arrUpdateTemplate['groups']=$groupsarray;     
      $arrUpdateTemplate['heading']=$headingarray;     
      $arrUpdateTemplate['datatype']=$datatypearray;

      foreach($groupsarray as $gpvalues)
      {
        $groupnames=$gpvalues;    
       // echo $groupnames; exit;      
        $pulsesarray=(array)$arrthemeDesign[$groupnames]["pulses"];  
        $colcount=2;
       
        for($i=0;$i<$colcount;$i++)
        {
          $pulses=$pulsesarray[$i];  
        //  echo $rowvalue.",".$oldrowvalue."<br/>";
          $pulsescontent=(array)$arrthemeDesign[$groupnames]["pulses"][$pulses];
          if($row==$i &&  (trim($gpname)==trim($groupnames)) ){
           $pulsescontent=str_replace($rowvalue,$oldrowvalue,$pulsescontent); 
           //print_r($pulsescontent);
          }
          else {
            $newcontent=str_replace($rowvalue,$rowvalue,$pulsescontent); 
          }
         // print_r($newcontent); exit;

          array_merge($pulsesarray[$pulses], $pulsescontent); 
        //  print_r($pulsesarray); exit;
          $arrUpdateTemplate[$groupnames]["pulses"]=$pulsesarray;  



          // print_r($pulsescontent); exit;
          //$arrUpdateTemplate[$groupnames]["pulses"][$pulses]=$pulsescontent;  
        }       


        
        //  for($i=0;$i<$colcount;$i++)
        // {
        //   $pulses=$pulsesarray[$i];  
        //   $pulsescontent=(array)$arrthemeDesign[$groupnames]["pulses"][$pulses];
        //   $newcontent=str_replace($rowvalue,$oldrowvalue,$pulsescontent); 
        //   $arrUpdateTemplate[$groupnames]["pulses"][$pulses]=$newcontent;    


        //   // if($row==$i &&  (trim($gpname)==trim($groupnames)) ){
        //   // $newcontent=str_replace($rowvalue,$oldrowvalue,$pulsescontent); 
        //   // $arrUpdateTemplate[$groupnames]["pulses"][$pulses]=$newcontent;    
        //   // }  
        //   // else {
        //   //   $newcontent=str_replace($rowvalue,$rowvalue,$pulsescontent); 
        //   // }  
        //   // //  array_push($pulsesarray[$pulses], 'New'); 
        //   //   $arrUpdateTemplate[$groupnames]["pulses"]=$newcontent;  

        //   // $arrUpdateTemplate[$groupnames]["pulses"][$pulses]=$newcontent;       
        //   // $templatearray[$groupnames]["pulses"]=$pulsesarray;    
        // }g
      
      }   
     //print_r($arrUpdateTemplate); exit;
         $update_json_data = json_encode($arrUpdateTemplate); 
         //print_r($update_json_data); exit;
         $themes_update=new themes;    
         $upd_themes=$themes_update::where('id', $themeId)
                    ->update(['themetemplate' => $update_json_data]);  

    }   
  }  