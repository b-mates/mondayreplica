<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\company;
use Mail;
use Carbon\Carbon;
use App\Jobs\SendEmailJob;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Hash;


class LoginController extends Controller
{
    //
    public function login(Request $request)
    {
        $subdomainname = $request->instance()->query('subdomainname');
        

        $subdomainhost= str_replace(env('APP_URL'),'',$_SERVER['HTTP_HOST']) ;
        $subdomainhostlength=strlen($subdomainhost);
        if($subdomainhostlength>0)
        {
            $subdomainname=substr($subdomainhost,0,intval($subdomainhostlength)-1);
        }

        $val=explode('.', $_SERVER['HTTP_HOST'])[0];

        $data["subdomainname"]=$subdomainname;
        $data['subdomain']=$subdomainname;
        return View("pages.login")->with($data);
    }

    public function forgotpassword(Request $request)
    {

        $subdomainhost= str_replace(env('APP_URL'),'',$_SERVER['HTTP_HOST']) ;
        $subdomainhostlength=strlen($subdomainhost);
        if($subdomainhostlength>0)
        {
            $subdomainname=substr($subdomainhost,0,intval($subdomainhostlength)-1);
        }

        //$subdomainname = $request->instance()->query('subdomainname');
        $data["subdomainname"]=$subdomainname;
        $val=explode('.', $_SERVER['HTTP_HOST'])[0];

        $val=explode('.', $_SERVER['HTTP_HOST'])[0];
        $data['subdomain']=$subdomainname;
        return View("pages.forgotpassword")->with($data);
    }


    public function postlogin(Request $request)
    {
        $request->validate([
            'emailid' => 'required|email',
            'password' => 'required'
        ]);
        
        $email=$request->get('emailid');
        $password=$request->get('password');
        $sub=$request->get('sub');

        $companymodel=new company();
        $companyid=$companymodel::where('companyname', $sub)->first();    
        $remember=true;
        $companyid->id;
        $planid=$companyid->currentplan;
        if (Auth::attempt(['email' => $email, 'password' => $password,'companyid'=>$companyid->id,'status'=>'1']) )
        {
         
            $appurl=config('app.url');
            $loginurl=$sub.".".$appurl;

            Session(['plan'=>$planid,'companyid'=>$companyid->id]);
           

            return redirect()->intended('/profile');
        }   
        else 
        {
            return redirect()->back()->with('message', 'Please check your username/password!');

        }
    }

    public function postforgotpassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);
        
        $emailid=$request->get('email');
        $sub=$request->get('sub');
        
        $companymodel=new company();
        $company=$companymodel::where('companyname', $sub)->first();
        $companyid=$company->id;
        

        $user=new User();
        $count=$user::where(['companyid'=>$companyid,'email'=>$emailid])->count();
        
        if($count>0)
        {
            $length=6;
            $result="";
            
            for($i = 0; $i < $length; $i++) {
                $result .= mt_rand(0, 9);
            } 
            
            //$token="1212";
            $token=$result;
            $when = now()->addMinutes(1);
            $data["token"]=$token;
            $data["emailid"]=$emailid;
            
          
            $updatetoken=$user::where(['companyid'=>$companyid,'email'=>$emailid])->update(['resettoken'=>$token]);
            

            $emailJob = new SendEmailJob($data);
            

            if(dispatch($emailJob))
            {
                echo "Done";
                
                
                return redirect()->route('changepassword',['emailid'=>$emailid])->with(['message', 'Please check your mail to reset the password','emailid'=>$emailid]);
            }
            else
            {
                echo "Not send";
                return redirect()->back()->with('message', 'Please try again later');
            }




            

             
        }
        
    }

    public function changepassword(Request $request)
    {
        $subdomainname = $request->instance()->query('subdomainname');
        $data["subdomainname"]=$subdomainname;
        $val=explode('.', $_SERVER['HTTP_HOST'])[0];

        $val=explode('.', $_SERVER['HTTP_HOST'])[0];

        $subdomainhost= str_replace(env('APP_URL'),'',$_SERVER['HTTP_HOST']) ;
        $subdomainhostlength=strlen($subdomainhost);
        if($subdomainhostlength>0)
        {
            $subdomainname=substr($subdomainhost,0,intval($subdomainhostlength)-1);
        }


        $data['subdomain']=$subdomainname;
        $data['emailadd']=$request->get('emailid');
        return View("pages.changepassword")->with($data);
    }

    public function postresetpassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'newpassword' => 'required',
            'token' => 'required',
        ]);
        $sub=$request->get('sub');
        $emailid=$request->get('email');
        $newpassword=$request->get('newpassword');
        $token=$request->get('token');

        $user=new User();

        $resetpassword=$user::where(['email'=>$emailid,'resettoken'=>$token])->update(['password'=>Hash::make($newpassword)]);
        if($resetpassword)
        {
            return redirect()->intended('login')->with('message','Password changed');
        }
        else {
            return redirect()->back()->with('message', 'Please try again later');
        }



    }
}
