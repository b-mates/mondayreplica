<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Boardpulseitem;
use \App\boardmodels\Boardcolumnlabels;

class WidgetsController extends Controller
{
    //
    use \App\Http\Traits\Boards;
    public function LeftMenu()
    {
        $leftmenu=$this->getLeftMenu();
        return $leftmenu;
    }
    use \App\Http\Traits\columns;
    public function chooseControl(Request $request)
    {
        $boardid=$request->get('boardid');
        $componentname=$request->get('component');
        $columnvalueid=$request->get('componentvalueid');
        $headid=$request->get('headid');
        //$request->validate(['component'=>'required']);
        $Boardpulseitem=new Boardpulseitem;
        $Boardpulsevaluearray=$Boardpulseitem->getPulseitemvalue($columnvalueid);
        
        $Boardpulsevalue=$Boardpulsevaluearray->pulsevalue;

        echo $this->getControl($columnvalueid,$componentname,$Boardpulsevalue,$headid);
        
        
    }

    public function getStatuscolumns(Request $request)
    {
        $headid=$request->get('headid');
        $Boardcolumnlabels=new Boardcolumnlabels;
        $heads=$Boardcolumnlabels->getLabels($headid);
        $data["heads"]=$heads;
        return view('includes/boardlabels', $data);
        
    }

    function saveComponent(Request $request)
    {
        $boardid=$request->get('boardid');
        $pulseitemid=$request->get('pulseitemid');
        $pulsevalue=$request->get('pulsevalue');

        $component=$request->get('component');

        $Boardpulseitem=new Boardpulseitem;
        $Boardpulseitem->updateBoardpulseitem($boardid,$pulseitemid,$pulsevalue);
        

    }

}