<?php
use Illuminate\Database\Eloquent\Model;
namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Route;
use App\Admin;
use App\company;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Auth\Authenticatable;
use DB;
use App\Http\Requests;
class AdminLoginController extends Controller
{ 
  use AuthenticatesUsers;
  protected $redirectTo = '/';
    public function __construct()
    {
      /*$this->middleware('guest:admin', ['except' => ['logout']]);*/
      $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
      return view('mondaymaster.pages.login');
    }    
   
    public function login(Request $request)
    { 
      $this->validate($request, [
        'emailId'   => 'required|email',
        'password' => 'required|min:6'
      ]);
      if (auth()->guard('admin')->attempt(['email' => $request->emailId, 'password' => $request->password]))
      {
        //echo "aa:-".Auth::guard('admin')->user(); exit; 
        $request->session()->put('loginId', $request->emailId);
        return redirect()->route('masterDashboard');
      }else{
        return redirect()->back()->with('message', 'Please check your username/password!');
      }
      return redirect()->back()->withInput($request->only('email', 'remember'));
    }
    public function logout(Request $request)
    { 
        Auth::guard('admin')->logout();
        auth()->logout();
        $request->session()->forget('loginId');
        $request->session()->flush();
        return redirect('mondaymaster/login');
    }
}