<?php

namespace App\Http\Controllers\boards;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\dashmodels\boardgroupcolumns;

use App\boardmodels\columntypes;

use App\dashmodels\boardgroup;

use App\BoardHeading;

class columncontroller extends Controller
{
    //
    use \App\Http\Traits\columns;
    function newBoardColumn(Request $request)
    {
        $boardid=$request->get('boardid');
        $columntypeid=$request->get('columnid');

        $columntypes=new columntypes;

        $boardcoltype=$columntypes->where('id',$columntypeid)->first();

    
        $boardcolumnname=$boardcoltype->columnname;
        $displayname=$boardcoltype->displayname;
        $statuscolumn=$boardcoltype->statuscolumn;
        /*Add new Heading to the Board */
        $BoardHeading=new BoardHeading;

        $newheadid=$BoardHeading->addNewHead($boardid,$boardcolumnname,$displayname);
        $labelarr=array();
        $defaultvalue="";
        $defaultlabel="";
        if($statuscolumn=="1")
        {
           $statuslabels=new \App\boardmodels\statuslabel;
           $labelsarray=$statuslabels->getLabels();
           
           foreach($labelsarray as $labels)
           {
               
            array_push($labelarr,array('headid'=>$newheadid,'label'=>$labels->label,'colorcode'=>$labels->colorcode,'defaultvalue'=>$labels->defaultvalue,'done'=>$labels->done)); 
            $defaultvalue=$labels->defaultvalue;
            if($defaultvalue==1)
            {
                $defaultlabel=$labels->label;
            }
            
           }
           
           $Boardcolumnlabels=new \App\boardmodels\Boardcolumnlabels;
           $Boardcolumnlabels->addBoardcolumnlabels($labelarr);
           

           $boardgroup=new boardgroup;

            $boardgroup->addPulsevaluestoHead($newheadid,$boardid,$defaultlabel);

        }
        else{
            $boardgroup=new boardgroup;

            $boardgroup->addPulsevaluestoHead($newheadid,$boardid,$defaultlabel);
        }

        /* */

        /*Add value against pulse and head */
        
       

        


    }
}
