<?php

namespace App\Http\Controllers;

use App\Board;
use App\Boardgrouppulse;
use App\BoardHeading;
use App\Boardpulseitem;
use App\Boardrole;
use App\Boardtype;
use App\dashmodels\boardcolumn;
use App\dashmodels\boardgroup;
use App\dashmodels\boardgroupcolumns;
use App\userroles;
use Illuminate\Http\Request;
use \App\dashmodels\themes;

class TemplateController extends Controller
{
    //

    public function createtemplate(Request $request)
    {
        /*
        Get the Board name and what kind of board going to create from the input page
         */

        $boardname = $request->session()->get('boardname');
        $boardtype = $request->session()->get('boardtype');

        $board = new Board;
        $request->session()->reflash();

        $templateid = $request->get('templateid');
        /*
        Get the Themes json from Themes table
         */
        $themes = new themes;
        $themeval = $themes->getTheme($templateid);

        $newboard = $board->createBoard($boardname, $boardtype);
        $userroles = new userroles;
        $boardtype = new Boardtype;

        /*
        Retrieving the default creator role from the table
         */
        $defaultvalues["userroleid"] = $userroles->getDefaultvalues();
        $defaultvalues["boardtype"] = $boardtype->getDefaultvalues();
        $newboardid = $newboard->id;

        if ($newboard->id != null) {

            $boardrole = new Boardrole;
            $boardrole->addUserrole($newboard->id, $defaultvalues["userroleid"]);
            $array["success"] = 1;
        }

        $templatearray = (array) $themeval->themetemplate;

        $cols = $themeval->themetemplate;

        $vals = (array) json_decode($cols, true);

        $groupsarray = $vals['groups'];

        $headingarray = $vals['heading'];
        $datatypearray = $vals['datatype'];

        $j = 0;
        foreach ($headingarray as $boardcolumnname) {
            # code...
            $BoardHeading = new BoardHeading;
            $datatype = $datatypearray[$j];
            $BoardHeading->addNewHeadfromtheme($newboardid, $boardcolumnname, $datatype, $j);

            $j++;

        }

        $colcount = count($headingarray);

        $i = 0;
        foreach ($groupsarray as $gpvalues) {
            $i++;
            $groupnames = $gpvalues;
            $boardgroup = new boardgroup;
            $newgroup = $boardgroup->addGroup($groupnames, $newboardid, $i);

            $groupid = $newgroup->id;

            $pulsesarray = (array) $vals[$groupnames]["pulses"];
            for ($i = 0; $i < $colcount; $i++) {
                $pulses = $pulsesarray[$i];

                $Boardgrouppulse = new Boardgrouppulse;
                $pulsescontent = (array) $vals[$groupnames]["pulses"][$pulses];
                $pulseid = $Boardgrouppulse->addPulses($groupid, $pulses, $i);

                $BoardHeading = new BoardHeading;
                $BoardHeadingid = $BoardHeading->getBoardHeadingid($newboardid);

                $colcount = count($pulsescontent);

                for ($j = 0; $j < $colcount; $j++) {

                    $colcontent = $pulsescontent[$j];
                    $headid = $BoardHeadingid[$j];

                    $Boardpulseitem = new Boardpulseitem;

                    $Boardpulseitem->addBoardpulseitem($newboardid, $groupid, $pulseid, $colcontent, $headid);

                }

            }

        }

        $requiredarray = ['board' => $newboardid];

        echo json_encode($requiredarray);

    }

    /*Adding pulse to existing Board group */
    public function addPulse(Request $request)
    {
        $request->validate([
            'pulse' => 'required| max:255',
        ]);
        $boardid = $request->get('boardid');
        $groupid = str_replace("-newpulse", "", $request->get('groupid'));
        $Boardgrouppulse = new Boardgrouppulse;

        $pulsename = $request->get('pulse');
        $pulserow = $Boardgrouppulse->lastPulseofGroup($groupid);

        $newsequence = $pulserow->sequence + 1;
        $Boardgrouppulse = new Boardgrouppulse;
        $newpulse = $Boardgrouppulse->addPulses($groupid, $pulsename, $newsequence);

        $pulseid = $newpulse;

        $BoardHeading = new BoardHeading;

        $headingsarray = $BoardHeading->getBoardHeading($boardid);
        $pulsescontent = [];
        $colcontent = "";
        foreach ($headingsarray as $heading) {
            $headingid = $heading->id;
            $Boardpulseitem = new Boardpulseitem;
            $Boardpulseitem->addBoardpulseitem($boardid, $groupid, $pulseid, $colcontent, $headingid);
        }
    }

    /*After clicking the templates in the board this function helps to retrieve the groups */

    public function setBoardRowsandColumns(Request $request)
    {
        return view('pages.boards.setBoardRowsandColumns');
    }
    use \App\Http\Traits\Boards;
    public function boardrowsandcolums(Request $request)
    {
        $boardgroupsid = $request->get('boardgroup');
        $boardid = $request->get('boardid');

        $board = new Board;
        $boarddata = $board->where('id', $boardid)->firstorfail();
        $boardname = $boarddata->boardname;
        $boarddesc = $boarddata->boarddescription;

        $boardgroupcolumns = new boardgroupcolumns;

        $boardcolumn = new boardcolumn;

        $boardgroupvalues = $boardgroupcolumns->getCollection($boardgroupsid);

        $data["boardgroupvalues"] = $boardgroupvalues;

        $columarray = [];

        $boardcontent = $this->getBoardarray($boardid);

        foreach ($boardgroupvalues as $boardgroup) {

            $boardgpid = $boardgroup->id;

            $boardcolumns = $boardcolumn->getColumncollection($boardgpid);

        }
        /*

        foreach($boardgroupvalues as $boardgroup)
        {
        $boardgpid=$boardgroup->id;

        $boardcolumn=new boardcolumn;

        $boardcolumns=$boardcolumn->getColumncollection($boardgpid);

        }
         */

        $boardgroup = new boardgroup;
        $boardgroupcollection = $boardgroup->getCollection($boardid);

        $data["boardname"] = $boardname;
        $data["boarddesc"] = $boardname;
        $data["boardgroups"] = $boardgroupcollection;

        $data["bcontent"] = $boardcontent;
        return view("pages.boards.boardrowsandcolumns")->with($data);
    }

    use \App\Http\Traits\Boards;
    use \App\Http\Traits\columns;
    public function populatedata(Request $request)
    {

        $valuearray = array();

        $grouprows = array();
        $boardid = $request->get('boardid');

        $BoardHeading = new BoardHeading;
        $boardheadings = $BoardHeading->getBoardHeading($boardid);

        $boardheadingarray = array();

        foreach ($boardheadings as $boardhead) {
            array_push($boardheadingarray, $boardhead->id);
        }

        $step = $request->session()->get('step');
        if ($step == 1) {
            $grouparray = $request->session()->get('grouparray');

        }

        $Boardgrouppulse = new Boardgrouppulse;

        $boardgroup = new boardgroup;
        $boardgroupcollection = $boardgroup->getCollection($boardid);

        $data["boardgroups"] = $boardgroupcollection;

        $group = [];

        $grouppulses = [];

        foreach ($boardgroupcollection as $grp) {

            $res = $this->boardgrouprelation($grp->id);

            $pulses = $Boardgrouppulse->getPulses($boardid, $grp->id);
            $valarray = $this->getPulsesandvalues($boardid, $grp->id, $boardheadingarray);

            $valuearray[$grp->id] = $valarray;

            $grouppulses[$grp->id] = $pulses;

        }

        $data["group"] = $group;

        $data["boardid"] = $boardid;

        $data["pulseitemsarray"] = $valuearray;

        $data["boardheadings"] = $boardheadings;

        $data["grouppulses"] = $grouppulses;

        $plan = $request->session()->get('plan', '0');
        $companyid = $request->session()->get('companyid', '0');

        $planbasedcolumns = $this->getColumnUlList($plan);
        $data["planbasedcolumns"] = $planbasedcolumns;

        $board = new Board;
        $remainingBoardarray = $board->whereNotIn('id', [$boardid])->get();
        $data["remainingBoardarray"] = $remainingBoardarray;

        return view("pages.boards.boardgroup")->with($data);

    }

    public function populateheading(Request $request)
    {

        $maingroup = $request->session()->get('maingroup', 'default');

        $bheading = [];
        foreach ($maingroup as $mgroup) {

            $boardgroupcolumns = new boardgroupcolumns;

            $groupcolumnvalue[$mgroup] = $bheading[$mgroup] = $boardgroupcolumns->getCollection($mgroup);
        }

    }

    public function postEditpulse(Request $request)
    {
        $pulseid = $request->get('pk');
        $groupid = $request->get('groupid');
        $newpulsevalue = $request->get('value');
        $request->validate([
            'value' => 'required| max:255',
        ]);
        $Boardgrouppulse = new Boardgrouppulse;
        $Boardgrouppulse->updatePulse($pulseid, $groupid, $newpulsevalue);
    }

    public function managePulse(Request $request)
    {
        $boardid = $request->get('boardid');
        $pulseid = $request->get('pulseid');
        $process = $request->get('process');

        if ($process == "delete") {

            $Boardpulseitem = new Boardpulseitem;
            $deleted = $Boardpulseitem->deleteBoardpulseitem($pulseid);

            if ($deleted) {
                $Boardgrouppulse = new Boardgrouppulse;
                $Boardgrouppulse->deletePulse($pulseid);
            }
            //
        }
    }

/**-------------Athira Started */

    public function duplicateGroup(Request $request)
    {
        $boardid = $request->get('boardid');
        $groupid = $request->get('groupid');

        //getting group name details
        $boardgroup = new boardgroup;

        $groupsDetails = $boardgroup->getCollection($boardid);
        $recCnt = count($groupsDetails);
        $nextSeq = $groupsDetails[$recCnt - 1]->sequence;

        $groupsarray = $boardgroup->getGroupDetails($boardid, $groupid);

        foreach ($groupsarray as $gpvalues) {
            $nextSeq++;
            $groupnames = $gpvalues->grouptitle;
            $boardgroup = new boardgroup;
            $newgroup = $boardgroup->addGroup($groupnames, $boardid, $nextSeq);
            $newgroupid = $newgroup->id;
        }
        //$newgroupid=261;
        $data["boardgroups"] = $groupsarray;
        $BoardHeading = new BoardHeading;
        $boardheadings = $BoardHeading->getBoardHeading($boardid);
        $boardheadingarray = array();
        foreach ($boardheadings as $boardhead) {
            array_push($boardheadingarray, $boardhead->id);
        }
        $index = 0;
        $Boardgrouppulse = new Boardgrouppulse;
        $arraypulses = $Boardgrouppulse->getPulses($boardid, $groupid);
        //dd($arraypulses); exit;
        $pulsecount = $Boardgrouppulse->getPulseCount($newgroupid);
        foreach ($arraypulses as $oldGpPulses) {
            $Boardgrouppulse = new Boardgrouppulse;
            $pulsename = $oldGpPulses->pulsename;
            $pulseSeq = $oldGpPulses->sequence;
            $newpulse = $Boardgrouppulse->addPulses($newgroupid, $pulsename, $pulseSeq);
            $newpulseid = $newpulse;

            $Boardpulseitem = new Boardpulseitem;
            $arrpulseItem = $Boardpulseitem->getPreviousPulseitems($oldGpPulses->id, $groupid);
            // dd($arrpulseItem); exit;
            foreach ($arrpulseItem as $arrValue) {
                $Boardpulseitem = new Boardpulseitem;
                $newpulseitem = $Boardpulseitem->addPulseItem($newgroupid, $boardheadingarray[$index], $pulsecount, $newpulseid, $arrValue->pulsevalue);
            }
            $index++;
        }

        $boardgroup = new boardgroup;
        $boardgroupcollection = $boardgroup->getCollection($boardid);

        $data["boardgroups"] = $boardgroupcollection;

        $group = [];

        $grouppulses = [];
        // dd($boardgroupcollection); exit;
        foreach ($boardgroupcollection as $grp) {
            $res = $this->boardgrouprelation($grp->id);
            $pulses = $Boardgrouppulse->getPulses($boardid, $grp->id);
            $valarray = $this->getPulsesandvalues($boardid, $grp->id, $boardheadingarray);
            $valuearray[$grp->id] = $valarray;
            $grouppulses[$grp->id] = $pulses;
        }

        $data["groupid"] = $groupid;

        $data["boardid"] = $boardid;

        $data["pulseitemsarray"] = $valuearray;

        $data["boardheadings"] = $boardheadings;

        $data["grouppulses"] = $grouppulses;

        $plan = $request->session()->get('plan', '0');
        $companyid = $request->session()->get('companyid', '0');

        $planbasedcolumns = $this->getColumnUlList($plan);
        $data["planbasedcolumns"] = $planbasedcolumns;
        // dd($data); exit;
        return view("pages.boards.boardgroup")->with($data);
    }
    public function addGroup(Request $request)
    {
        $boardid = $request->get('boardid');
        $BoardHeading = new BoardHeading;
        $boardheadings = $BoardHeading->getBoardHeading($boardid); //getting board heading of the board
        $boardheadingarray = array();
        foreach ($boardheadings as $boardhead) {
            array_push($boardheadingarray, $boardhead->id);
        }

        $i = 0;
        $groupnames = 'New Group';
        $boardgroup = new boardgroup;
        $newgroup = $boardgroup->addGroup($groupnames, $boardid, $i); //Adding new Group
        $groupid = $newgroup->id;
        $boardpulsearray = array();
        $index = 0;

        foreach ($boardheadingarray as $heading) {
            $headingid = $heading;
            $Boardgrouppulse = new Boardgrouppulse;
            $pulses = "";
            $pulseid = $Boardgrouppulse->addPulses($groupid, $pulses, $index); // Adding pulses with empty data

            $Boardpulseitem = new Boardpulseitem;
            $colcontent = "";
            $Boardpulseitem->addBoardpulseitem($boardid, $groupid, $pulseid, $colcontent, $headingid); //Adding pulses items.
            $index++;
        }

        $boardgroup = new boardgroup;
        $boardgroupcollection = $boardgroup->getCollection($boardid);
        $data["boardgroups"] = $boardgroupcollection;
        $group = [];
        $grouppulses = [];
        foreach ($boardgroupcollection as $grp) {
            $res = $this->boardgrouprelation($grp->id);
            $pulses = $Boardgrouppulse->getPulses($boardid, $grp->id);
            $valarray = $this->getPulsesandvalues($boardid, $grp->id, $boardheadingarray);
            $valuearray[$grp->id] = $valarray;
            $grouppulses[$grp->id] = $pulses;
        }

        $data["groupid"] = $groupid;
        $data["boardid"] = $boardid;
        $data["pulseitemsarray"] = $valuearray;
        $data["boardheadings"] = $boardheadings;
        $data["grouppulses"] = $grouppulses;
        $plan = $request->session()->get('plan', '0');
        $companyid = $request->session()->get('companyid', '0');
        $planbasedcolumns = $this->getColumnUlList($plan);
        $data["planbasedcolumns"] = $planbasedcolumns;
        return view("pages.boards.boardgroup")->with($data);

    }
    public function getremainingColumns(Request $request)
    {
        $boardid = $request->get('boardid');
        $selectedboardid = $request->get('selectedboard');
        $gpid = $request->get('gpid');

        $arrayRemainingColums = array();
        $arrayRemainingColumsid = array();
        //get details of selected boards and column details

        $board = new Board;
        $boardhead = new BoardHeading;

        $selBoard = $board->getBoard($selectedboardid);
        $selBoarddetails = $boardhead->getBoardHeadingDetails($selectedboardid);

        $crntBoard = $board->getBoard($boardid);
        $currentBoarddetails = $boardhead->getBoardHeadingDetails($boardid);

        $data['selBoard'] = $selBoard;
        $data['selBoarddetails'] = $selBoarddetails;

        $data['crntBoard'] = $crntBoard;
        $data['currentBoarddetails'] = $currentBoarddetails;

        foreach ($selBoarddetails as $selectedBoard) {
            if (in_array($selectedBoard, $currentBoarddetails)) {
                // array_push($arrayRemainingColums,'');
            } else {
                array_push($arrayRemainingColums, $selectedBoard);
            }
        }
        $arrayMoveColums['gpid'] = $gpid;
        $arrayMoveColums['boardid'] = $boardid;
        $arrayMoveColums['selboard'] = $selectedboardid;
        $arrayMoveColums['boardname'] = $selBoard->boardname;
        $arrayMoveColums['additionalColums'] = $arrayRemainingColums;

        return $arrayMoveColums;
// return view("pages.boards.boardgroup")->with($data);
    }

    public function moveGrouptoBoard(Request $request)
    {
        $boardid = $request->get('boardid');
        $selectedboardid = $request->get('selectedboardid');
        $groupid = $request->get('group');

        if ($boardid && $selectedboardid && $groupid) {
            try {
                $boardgroup = new boardgroup;
                $arrGpDetails = $boardgroup->getGroupDetails($boardid, $groupid); //group details

                $Boardgrouppulse = new Boardgrouppulse;
                $arrGpPulses = $Boardgrouppulse->getPulses($boardid, $groupid); //pulse details
                //echo count($arrGpPulses); exit;
                foreach ($arrGpPulses as $gpPulses) {
                    $pulseid = $gpPulses->id;
                    $Boardpulseitem = new Boardpulseitem;
                    $arrPulseItem[$pulseid] = $Boardpulseitem->getPulseItemDetails($pulseid); //pulse item details
                }
                dd($arrPulseItem);
                $arrPulseItems = $arrPulseItem;
                $BoardHeading = new BoardHeading;
                $SelColumncount = $BoardHeading->getColumnCount($selectedboardid); //selected board's group's columns Count
                $crntGpColumnCnt = $BoardHeading->getColumnCount($boardid); //current board's group's columns Count
            //      echo  $SelColumncount.",".$crntGpColumnCnt.",".count($arrGpPulses); exit;
                // echo $Columncount; exit;
                /**---Move to new Board */

                $groupSeq = $boardgroup->lastGroupSequence($selectedboardid);
                if (isset($groupSeq)) {
                    $nextSeq = $groupSeq->sequence + 1;
                } else {
                    $nextSeq = 0;
                }

                $BoardHeading = new BoardHeading;
                $selectedboardheadings = $BoardHeading->getBoardHeading($selectedboardid); //selected board heading details
                $selectedboardheadingarray = array();
                $selectedboardheadingNamearray = array();
                $selectedboardheadingSeqarray = array();
                foreach ($selectedboardheadings as $boardhead) {
                    array_push($selectedboardheadingarray, $boardhead->id);
                    array_push($selectedboardheadingNamearray, $boardhead->headingname);
                    array_push($selectedboardheadingSeqarray, $boardhead->sequence);
                }

                $BoardHeading = new BoardHeading;
                $currentedboardheadings = $BoardHeading->getBoardHeading($boardid); //current board heading details
                $currentboardheadingarray = array();
                $currentboardheadingNamearray = array();
                $currentboardheadingSeqarray = array();
                foreach ($currentedboardheadings as $boardhead) {
                    array_push($currentboardheadingarray, $boardhead->id);
                    array_push($currentboardheadingNamearray, $boardhead->headingname);
                    array_push($currentboardheadingSeqarray, $boardhead->sequence);
                }

                foreach ($arrGpDetails as $gpDetails) {
                    $gpTitle = $gpDetails->grouptitle;
                    $boardgroup = new boardgroup;
                    $newgroup = $boardgroup->addGroup($gpTitle, $selectedboardid, $nextSeq);
                    $newgroupid = $newgroup->id;
                    $Boardgrouppulse = new Boardgrouppulse;

                    if ($newgroupid) {
                            $i=0;
                            foreach($arrGpPulses as $pulses)
                            {
                                $pulsename = $pulses->pulsename;
                                $Boardgrouppulse = new Boardgrouppulse;
                                $arrNewPulse[$i] = $Boardgrouppulse->addPulses($newgroupid, $pulsename, $i);
                               // $arrNewPulse=$pulseid ;
                                $i++;
                            }
                            $k=0;
                            foreach($arrGpPulses as $pulses)
                            {
                                $pulseid = $pulses->id;
                               dd($arrPulseItem[$pulseid]); exit;




                                $Boardpulseitem = new Boardpulseitem;
                                $arrPulseItem = $Boardpulseitem->getPulseItemDetails($pulseid); //pulse item details

                                dd($arrPulseItem); exit;
                               if($SelColumncount < $crntGpColumnCnt)
                               {
                                   for($j=0;$j<$crntGpColumnCnt;$j++)
                                   {
                                       if($j==$SelColumncount)
                                       {
                                         $value = $pulseItems->pulsevalue;
                                         echo  $value ."<br/>";
                                       }
                                   }
                               }
                               else {
                                   echo "nooo"."<br/>";
                               }
                            }
                            ext;






















                        // for ($i = 0; $i < $SelColumncount; $i++) {
                        //     $Boardgrouppulse = new Boardgrouppulse;
                        //     if ($i == count($arrGpPulses)) {
                        //         $value = 'Done';
                        //         $Boardpulseitem = new Boardpulseitem;
                        //        // $newpulseitem = $Boardpulseitem->addPulseItem($newgroupid, $selectedboardheadingarray[$i], $SelColumncount, $pulseid, $value);

                        //     } else {
                        //         $pulsename = $arrGpPulses[$i]->pulsename;
                        //         $pulseid = $Boardgrouppulse->addPulses($newgroupid, $pulsename, $i);
                        //         $arrNewPulse=$pulseid ;
                        //         // $k=0;
                           
                        //         // foreach ($arrPulseItembyGp as $pulseItems) {
                            
                        //         //     if ($k != $SelColumncount) {                                        
                        //         //             $value = $pulseItems->pulsevalue;
                        //         //             $Boardpulseitem = new Boardpulseitem;
                        //         //             $newpulseitem = $Boardpulseitem->addPulseItem($newgroupid, $selectedboardheadingarray[$k], $SelColumncount, $pulseid, $value);
                        //         //     }
                        //         //     $k++;
                        //         // }
                        //     }
                        // }

                        // for ($index = 0; $index < count($arrGpPulses); $index++) {
                        //     if (($currentboardheadingNamearray[$index] == $selectedboardheadingNamearray[$index]) && ($currentboardheadingSeqarray[$index] == $selectedboardheadingSeqarray[$index])) {
                        //         $Boardgrouppulse = new Boardgrouppulse;
                        //         $pulsename = $arrGpPulses[$index]->pulsename;
                        //         $pulseid = $Boardgrouppulse->addPulses($newgroupid, $pulsename, $index);
                        //         $arrPulseItembyGp = $arrPulseItem[$arrGpPulses[$index]->id];
                        //         $i=0;
                        //         foreach ($arrPulseItembyGp as $pulseItems) {
                        //             if($i!=$Columncount){
                        //             if (($currentboardheadingNamearray[$i] == $selectedboardheadingNamearray[$i]) && ($currentboardheadingSeqarray[$i] == $selectedboardheadingSeqarray[$i])) {
                        //                 $Boardpulseitem = new Boardpulseitem;
                        //                 $newpulseitem = $Boardpulseitem->addPulseItem($newgroupid, $selectedboardheadingarray[$index], $SelColumncount, $pulseid, $pulseItems->pulsevalue);
                        //             }
                        //          }
                        //         $i++;
                        //         }
                        //     }
                        // }
                    }
                }

                //Deleting from old Boards.
                // foreach ($arrGpPulses as $arrGpPulses) {
                //     $Boardpulseitem = new Boardpulseitem;
                //     $deleted = $Boardpulseitem->deleteBoardpulseitem($arrGpPulses->id);

                //     $Boardgrouppulse = new Boardgrouppulse;
                //     $Boardgrouppulse->deletePulse($arrGpPulses->id);
                // }
                // $boardgroup = new boardgroup;
                // $boardgroup->deleteBoardGroup($boardid, $groupid);
            } catch (Exception $e) {
                report($e);

                return false;
            }
        }
    }

}
