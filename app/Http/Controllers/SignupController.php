<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use App\Userverification;
use App\Teamindustry;
use App\company;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Auth\Authenticatable;


use Mail;
use Cookie;
use App\teaminvitation;

class SignupController extends Controller
{
    //
    use AuthenticatesUsers;
    public function index()
    {
        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');

        $request->session()->reflash();
        $data="";
        return view('pages.signup.step1',['init'=>$data] );
        ;
    }

    public function createuser(Request $request)
    {
       
        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');

        $request->session()->reflash();

        $subdomainname = $request->instance()->query('subdomainname');
        $data["subdomainname"]=$subdomainname;
        return View('pages.signup.signup')->with($data);
    }
    

    public function step1(Request $request)
    {
       
        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');

        $request->session()->reflash();

        $subdomainname = $request->instance()->query('subdomainname');
        $data["subdomainname"]=$subdomainname;
        $data["invitee"]=$invitee;
       
        
        return View('pages.signup.verification')->with($data);
        
    }

    public function checkverificationmail(Request $request)
    { 
        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');

        $request->session()->reflash();

        $emailid= $request->get("emailid");
        $data["Email address"]=$emailid;
        
        $count = \App\Userverification::where(['emailaddress'=>$emailid,'status'=>'1'])->count();
        $data["count"]=$count;
        echo \json_encode($data);
   
    }

    public function checkaccounturl(Request $request)
    { 
        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');

        $request->session()->reflash();
        $accounturl= $request->get("accounturl");
        $data["accounturl"]=$accounturl;
        
        $count = \App\company::where(['companyname'=>$accounturl,'status'=>'1'])->count();
        $data["count"]=$count;
        echo \json_encode($data);
   
    }

    public function createuserpost(Request $request)
    {

        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');

        $request->session()->reflash();

        
        $emailid= $request->cookie('remail');
        $token= $request->cookie('token');
        $password= $request->get('password');
        $name=$request->get('fullname');
        Cookie::queue(Cookie::make('name',  $name, 3600));
        $request->validate([
            'fullname' => 'required',
            'password' => 'required',
            'phone'=>'numeric'
        ]);

        $usverification=new Userverification;
        $saveapassword=$usverification::where('emailaddress', $emailid)
                        ->where('token', $token)
                        ->update(['password' => $password]);
        if($saveapassword)
        {
            return \redirect('signup/step2');
        }

    }
    public function verifysignupemail(Request $request)
    {
        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');

        $request->session()->reflash();

        $emailid= $request->get("emailid");
        $token=$request->get("txt_1").$request->get("txt_2").$request->get("txt_3").$request->get("txt_4").$request->get("txt_5").$request->get("txt_6");
        $update = \App\Userverification::where(['emailaddress'=>$emailid,'token'=>$token])->update(['status' => 1]);
        $data["count"]=$update;
        if($update)
        {
            $minutes=time()+3600;
            
           // $response = new \Illuminate\Http\Response('regemail');
            //$response->withCookie(cookie('email', $emailid, 3600));
            //return $response;

            Cookie::queue(Cookie::make('remail',  $emailid, 3600));
            Cookie::queue(Cookie::make('token',  $token, 3600));

            //$cookie = Cookie::make('email', $emailid);
            
        }
        echo \json_encode($data);
    }


    public function sendverificationmail(Request $request)
    { 
        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');

        $request->session()->reflash();

        $emailid= $request->get("emailid");
        $verificationinsert = new Userverification;

        $result = '';

        $length=6;
        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        
        
        $signup = $verificationinsert::firstOrNew(array('emailaddress'=> $emailid, 'status' => 0));
        $signup->token =  $result;
        $signup->status =  '0';
        $signup->save(); 
        
        

        $data = ['token'=>$result,'email'=>strip_tags($emailid)];
         
        $mail=Mail::send('mail', $data, function($message)use($emailid) {
            $message->to($emailid, $emailid)->subject
               ('77 Tec User Verification');
            $message->from('info@b-matesdemo.com','77 TEC');
         });
        /**/

        if($mail)
        {
            echo "Email Sent. Check your inbox.";
        }
        else
        {
            echo "Not send";
        }
         

    }




    /*
    Signing up Step2
    */

    public function step2(Request $request)
    {

        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');
        $data["teamname"]="";
        

        $request->session()->reflash();

        if(session('inviter')!="")
        {
            $companymodel=new company;
            $companydetail=$companymodel->where('id',$companyid)->first();
            $companyname=$companydetail->companyname;
            $Memberslimit=$companydetail->teamstrength;
            $warea=$companydetail->warea;
            
            $emailid=$invitee;

            Cookie::queue(Cookie::make('Memberslimit',  $Memberslimit, 3600));
            Cookie::queue(Cookie::make('teamname',  $companyname, 3600));
            Cookie::queue(Cookie::make('TeamIndustry',  $warea, 3600));
            $emailtoken=$request->cookie('token');
            $Userverificationdetail=\App\Userverification::where(['emailaddress'=>$emailid,'token'=>$emailtoken])->first();
            $user = new \App\User();
                $user->password = Hash::make($Userverificationdetail->password);
                $user->email = $emailid;
                $user->name = $request->cookie('name');
                $user->phoneno = "";
                $user->companyid = $companyid;
                $user->status = 1;
                $user->save();
                $remember=true;
                if($user)
                {
                    $password = $Userverificationdetail->password;
                    $email = $emailid;

                        $request->session()->flash('companyid', $companyid);
                        $redirectionlink="http://".$request->get('Account').".".env('APP_URL');
                        return redirect()->intended('signup/profilecompletion');
                    
                    
                }
            
        }
        

        $subdomainname = $request->instance()->query('subdomainname');
        $data["subdomainname"]=$subdomainname;

        $value  = $request->cookie('remail');

        $data["emailid"]=$value;
        return View('pages.signup.step2')->with($data);
    }

    public function poststep2(Request $request)
    {
        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');

        $request->session()->reflash();

        $request->validate([
            'Memberslimit' => 'required',
            'teamname' => 'required',
        ]);

        Cookie::queue(Cookie::make('Memberslimit',  $request->get('Memberslimit'), 3600));
        Cookie::queue(Cookie::make('teamname',  $request->get('teamname'), 3600));
        return \redirect('signup/step3');
        
    }

    public function step3(Request $request)
    {

        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');

        $request->session()->reflash();


        $subdomainname = $request->instance()->query('subdomainname');
        $data["subdomainname"]=$subdomainname;

        $teamindustryds=\App\Teamindustry::all();
        $value  = $request->cookie('remail');
        $data["emailid"]=$value;
        $data["teamindustryds"]=$teamindustryds;
        return View('pages.signup.step3')->with($data);
    }

    public function poststep3(Request $request)
    {
        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');

        $request->session()->reflash();

        $request->validate([
            'TeamIndustry' => 'required',
            
        ]);

        Cookie::queue(Cookie::make('TeamIndustry',  $request->get('TeamIndustry'), 3600));
        
        return \redirect('signup/step4');
        
    }

   



    public function step4(Request $request)
    {
        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');

        $request->session()->reflash();

        
        $subdomainname = $request->instance()->query('subdomainname');
        $data["subdomainname"]=$subdomainname;

        //$teamindustryds=\App\Teamindustry::all();
        
       
        return View('pages.signup.step4')->with($data);
    }

    public function poststep4(Request $request)
    {
        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');

        $request->session()->reflash();

        $request->validate([
            'Account' => 'required'    
        ]);
        
        $emailid= $request->cookie('remail');
        $token= $request->cookie('token');
        $password= $request->get('password');
        $TeamIndustry= $request->cookie('TeamIndustry');
        $Memberslimit= $request->cookie('Memberslimit');    
        
        
        
        $companymodel=new company;
        $companymodel->companyname=$request->get('Account');
        $companymodel->description=$request->cookie('teamname');
        $companymodel->teamstrength=$Memberslimit;
        $companymodel->warea=$TeamIndustry;
        $companymodel->createdby=0;
        $companymodel->logoimage='0';
        $companymodel->headerimage='0';
        $companymodel->currentplan='1';
        $companymodel->status='1';
        $company=$companymodel->save();
        $lastid=$companymodel->id;

        if($company)
        {

            $Userverificationdetail=\App\Userverification::where(['emailaddress'=>$emailid,'token'=>$token])->first();
              
           
            
                $user = new \App\User();
                $user->password = Hash::make($Userverificationdetail->password);
                $user->email = $emailid;
                $user->name = $request->cookie('name');
                $user->phoneno = "";
                $user->companyid = $lastid;
                $user->status = 1;
                $user->save();
                $userid=$user->id;
                $remember=true;
                if($user)
                {   
                    $companymodel=new company;
                    $companydetail=$companymodel::where('id',$lastid)->first();
                    $companydetail->createdby=$userid;
                    $companydetail->save();
                    $password = $Userverificationdetail->password;
                    $email = $emailid;
                  // echo $request->get('Account').".".env('APP_URL'); exit;
                        $request->session()->flash('companyid', $lastid);
                        $redirectionlink="http://".$request->get('Account').".".env('APP_URL');
                        return redirect()->intended('signup/profilecompletion');
                       
                }
           
            
        }


        


        //Cookie::queue(Cookie::make('TeamIndustry',  $request->get('TeamIndustry'), 3600));
        
        //return \redirect('signup/step4');
        
    }

    public function profilecompletion(Request $request)
    {
        $inviter=session('inviter');
        $invitee=session('invitee');
        $companyid=session('companyid');
        $invitetoken=session('invitetoken');

        $request->session()->reflash();
        
        $companyid=$request->session()->get('companyid');
        $request->session()->reflash();
        
        $companydetail=\App\company::where(['id'=>$companyid])->first();
        $companyname=$companydetail->companyname;

        $linkurl=$companyname.".".$_SERVER['HTTP_HOST']."/profile";
        $data["linkurl"]=$linkurl;
        $data["subdomainname"]="";
        return View('pages.signup.profilecompletion')->with($data);


        
    }

    public function registerinvited(Request $request,$returnlink)
    {
        $detailarray=explode("-tkn-",$returnlink);
        $emailid=$detailarray[0];
        $tok=$detailarray[1];
        $teaminvitation=new teaminvitation();
        $details=$teaminvitation->where(['invitee'=>$emailid,'token'=>$tok])->first();

        $inviter=$details->inviter;
        $invitee=$details->invitee;
        $companyid=$details->companyid;
        
        
        $request->session()->flash('inviter', $inviter);
        $request->session()->flash('invitee', $invitee);
        $request->session()->flash('companyid', $companyid);
        $request->session()->flash('invitetoken', $tok);

        return redirect()->intended('signup/step1');
        


    }

}
