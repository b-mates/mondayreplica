<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Mail;
use DB;
use App\Jobs\SendInvitation;
use App\userroles;
use App\Boardtype;
use App\Board;
use App\Boardrole;

use \App\dashmodels\themegroups;
use \App\dashmodels\themecategory;
use \App\dashmodels\themes;

use \App\dashmodels\boardgroup;
use \App\Http\Traits\Boards;


class ProfileController extends Controller
{
    use \App\Http\Traits\Boards;
   
    //
    public function index(Request $request)
    {
        
        $boardtype=new boardtype;
        $boardtypes=$boardtype->getBoardtypes();
        $subdomainname = $request->instance()->query('subdomainname');
        $data["subdomainname"]=$subdomainname; 
        $data["boardtypes"]=$boardtypes;  

        return View("pages.user.index")->with($data);

    }


    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

    public function inviteteam(Request $request)
    {
        
        $emaillist=$request->get('emaillist');
        $emailcount=count($emaillist);

            $token = '';

            $length=6;
            for($i = 0; $i < $length; $i++) {
                $token .= mt_rand(0, 9);
            }

            $myuserid=Auth::user()->id;
            $companyid=Auth::user()->companyid;

            $companyinst=new \App\company();
            $company=$companyinst->where('id',$companyid)->first();
            $companyname=$company->companyname;

            $failedemails=array();
        foreach($emaillist as $emails)
        {
            $jsarray["success"]="";
            $data["emailid"]=$emails;
            $data["companyid"]=$companyid;
            $data["companyname"]=$companyname;
            $data["token"]=$token;

            
            $invitationmodel=new \App\teaminvitation();
            $invitationmodel->inviter=$myuserid;
            $invitationmodel->invitee=$emails;
            $invitationmodel->companyid=$companyid;
            $invitationmodel->token=$token;
            $invitationmodel->status=false;
            $returnlink="http://".env('APP_URL')."/signup/registerinvited/".$emails."-tkn-".$token;
            $data["returnlink"]=$returnlink;
            try {
                if($invitationmodel->save())
                {
                    $appurl=env('APP_URL');


                    $emailJob = new SendInvitation($data);
                    if(dispatch($emailJob))
                    {
                        $jsarray["success"]="1";
                        
                    }
                    else 
                    {
                        $jsarray["success"]="1";                        
                    }

                }
                else 
                {
                    $jsarray["success"]="1";
                    
                }
            } catch (\Throwable $th) {
                array_push($failedemails,$emails);
                $jsarray["success"]="0";
            }
            $jsarray["failedemails"]=$failedemails;
        }

        

       
        echo json_encode($jsarray);

        
        
    }
    /*
    Get Board details
    */
    
    public function BoardDetails(Request $request)
    {



        /*Columns based on the plan */
        

        $boardid=$request->get('boardid');
        $data=array();
        
        $board=new Board;
        $boarddata=$board->where('id',$boardid)->firstorfail();
        $boardname= $boarddata->boardname;
        $boarddesc= $boarddata->boarddescription;
        
              
        $boardcontent=$this->getBoardarray($boardid);

        $boardgroup=new boardgroup;
        $boardgroupcollection=$boardgroup->getCollection($boardid);
        
        
   
        //print_r($boardcontent["groupheading"]);
        $data["mainboard"]=$boardcontent["board"];
        $data["boardrows"]=$boardcontent["boardrows"];
        $data["groupheading"]=$boardcontent["groupheading"];
        
        
        $data["boardname"]=$boardname;
        $data["boarddesc"]=$boardname;

        $data["boardgroups"]=$boardgroupcollection;
        $data["bcontent"]=$boardcontent;



        $boardgroupidarray="";
        foreach($boardgroupcollection as $boardgroupvalues)
        {
            $boardgroupidarray.=$boardgroupvalues->id.",";
        }

        
        $request->session()->flash('grouparray', $boardgroupidarray);
        $request->session()->flash('step', '1');
        
        $data["boardgroupsid"]=$boardgroupidarray;

       
        

        return View("pages.user.dash-inbox")->with($data);

    }
    use \App\Http\Traits\Themestrait;
    public function getThemeslist(Request $request)
    {
        //$themes=$this->getThemes($category);
        
        //$data["category"]=$themes;
        
        $request->session()->reflash();

        
        $themecategories=new themecategory;
        $categorylist=$themecategories->getList();
        
        $themesarray=array();
        foreach($categorylist as $clist)
        {

            $categoryid=$clist->id;
            $themesarray=array_merge($themesarray,$this->getThemes($categoryid));
                       
        }

        $data["themesarray"]=$themesarray;
        $hasmany=themecategory::with('themes')->get();
        $data["category"]=$hasmany;
        //return($hasmany);

        return view("includes.themes")->with($data);
    }

    public function createboard(Request $request)
    {
        $boardname=$request->get('boardname');
        $boardtypeval=$request->get('boardtype');
        
        $userroles=new userroles;
        $boardtype=new Boardtype;
        $board=new Board;
        $data=array();
        try
        {
            $request->session()->flash('boardname', $boardname);
            $request->session()->flash('boardtype', $boardtypeval);
            $data["success"]=1;
            //return true;
        }
        catch(\Exception $ex)
        {
            $data["success"]=0;
            //return false;
        }

        echo json_encode($data);
        


    }
    use \App\Http\Traits\Themestrait;
    public function Themeslist(Request $request)
    {
        $category=$request->get('category');
        $themes=$this->getThemes($category);
        return json_encode($themes);
    }

    public function Themescategorylist()
    {
        $themecategories=new themecategory;
        $categorylist=$themecategories->getList();

        $listarray=array();
        foreach($categorylist as $clist)
        {
            $categoryid=$clist->id;
            $categoryname=$clist->themecategory;
            $listarray+=array($categoryid =>$categoryname);

            //$themes=new themes;
            //$themelist=$themes->getThemes($categoryid);
          

        }
            echo(json_encode($listarray));
    }
    public function createboardbck(Request $request)
    {
        $boardname=$request->get('boardname');
        $boardtypeval=$request->get('boardtype');
        
        $userroles=new userroles;
        $boardtype=new Boardtype;
        $board=new Board;
        $defaultvalues["userroleid"]=$userroles->getDefaultvalues();
        $defaultvalues["boardtype"]=$boardtype->getDefaultvalues();
        $array=array();
        if($defaultvalues["userroleid"]!=null && $defaultvalues["boardtype"]!=null)
        {
            $newboardid=0;
            try
            {
               $newboardid=$board->createBoard($boardname,$boardtypeval);
               if($newboardid->id!=null)
               {
                   $boardrole=new Boardrole;
                   $boardrole->addUserrole($newboardid->id,$defaultvalues["userroleid"]);
                   $array["success"]=1;
               }
            }
            catch(\Exception $ex)
            {
                $board->deleteBoard($newboardid->id);
                $array["success"]=0;
            }

            echo json_encode($array);
          
        }
        
    }
}
