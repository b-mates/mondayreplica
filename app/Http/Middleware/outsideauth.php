<?php

namespace App\Http\Middleware;

use Closure;

class outsideauth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $httphost=str_replace("www.","",$_SERVER['HTTP_HOST']) ;
        $subdomainhost= str_replace(env('APP_URL'),'',$httphost) ;
        $subdomainhostlength=strlen($subdomainhost);
        
        $val=explode('.', $httphost)[0];
        $data['subdomain']=$val;
        $data['sub']=$val;
        //if($data['subdomain']!=env('app_url') && count(explode('.', $_SERVER['HTTP_HOST']))>2)
        if($subdomainhostlength>0)
        {           
            return redirect("http://".env('APP_URL')."/signup/step1");
        }
        return $next($request);
    }
}
