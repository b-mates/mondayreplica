<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class appconfigs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $httphost=str_replace("www.","",$_SERVER['HTTP_HOST']) ;
        $subdomainhost= str_replace(env('APP_URL'),'',$httphost) ;
        $subdomainhostlength=strlen($subdomainhost);

        $val=explode('.', $httphost)[0];
        $data['subdomain']=$val;
        $data['sub']=$val;

        $domainname=$httphost;
        $subdomainhost= str_replace(env('APP_URL'),'',$httphost) ;
        $subdomainhostlength=strlen($subdomainhost);
        
       
        if($subdomainhostlength>0 )
        {
            $subdomainname=substr($subdomainhost,0,intval($subdomainhostlength)-1);
            $request->merge(array("subdomainname" => $subdomainname));
            $request->merge(array("sub" => $subdomainname));
           
        }
        return $next($request);
    }
}
