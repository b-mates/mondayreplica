<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class checkauth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $httphost=str_replace("www.","",$_SERVER['HTTP_HOST']) ;
        $subdomainhost= str_replace(env('APP_URL'),'',$httphost) ;
        $subdomainhostlength=strlen($subdomainhost);
        if($subdomainhostlength>0)
        {
            $subdomainname=substr($subdomainhost,0,intval($subdomainhostlength)-1);
          
        }
        if(!Auth::check())
        {   
            $companyname = $subdomainname;          
            $linkurl="http://".$companyname.".".env('APP_URL')."/login";  
            return redirect()->intended($linkurl);
        }
        return $next($request);
    }
}
