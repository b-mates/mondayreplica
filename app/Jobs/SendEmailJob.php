<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerification;
use Auth;


class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct($dat)
    {
        //
        $this->data=$dat;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $emailid=$this->data['emailid'];
        //
        Mail::to($emailid)->queue(new EmailVerification($this->data));
        
    }
}
