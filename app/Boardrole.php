<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use \App\User;

class Boardrole extends Model
{
    protected $table="board_roles";
    //
    protected $fillable=['boardid','userid','userroles','status'];

    public function addUserrole($boardid,$userroles)
    {
        $companyid=Auth::user()->companyid;
        $userid=Auth::user()->id;
        return $this->create(['boardid'=>$boardid,'userid'=>$userid,'userroles'=>$userroles,'status'=>'1'])->id;
    }

    
}
