<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BoardHeading;
class Boardpulseitem extends Model
{
    //
    protected $table="boardpulse-items";
    protected $fillable=['pulsevalue','pulseid','groupid','headingid','status'];

    public function getPulseitems($pulseid,$headingarray)
    {
        $pulseitems=$this->where('pulseid',$pulseid)->wherein('headingid',$headingarray)->get();
        return $pulseitems;
    }

    public function getPulseitemvalue($pulseitemid)
    {
        $pulseitems=$this->where('id',$pulseitemid)->first();
        return $pulseitems;
    }

    public function addPulseItem($groupid,$newheadid,$pulsecount,$pulseid,$defaultvalue)
    {
        
        $this->pulsevalue="";
        $this->pulseid=$pulseid;
        $this->pulsevalue=$defaultvalue;
        $this->groupid=$groupid;
        $this->headingid=$newheadid;
        $this->status=1;
        $this->save();
        return $this;


    }

    public function addBoardpulseitem($boardid,$groupid,$pulseid,$colcontent,$headid)
    {
        
        $this->pulsevalue=$colcontent;
        $this->pulseid=$pulseid;
        $this->groupid=$groupid;
        $this->headingid=$headid;
        $this->status=1;
        $this->save();
               
    }

    public function updateBoardpulseitem($boardid,$pulseid,$colcontent)
    {
        
        $update=$this->find($pulseid);
        $update->pulsevalue=$colcontent;
        $update->save();
        return $update;
               
    } 

    public function deleteBoardpulseitem($pulseid)
    {
        $updatepulse=$this->where('pulseid',$pulseid);
        
        $updatepulse->delete();
        return $updatepulse;
    }

    public function getPreviousPulseitems($pulseid,$groupid)
    {
        $pulseitems= $this->where(['pulseid'=>$pulseid,'groupid'=>$groupid])->get();
        return $pulseitems;
    }

    public function getPulseItemDetails($pulseid)
    {
        $pulseitems= $this->where(['pulseid'=>$pulseid])->get();
        return $pulseitems;
    }

}
