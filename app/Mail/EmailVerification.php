<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\SendEmailJob;


class EmailVerification extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;

    public function __construct($dat)
    {
        //
        $this->data=$dat;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        
        
        return $this->view('forgotpassword')->with($this->data);

        
        
    }
}
