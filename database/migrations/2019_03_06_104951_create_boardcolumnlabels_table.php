<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardcolumnlabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boardcolumnlabels', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->integer('id',true);
            $table->integer("headid");
            $table->string("label");
            $table->string("colorcode");
            $table->boolean("done")->default('0');
            $table->boolean('status')->default('1');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('headid')->references('id')->on('board_headings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boardcolumnlabels');
    }
}
