<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardHeadingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board_headings', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->integer('id',true);
            $table->string('headingname');
            $table->integer('boardid');
            $table->string('columntype');
            $table->integer('sequence');
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('columntype')->references('columnname')->on('columntypes');
            $table->foreign('boardid')->references('id')->on('boards');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('board_headings');
    }
}
