<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeaminvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teaminvitations', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->increments('id');
            $table->integer('inviter');
            $table->string('invitee');
            $table->integer('companyid');
            $table->string("token");
            $table->boolean("status");
            $table->timestamps();
            $table->unique(['invitee','companyid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teaminvitations');
    }
}
