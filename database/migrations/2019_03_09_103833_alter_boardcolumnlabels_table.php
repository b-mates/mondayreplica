<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBoardcolumnlabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('boardcolumnlabels', function (Blueprint $table) {
            //
            $table->integer('defaultvalue')->after('done');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('boardcolumnlabels', function (Blueprint $table) {
            //
            $table->dropColumn('defaultvalue');
        });
    }
}
