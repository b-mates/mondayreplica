<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Boardcolumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boardcolumns', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->integer('id',true);
            $table->integer('columnid');
            $table->string('columnvalue');
            $table->integer('rowindex');
            $table->integer('boardid');
            $table->integer('account');
            $table->boolean('status')->default(true);
            $table->timestamps();

            $table->foreign('columnid')->references('id')->on('boardgroupcolumns');
            $table->foreign('boardid')->references('id')->on('boards');
            $table->foreign('account')->references('id')->on('companies');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boardcolumns');
    }
}
