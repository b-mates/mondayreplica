<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardgroupcolumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boardgroupcolumns', function (Blueprint $table) {
            $table->Engine="InnoDB";
            $table->integer('id',true);
            $table->string('title');
            $table->string('columntype');
            $table->integer('colindex');
            $table->integer('groupid');
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('groupid')->references('id')->on('boardgroups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boardgroupcolumns');
    }
}
