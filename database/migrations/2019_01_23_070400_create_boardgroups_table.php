<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardgroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boardgroups', function (Blueprint $table) {
            $table->Engine="InnoDB";
            $table->integer('id',true);
            $table->string('grouptitle');
            $table->integer('boardid');
            $table->integer('sequence');
            $table->boolean('status');
            $table->timestamps();

            $table->foreign("boardid")->references('id')->on('boards');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boardgroups');
    }
}
