<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardpulseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boardpulse-items', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->integer('id',true);
            $table->string('pulsevalue');
            $table->integer('pulseid');
            $table->integer('groupid');
            $table->integer('headingid');
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('pulseid')->references('id')->on('boardgroup-pulses');
            $table->foreign('groupid')->references('id')->on('boardgroups');
            $table->foreign('headingid')->references('id')->on('board_headings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boardpulse-items');
    }
}
