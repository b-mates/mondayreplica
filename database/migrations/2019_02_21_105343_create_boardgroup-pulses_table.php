<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardgroupPulsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boardgroup-pulses', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->integer('id',true);
            $table->string('pulsename');
            $table->integer('groupid');
            $table->integer('sequence');
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('groupid')->references('id')->on('boardgroups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boardgroup-pulses');
    }
}
