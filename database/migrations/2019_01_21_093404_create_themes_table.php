<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('themes', function (Blueprint $table) {
            $table->Engine="InnoDB";
            $table->integer('id',true);
            $table->string("themename",100);
            $table->string("description")->nullable();
            $table->integer('themecategory');
            $table->string('thumbnails')->nullable();
            $table->mediumText('themetemplate');
            $table->boolean("status")->default(true);
            $table->timestamps();

            $table->foreign('themecategory')->references('id')->on('themecategories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('themes');
    }
}
