<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board_roles', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->integer('id', true);
            $table->integer('boardid');
            $table->integer('userid');
            $table->integer('userroles');
            $table->boolean('status');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->foreign('userid')->references('id')->on('users');
            $table->foreign('boardid')->references('id')->on('boards');
            $table->foreign('userroles')->references('id')->on('userroles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('board_roles');
    }
}
