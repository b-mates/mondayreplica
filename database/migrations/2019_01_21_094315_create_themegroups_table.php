<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThemegroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('themegroups', function (Blueprint $table) {
            $table->Engine="InnoDB";
            $table->integer('id',true);
            $table->integer('themeid');
            $table->integer('sequence')->default(0);
            $table->string('groupname');
            $table->string("description")->nullable();
            $table->boolean("status")->default(true);
            $table->timestamps();

            $table->foreign('themeid')->references('id')->on('themes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('themegroups');
    }
}
