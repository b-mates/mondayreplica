<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Plantocolumnsrelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('--plans-columns', function (Blueprint $table) 
        {

            $table->engine="InnoDB";
            $table->increments('id');
            $table->integer('plans');
            $table->integer('columns');
            $table->boolean('status')->default('1');
            $table->timestamps();

            $table->foreign('plans')->references('id')->on('plans');
            $table->foreign('columns')->references('id')->on('columntypes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('--plans-columns');
    }
}
