<?php $__env->startSection('content'); ?>


<div class="inbox" id="inbox">
    <div class="sec-inbox">
        <div class="col-xs-12 ">
            <div class="border-inbox" style="width:1080px;">
                <div class="tabs" style="width:100%">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#Companies" role="tab"
                                aria-controls="Companies">Companies</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#Users" role="tab" aria-controls="Users"
                                id="li2">Users</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="Companies" role="tabpanel">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Company</th>
                                            <th>Strength</th>
                                            <th>Workarea</th>
                                            <th>Plan</th>
                                            <th>Created by</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($companyDatas): ?>
                                        <?php $__currentLoopData = $companyDatas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $companyDatas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($index +1); ?></td>
                                            <td><a
                                                    href="<?php echo e(route('usersList',$companyDatas->id)); ?>"><?php echo e($companyDatas->companyname); ?></a>
                                            </td>
                                            <td><?php echo e($companyDatas->teamstrength); ?></td>
                                            <td><?php echo e($companyDatas->workarea); ?></td>
                                            <td><?php echo e($companyDatas->planname); ?></td>
                                            <td><?php echo e($companyDatas->name); ?></td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="Users" role="tabpanel">
                            <h4>Users</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Company</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(isset($userList)): ?>
                                        <?php $__currentLoopData = $userList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $userList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($index +1); ?></td>
                                            <td><?php echo e($userList->name); ?></td>
                                            <td><?php echo e($userList->email); ?></td>
                                            <td><?php echo e($userList->companyname); ?></td>
                                            <td><?php if($userList->status =='1'): ?> Active <?php else: ?> Inactive <?php endif; ?></td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('script'); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">   
    $(document).ready(function() {   
        if (window.location.href.indexOf("Users") > -1) {
            target = 'Users';
            $('.nav a').filter('[href="#' + target + '"]').class = 'nav-link';
            $('.nav a').filter('[href="#' + target + '"]').tab('show');
            $("#li2").removeClass("disabled"); // for 3rd li enable 
        } else {
            target = 'Users';
            $('.nav a').filter('[href="#' + target + '"]').class = 'nav-link disabled';
            $("#li2").addClass("disabled"); // for 2nd li disable  
        }
        $('#mytab').on("click", "li", function(event) {
            var activeTab = $(this).find('a').attr('href').split('-')[1];
            alert(activeTab);
            FurtherProcessing(activeTab);
        });
    });
    </script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('mondaymaster.layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('mondaymaster.layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('mondaymaster.layouts.submain', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>