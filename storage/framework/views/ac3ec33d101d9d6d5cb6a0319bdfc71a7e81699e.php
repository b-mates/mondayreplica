<?php $__env->startSection('content'); ?>


<h5>Dashboard</h5>
<div class="inbox" id="inbox">
    <div class="sec-inbox">
        <div class="col-xs-12">
            <div class="border-inbox">
                <div class="col-xs-12 intro-hd">
                    <ul class="list-inline">
                        <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                        <?php endif; ?>
                        Welcome to Admin Dashboard ...
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="preloader-wrapper">
    <div class="preloader">
        <img src="<?php echo e(asset('images/preloader.gif')); ?>" alt="preloader()">
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script>

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('mondaymaster.layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('mondaymaster.layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('mondaymaster.layouts.submain', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>