
<?php


$colcount=0;
?>
<style>
  .dropdown-submenu {
    position: relative;
  }
  
  .dropdown-submenu .dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -1px;
  }
  </style>
    

<?php $__env->startSection('css'); ?>
.style
{
  
}

<?php $__env->stopSection(); ?> 

<?php $__currentLoopData = $boardgroups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $boardgroupsitem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<div class="table-t" id="<?php echo e($boardgroupsitem["id"]); ?>">
    <h2><?php echo e($boardgroupsitem["grouptitle"]); ?><span>           
    <div class="dropdown drop-1">
        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" style="padding:0px !important;">
           <i class="fas fa-angle-down"></i></button>
     </button>
      <ul class="dropdown-menu">
          <li><a href="#"><i class="fas fa-minus-square"></i>Collapse This Group</a></li>
          <li><a href="#"><i class="fas fa-mouse-pointer"></i>Select All Pulses</a></li>
          <li><a href="#"><i class="fas fa-plus-square"></i>Add Group Of Pulses</a></li>
          <li><a href="#" onclick="duplicateGroup(<?php echo e($boardid); ?>,<?php echo e($boardgroupsitem["id"]); ?>)"><i class="fas fa-trash-alt"></i>Duplicate This Group</a></li>        
          <li class="dropdown-submenu">
          <a class="test" tabindex="-1" href="#"><i class="fas fa-trash-alt"></i>Move Group to Boards </a>
          <ul class="dropdown-menu">
          <?php
            if(isset($remainingBoardarray))
              {                  
              ?>
              <h5 style="text-align:center;">Main Boards</h5>
              <?php
                foreach($remainingBoardarray as $remainingBoard)    
                { ?>
                 
                  <li><a style="cursor:pointer;" onclick="getremainingColumns(<?php echo e($boardid); ?>,<?php echo e($remainingBoard->id); ?>,<?php echo e($boardgroupsitem["id"]); ?>)" href="#my_modal" data-toggle="modal" data-target="#myModal"  data-id="<?php echo e($remainingBoard->id); ?>" class="open-AddBookDialog btn btn-primary" ><i class="fa fa folder"></i><?php echo e($remainingBoard->boardname); ?></a></li>
                <?php }
              }
          ?>
          </ul>
        </li>
        <li><a href="#"><i class="fas fa-pencil-alt"></i>Rename Group</a></li>
        <li><a href="#"><i class="fas fa-dice-three"></i>Change Group Color</a></li>
        <li><a href="#"><i class="fas fa-file-export"></i>Export to Excel</a></li>
        <li><a href="#"><i class="fas fa-trash-alt"></i>Delete</a></li>
        <li><a href="#"><i class="fas fa-file-archive"></i>Archive</a></li>
      </ul>
    </div>
    </span>
    </h2>
  </div>
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
               
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <input type="hidden" name="txtselboard" id="txtselboard"/>
              <input type="hidden" name="boardid" id="boardid"/>
              <input type="hidden" name="groupid" id="groupid"/>
              <div class="modal-body" id="result">
              </div>
              <div style="heght:60px;"></div>
              <div>You should manually create these columns in the destination board <br/>before moving the group,otherwise the information in theses columns <br/>will be deleted</div>
              <div style="heght:80px;"></div>
              <div>
                  &nbsp;<button class="btn  btn-info btn-outline btn-rounded" data-dismiss="modal"><i class="zmdi zmdi-plus-circle"></i>Cancel</button>
                  &nbsp;<button class="btn  btn-info btn-outline btn-rounded" onclick="moveGrouptoBoard(document.getElementById('boardid').value,document.getElementById('txtselboard').value,document.getElementById('groupid').value)"><i class="zmdi zmdi-plus-circle"></i> Move Group </button>
                </div> 
            </div>
            
          </div>
        </div>
      <div  class="table-t">

          <div style="margin-left:5px;" class="row" id="<?php echo e($boardgroupsitem["id"]); ?>-row"> 
              <div  class="col-sm" style="max-width:40px;border-right:solid 1px #fff;height:35px"></div>
              <div class="col-sm" style="border-left:solid 1px #fff;border-right:solid 1px #fff;"></div>
              <?php $__currentLoopData = $boardheadings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $heading): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <div  class="col-sm " style="font-family:Helvetica, Arial;border-left:solid 1px #fff;border-right:solid 1px #fff;"><span name="<?php echo e($heading->id); ?>-<?php echo e($boardgroupsitem["id"]); ?>-span" id="<?php echo e($heading->id); ?>-<?php echo e($boardgroupsitem["id"]); ?>-span" style="cursor:pointer" onclick="editBoardheadlabel(this,<?php echo e($heading->id); ?>,<?php echo e($boardgroupsitem["id"]); ?>)"  class="boardheadings"><?php echo e($heading->headingname); ?></span><input class="boardheadtext" style="display:none" type="text" name="head-<?php echo e($boardgroupsitem["id"]); ?>-<?php echo e($heading->id); ?>" id="head-<?php echo e($boardgroupsitem["id"]); ?>-<?php echo e($heading->id); ?>" value="<?php echo e($heading->headingname); ?>" onblur="editBoardheadblur(this,<?php echo e($heading->id); ?>,<?php echo e($boardgroupsitem["id"]); ?>)" /></div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              
              <div class="col-sm" style="background-color:#6c757d;max-width:50px;height:35px">
                  <div class="btn-group" style="background-color:#6c757d;max-width:50px;height:35px">
                  <input type="hidden" name="boardhid" id="boardhid" value="<?php echo e($boardid); ?>" /> 
                   <button class="btn btn-secondary btn-sm" type="button">
                       <i class="fas fa-plus"></i>
                   </button>
                   <button type="button" class="btn btn-sm btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <span class="sr-only">Toggle Dropdown</span>
                   </button>
                   <div class="dropdown-menu">
                     <ul class="list-group">
                       
                       <?php $__currentLoopData = $planbasedcolumns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cols): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <li class="list-group-item" onclick="addBoardcolumn(<?php echo e($cols["id"]); ?>)" style="cursor: pointer"><?php echo e($cols["displayname"]); ?></li>
                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </ul>
                   </div>
                 </div>
               </div>
              

          </div>


          <?php
          
              $pulsearray=$grouppulses[$boardgroupsitem["id"]];
              
              foreach($pulsearray as $parray)
              {  
                  $pulseid=$parray["id"];
                  echo '<div style="margin-left:5px;height:30px;background-color:#dcdcdc;border-bottom:solid 1px #fff" class="row" id="{{$boardgroupsitem["id"]}}-row">';
                  
                  echo '<div  class="col-sm" style="max-width:40px;"><a href="javascript:remPulse('.$pulseid.')">X</a></div><div  class="col-sm"><span data-pk="'.$pulseid.'"  data-groupid="'.$boardgroupsitem["id"].'" class="pulseid">'.$parray["pulsename"].'</span><input onkeyup ="updatePulse(this,'.$pulseid.','.$boardgroupsitem["id"].')" onblur ="editpulseblur(this,'.$pulseid.')" style="display:none" class="edit-pulseinput" type="text" id="'.$pulseid.'" name="'.$pulseid.'" value="'.$parray["pulsename"].'" /><div><img  onclick="editPulselabel(this,'.$pulseid.')" style="width:15px;margin-top:7px" src="images/edit.png" /></div></div>';

                  $groupval=$pulseitemsarray[$boardgroupsitem["id"]];

                  $pulsevaluesarray=$groupval[$pulseid];
                  $i=0;
                  foreach($pulsevaluesarray as $pitemsarray)
                  {
                    
                    $columntype=$boardheadings[$i]->columntype;
                   // echo "aa". $columntype;
                    if($columntype=="longtext")
                    {
                      echo '<div class="component col-sm '.$boardheadings[$i]->columntype.'" id="component'.$pitemsarray["id"].'" componenthead="'.$boardheadings[$i]->id.'" alt="component" componentvalueid="'.$pitemsarray["id"].'"  component="component-'.$boardheadings[$i]->columntype.'" style="border-left:solid 1px #fff;border-right:solid 1px #fff;cursor:pointer">'.$pitemsarray["pulsevalue"].'</div>';
                    }
                    else if($columntype=="statuscolumn")
                    {
                      echo '<div class="component  col-sm '.$boardheadings[$i]->columntype.'" pulse="pulse-'.$pulseid.'" componenthead="'.$boardheadings[$i]->id.'" id="component'.$pitemsarray["id"].'" alt="component" componentvalueid="'.$pitemsarray["id"].'"  data-trigger="focus"  component="component-'.$boardheadings[$i]->columntype.'" style="'.$boardheadingcss[$boardheadings[$i]->id."-".$pitemsarray["pulsevalue"]].';border-left:solid 1px #fff;border-right:solid 1px #fff;cursor:pointer"><a style="cursor: pointer" class="comments" rel="comments" title="Enter comments">'.$pitemsarray["pulsevalue"].'</a></div>';
                    }
                    
                    
                    $i++;

                  }
                  echo '<div class="col-sm" style="max-width:50px"> </div>';
                  echo'</div>';

                  
              }

              
          ?>
        
          <div style="margin-left:5px;margin-bottom:20px;margin-top:5px" class="row" id="<?php echo e($boardgroupsitem["id"]); ?>-newpulserow">
            <div class="col-sm-1" style="max-width:10px"></div><div class="col-sm-3">
                  <div ><input type="text" placeholder="New Pulse" class="form-control"  id="<?php echo e($boardgroupsitem["id"]); ?>-newpulse"></div>
              </div>
              <div class="col-sm-3">
                  <button id="<?php echo e($boardgroupsitem["id"]); ?>-newpulsebtn" onclick="addPulse(this)">Add pulse</button>
              </div>
          </div>
          
           
          

      </div>

      
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<div class="greet"></div>

<input type="hidden" id="currentpulse" value="" />
<input type="hidden" id="currcomponentval" value="" />

<script>

    $(document).ready(function(){
      $('.dropdown-submenu a.test').on("click", function(e){
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
      });
    });

    $(document).click('body',function(){
      $(".greet").hide();
    });
  </Script>
