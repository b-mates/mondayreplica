<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Admin Control Panel</title>
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">


<link href="<?php echo e(asset('css/bootstrap.min.css')); ?>" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" >
<link href="<?php echo e(asset('css/style.css')); ?>" type="text/css" rel="stylesheet">


<script src="<?php echo e(asset('js/jquery.min.js')); ?>"  crossorigin="anonymous"></script> 
<script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>

</head>
<body>
<div class="container-fluid menu">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light p-0"> <a class="navbar-brand" href="/"><img src="<?php echo e(asset('images/Logo.jpg')); ?>" alt=""/></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse Menu-content" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto ">
        
              <?php if(Auth::check()): ?>
                <li class="nav-item"> <a class="nav-link" href="profile">Home</a> </li>
              <?php else: ?>
              <?php if(auth()->guard('admin')->guest()): ?>
                <li class="nav-item"> <a class="nav-link" href="<?php echo e(route('login')); ?>">Login</a> </li>
               
                 <li class="nav-item"> <a class="nav-link" href="<?php echo e(route('register')); ?>">Register</a> </li>
                <?php endif; ?>
              <?php endif; ?>
          
        </ul>
      </div>
    </nav>
  </div>
</div>

<div id="main" class="row">
    
    <?php echo $__env->yieldContent('content'); ?>
    <?php echo $__env->yieldContent('script'); ?>
</div>
<!-- <div class="container-fluid footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 mt-4">
          <ul class="list-unstyled f-logo">
            <li><img src="<?php echo e(asset('images/Logo.png')); ?>" width="167" height="61" alt=""/></li>
            <li class="mb-2"><img src="<?php echo e(asset('images/google-play.jpg')); ?>" width="110" height="36" alt=""/></li>
            <li><img src="<?php echo e(asset('images/Applestore.jpg')); ?>" width="111" height="39" alt=""/></li>
          </ul>
        </div>
        <div class="col-sm-9 mt-5 f-content">
          <div class="row">
            <div class="col-md-3 col-6">
              <ul class="list-unstyled">
                <li>Our product</li>
                <li>Product</li>
                <li>Stories</li>
                <li>Pricing</li>
                <li>Partners/Affiliates</li>
                <li>Find a partner</li>
                <li>Templates</li>
                <li>Integrations</li>
                <li>Formerly dapulse</li>
                <li>Developers</li>
              </ul>
            </div>
            <div class="col-md-3 col-6">
              <ul class="list-unstyled">
                <li>About Us</li>
                <li>Contact Us</li>
                <li>Careers</li>
                <li>In The News</li>
                <li>Press Kit</li>
                <li>Blog</li>
              </ul>
            </div>
            <div class="col-md-3 col-6">
              <ul class="list-unstyled">
                <li>Client management</li>
                <li>Bug tracking</li>
                <li>Project design</li>
                <li>Lead tracking</li>
                <li>Task management</li>
                <li>All use cases</li>
              </ul>
            </div>
            <div class="col-md-3 col-6">
              <ul class="list-unstyled">
                <li>Daily Webinars</li>
                <li>Guides</li>
                <li>Support</li>
                <li>Security</li>
                <li>ISO 27001 / 27018</li>
                <li>SOC 2</li>
                <li>GDPR Ready</li>
                <li>Legal, Security & Privacy</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid s-footer">
    <div class="container">
      <div class="row social">
        <div class="col-sm-6 ">
          <p>2018 Seventyseven, Inc. All rights reserved.</p>
        </div>
        <div class="col-sm-6">
          <ul class="list-inline float-right">
            <li class="float-left pr-2"><i class="fab fa-facebook-square"></i></li>
            <li class="float-left pr-2"><i class="fab fa-twitter-square"></i></li>
            <li class="float-left pr-2"><i class="fab fa-linkedin"></i></li>
            <li class="float-left pr-2"><i class="fab fa-google-plus-square"></i></li>
          </ul>
        </div>
      </div>
    </div>
  </div>  -->
  </body>
  </html>


