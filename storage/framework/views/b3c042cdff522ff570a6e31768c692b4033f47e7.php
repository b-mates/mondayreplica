<?php $__env->startSection('content'); ?>



<div class="inbox" id="inbox">
    <h5>Themes</h5>
    <div class="sec-inbox">
        <div class="col-xs-12 ">
            <div class="border-inbox" style="width:1080px;">
                <div class="tabs" style="width:80%">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#categories" role="tab"
                                aria-controls="categories">Category</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#themes" role="tab" aria-controls="themes"
                                id="li2">
                                Themes</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="categories" role="tabpanel">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Category</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(isset($themecategory)): ?>
                                        <?php $__currentLoopData = $themecategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $themecategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($index +1); ?></td>
                                            <td>
                                                <a href="<?php echo e(route('themeList',$themecategory->id)); ?>">
                                                    <?php echo e($themecategory->themecategory); ?></a>
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="themes" role="tabpanel">
                            <div class="tab-pane" id="themes" role="tabpanel">
                                <?php if(isset($themeid)): ?>
                                <nav id="myNavbar" class="navbar navbar-default" role="navigation">
                                    <div class="container">
                                        <div class="navbar-header">
                                            <a class="navbar-toggle" data-toggle="collapse"
                                                data-target="#bs-example-navbar-collapse-1" href="#">New</a>
                                        </div>
                                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                            <!-- <ul class="nav navbar-nav ">
                                                <li class="dropdown"> -->
                                                    <!-- <a href="#" data-toggle="dropdown"
                                                        class="dropdown-toggle">Columns</a> -->
                                                
                                                    <ul class="nav navbar-nav">
                                                        <?php if(isset($colTypes)): ?>
                                                        <?php $__currentLoopData = $colTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cols): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <li onclick="addcolumn('<?php echo e($cols ["columnname"]); ?>','<?php echo e($cols["id"]); ?>','<?php echo e($catId); ?>','<?php echo e($themeid); ?>')" style="cursor: pointer;color:red;">
                                                            <?php echo e($cols["columnname"]); ?></li>
                                                        </p>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <?php endif; ?>
                                                    </ul>
                                                <!-- </li>
                                            </ul> -->
                                        </div>  
                                    </div>
                                </nav>
                                <table>
                                        <tr>
                                            <td>
                                                <table>
                                                    <?php
                                                  //  dd($themeDesign);
                                                    foreach($groupsarray as $gpvalues)
                                                    {
                                                        $groupnames=$gpvalues;
                                                        $columnsValue=(array)$themeDesign[$groupnames]["pulses"];
                                                        $headingarray=$themeDesign['heading'];
                                                        $colcount=count($headingarray);
                                                    ?>
                                                    <tr>
                                                            <td>
                                                                <h2><?php echo e($groupnames); ?></h2>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                                <td>
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <thead>
                                                                            <tr class="colTr">
                                                                                <td style="width:214px;"></td>
                                                                                <?php
                                                                                $index=0;
                                                                                $divCnt=count($headingarray);
                                                                                foreach($headingarray as $columsTitle)
                                                                                {
                                                                                ?>
                                                                                <td style="width:180px;text-align:center;" contenteditable="true">
                                                                                <input type="text" name="txtCol_<?php echo e($index); ?>" id="txtCol_<?php echo e($index); ?>" value="<?php echo e($columsTitle); ?>" style="border:none;text-align:center;"
                                                                                onchange="updateTemplateColumns('<?php echo e($themeid); ?>','<?php echo e($catId); ?>','<?php echo e($columsTitle); ?>',this.value)"/>                                                            
                                                                                </td>
                                                                                <?php
                                                                                $index++;
                                                                                }
                                                                                ?>
                                                                            </tr>
                                                                            <tr class="rowTr">
                                                                                <td style="width:214px;"></td>
                                                                                <?php
                                                                                $coli=0;
                                                                                $colcount=2;
                                                                                for($i=0;$i<$colcount;$i++)
                                                                                {
                                                                                   // echo $i."<br/>";
                                                                                    $pulses=$columnsValue[$i];
                                                                                    $pulsescontent=(array)$themeDesign[$groupnames]["pulses"][$pulses];
                                                                                    $colcnt=count($pulsescontent);
                                                                               // $valarray=(array)$sampleval;
                                                                               for($j=0;$j<$colcnt;$j++)
                                                                                {
                                                                                   // echo $i."<br/>";
                                                                                    $colcontent=$pulsescontent[$j];
                                                                                $coli++;
                                                                                ?>
                                                                                <td style="text-align:center;" contenteditable="true">
                                                                                <input type="text" name="txtrow_<?php echo e($coli); ?>" id="txtrow_<?php echo e($coli); ?>" value="<?php echo e($colcontent); ?>" style="border:none;text-align:center;"
                                                                                onchange="updateTemplateColRows('<?php echo e($themeid); ?>','<?php echo e($catId); ?>','<?php echo e($colcontent); ?>',this.value,<?php echo e($i); ?>,'<?php echo e($groupnames); ?>')"/>
                                                                                </td>
                                                                                <?php
                                                                                if($coli%$divCnt==0)
                                                                                {
                                                                                echo '
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                ';
                                                                                }
                                                                                }
                                                                                }
                                                                                ?>
                                                                            </tr>
                                                                        </thead>
                                                                    </table>
                                                                </td>
                                                            </tr>                                                       
                                                       
                                                    <?php
                                                    }
                                                    ?>
                                                </table>
                                            </td>
                                        </tr>
                                </table>
                                <?php else: ?>
                                <div class="rTable table table-bordered">
                                    <div class="table-responsive">

                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Description</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if(isset($themes)): ?>
                                                <?php $__currentLoopData = $themes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $themes): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($index +1); ?></td>
                                                    <td><?php echo e($themes->themename); ?></td>
                                                    <td><?php echo e($themes->description); ?></td>
                                                    <td>
                                                        <a
                                                            href="<?php echo e(route('themeLists',['catId'=>$catId,'theme'=>$themes->id])); ?>">
                                                            <img src="<?php echo e(asset('admin/images/view.gif')); ?>" /></a>
                                                    </td>
                                                </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('script'); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        var url = window.location.href;
        var segment = url.split("/").length - 1 - (url.indexOf("http://") == -1 ? 0 : 2);
        //  alert(segment);
        if (window.location.href.indexOf("themes") > -1) {
            target = 'themes';
            $('.nav a').filter('[href="#' + target + '"]').class = 'nav-link';
            $('.nav a').filter('[href="#' + target + '"]').tab('show');
            $("#li2").removeClass("disabled"); // for 3rd li enable 
        } else if (window.location.href.indexOf("themes") == -1) {
            if (segment >= 4) { //alert('111');
                target = 'themes';
                $('.nav a').filter('[href="#' + target + '"]').class = 'nav-link';
                $('.nav a').filter('[href="#' + target + '"]').tab('show');
                $("#li2").removeClass("disabled"); // for 3rd li enable 
            } else {
                //alert('2222);')
                target = 'themes';
                $('.nav a').filter('[href="#' + target + '"]').class = 'nav-link disabled';
                $("#li2").addClass("disabled"); // for 2nd li disable           
            }

        } else {
            target = 'themes';
            $('.nav a').filter('[href="#' + target + '"]').class = 'nav-link disabled';
            $("#li2").addClass("disabled"); // for 2nd li disable           
        }


        // $('#mytab').on("click", "li", function(event) {
        //     var activeTab = $(this).find('a').attr('href').split('-')[1];
        //     alert(activeTab);
        //     FurtherProcessing(activeTab);
        // });
    });
    </script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('mondaymaster.layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('mondaymaster.layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('mondaymaster.layouts.submain', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>