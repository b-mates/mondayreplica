<?php $__env->startSection('content'); ?>
<div class="container-fluid banner-sec">
    <div class="container">

      <form name="loginform" id="loginform" method="POST" action="<?php echo e(route('postlogin')); ?>">
      <?php echo csrf_field(); ?>
      <div class="row">
        <div class="col-sm-4 login mt-5">
            <h2>Log In</h2>
            <p>Enter your accounts web address:</p>
                <?php if($errors->any()): ?>
                    <div class="alert alert-danger">
                      <ul>
                          <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <li><?php echo e($error); ?></li>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </ul>
                    </div>
                  <?php endif; ?>

                  <?php if(session()->has('message')): ?>
                      <div class="alert alert-danger">
                          <?php echo e(session()->get('message')); ?>

                      </div>
                  <?php endif; ?>
            <div class="input-group mb-3">
              <input type="text" class="form-control" id="emailid" name="emailid"  aria-label="Recipient's username" aria-describedby="button-addon2">
            </div>
            <p>Password:</p>
            <div class="input-group mb-3">
              <input type="password" class="form-control" id="password" name="password"  aria-label="Recipient's username" aria-describedby="button-addon2">
              
            </div>
            <p><a href="/forgotpassword">Forgot your account's web address?</a></p>
            <button type="button" class="btn btn-primary" name="login" id="login">Login</button><input type="hidden" name="sub" id="sub" value="<?php echo e($subdomain); ?>" />
        </div>
        <div class="col-sm-8 mr-auto mb-5 imp "><img src="<?php echo e(asset('images/desktop-banner.jpg')); ?>" class="w-100 mt-4" alt=""/></div>
      </div>
      </form>

    </div>
  </div>
  <?php $__env->startSection('script'); ?>
<script type="text/javascript">
  
      $("#login").click(function(event){
        event.stopPropagation();
        $("#loginform").submit();
      });
 
    
</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.singlecolumn', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>