<?php
  //  print_r($bcontent);
?>
<div class="position">
        
        <div class="col-12 p-0" style="display:inline-block;">
        
          <div class="discrip">
              <h2><?php echo e($boardname); ?></h2>
              <p><a href="#" id="ht-area"><?php echo e($boarddesc); ?></a></p>
              <textarea rows="3" class="textarea s-here" placeholder="Text goes here"> </textarea> 
            
            
   
          </div>
          <div class="right-side">
            <ul class="list-inline">
              <li class=" float-left"><a href="#"><i class="fab fa-creative-commons-sampling"></i></a></li>
              <li class=" float-left"><a href="#" data-toggle="modal" data-target="#add-view">Add view</a></li>
              <li class=" float-left">|</li>
              <li class=" float-left"><a href="#">Unsubscribe</a></li>
              <li class=" float-left"><a href="#" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-users"></i></a></li>
              <li class=" float-left"><a href="#" onclick="openNav()"><i class="fas fa-chart-line"></i></a></li>
              <li class=" float-left">
                <div class="dropdown drop-2">
                  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="padding: 0px 0px !important;"><i class="fas fa-bars"></i></button>
                  <ul class="dropdown-menu">
                    <li><a href="#"><i class="fas fa-plus-circle"></i>Add Group of pulses</a></li>
                    <li><a href="#"><i class="fas fa-pencil-alt"></i>Rename Board</a></li>
                    <li><a href="#"><i class="fas fa-lock"></i>Board Permissions</a></li>
                    <li><a href="#"><i class="fas fa-users"></i>Board Subscribers</a></li>
                    <li><a href="#"><i class="fas fa-envelope"></i>Email Preferences</a></li>
                    <li><a href="#"><i class="far fa-copy"></i>Duplicate Board &gt;</a></li>
                    <li><a href="#"><i class="fas fa-exchange-alt"></i>Change Board Type</a></li>
                    <li><a href="#"><i class="fas fa-file-excel"></i>Export To Excel</a></li>
                    <li><a href="#"><i class="far fa-calendar-alt"></i>Calender Intergration</a></li>
                    <li><a href="#"><i class="fas fa-print"></i>Print</a></li>
                    <li><a href="#"><i class="fas fa-arrows-alt"></i>Full Screen</a></li>
                    <li><a href="#"><i class="fas fa-file-archive"></i>Archived Pulses/ Groups</a></li>
                    <li><a href="#"><i class="fas fa-trash-alt"></i>Delete</a></li>
                    <li><a href="#"><i class="fas fa-file-archive"></i>Archive this Board</a></li>
                  </ul>
                </div>
              </li>
            </ul>
            <div id="mySidenav" class="sidenav"> <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
              <div class="slider-menu">
                <div class="col-xs-12 mar-g">
                  <div class="col-sm-6 c-u menu-qq">
                    <h5>ROW</h5>
                  </div>
                  <div class="col-sm-6 menu-qq PR-menu">
                    <ul class="list-inline" style="margin-left:3px;margin-top:2px;">
                      <li style="margin-bottom:0px;margin-right:1px;" class="flot-l"><a href="#"><i class="fas fa-plus" style="font-size:16px;"></i></a></li>
                      <li class="flot-l" style="margin-bottom:0px;margin-right:1px;">
                        <div class="person-1"> <a href="#"><img src="images/U.jpg"></a></div>
                      </li>
                      <li class="flot-l" style="margin-right:1px;">
                        <div class="dropdown drop-2" style="margin-top:5px;">
                          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="padding: 0px 0px !important;" aria-expanded="false"><i class="fas fa-bars"></i></button>
                          <ul class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-98px, -25px, 0px);">
                            <li><a href="#"><i class="fas fa-pencil-alt"></i>Manage Subscribers</a></li>
                            <li><a href="#"><i class="fas fa-envelope"></i>Email Preferences</a></li>
                            <li><a href="#"><i class="fas fa-file-export"></i>Expert to Excel</a></li>
                            <li><a href="#"><i class="fas fa-copy"></i>Copy Pulse</a></li>
                            <li><a href="#"><i class="fas fa-file-archive"></i>Archived Pulse</a></li>
                            <li><a href="#"><i class="fas fa-trash-alt"></i>Delete Pulse</a></li>
                            <li><a href="#"><i class="fas fa-tv"></i> Pulse TV</a></li>
                          </ul>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="clearfix"></div>
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                  <li class="nav-item"> <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Updates</a> </li>
                  <li class="nav-item"> <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Inbox</a> </li>
                  <li class="nav-item"> <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-active" role="tab" aria-controls="pills-profile" aria-selected="false">Active fog</a> </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="col-12 updates">
                      <input type="text" placeholder="Write updates">
                      <a href="">Write update via email</a> </div>
                    <p class="no-updates">No updates Yet</p>
                  </div>
                  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="col-12">
                      <div class="in-box">
                        <ul>
                          <li><i class="fas fa-pencil-alt"></i> Q&amp;A list</li>
                          <li>
                            <div class="dropdown">
                              <button class=" dropdown-toggle" type="button" id="dropdownMenuButton3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-bars"></i> </button>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> <a class="dropdown-item" href="#"><i class="fas fa-pencil-alt"></i> Action</a> <a class="dropdown-item" href="#"><i class="fas fa-pencil-alt"></i> Another </a> <a class="dropdown-item" href="#"><i class="fas fa-pencil-alt"></i> Something </a> </div>
                            </div>
                          </li>
                        </ul>
                        <p>Question</p>
                        <input type="text">
                        <p>Answer</p>
                        <div class="answer-div"><img src="images/answer.jpg" alt="" style="width: 100%"></div>
                        <ul>
                          <li><a href="#" target="_blank">Q&amp;A List</a></li>
                          <li><a href="" class="save-btn">Save</a></li>
                          <li><a href="" class="p-0">Cancel</a></li>
                        </ul>
                      </div>
                      <div class="in-box">
                        <ul>
                          <li><i class="fas fa-pencil-alt"></i> Q&amp;A list</li>
                          <li>
                            <div class="dropdown">
                              <button class=" dropdown-toggle" type="button" id="dropdownMenuButton33" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-bars"></i> </button>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> <a class="dropdown-item" href="#"><i class="fas fa-pencil-alt"></i> Action</a> <a class="dropdown-item" href="#"><i class="fas fa-pencil-alt"></i> Another </a> <a class="dropdown-item" href="#"><i class="fas fa-pencil-alt"></i> Something </a> </div>
                            </div>
                          </li>
                        </ul>
                        <ul class="file-info">
                          <li>File Name </li>
                          <li>File Updates</li>
                          <li>Notes</li>
                        </ul>
                        <ul class="mb-0">
                          <li class="add-file">
                            <button><i class="fas fa-pencil-alt"></i> Add file</button>
                            <input type="file">
                          </li>
                        </ul>
                      </div>
                      <div class="in-box">
                        <ul>
                          <li><i class="fas fa-pencil-alt"></i> Q&amp;A list</li>
                          <li>
                            <div class="dropdown">
                              <button class=" dropdown-toggle" type="button" id="dropdownMenuButton44" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fas fa-bars"></i> </button>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> <a class="dropdown-item" href="#"><i class="fas fa-pencil-alt"></i> Action</a> <a class="dropdown-item" href="#"><i class="fas fa-pencil-alt"></i> Another </a> <a class="dropdown-item" href="#"><i class="fas fa-pencil-alt"></i> Something </a> </div>
                            </div>
                          </li>
                        </ul>
                        <p>Question</p>
                        <input type="text">
                        <p>Answer</p>
                        <div class="answer-div"><img src="images/answer.jpg" alt="" style="width: 100%"></div>
                        <ul>
                          <li><a href="#" target="_blank">Q&amp;A List</a></li>
                          <li><a href="" class="save-btn">Save</a></li>
                          <li><a href="" class="p-0">Cancel</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="pills-active" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="content">
                      <ul class="list-inline">
                        <li class="float-left" style="margin-bottom:-3px;">
                          <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="padding:10px 40px !important;color:#FFF !important 	;">Filter Log</button>
                            <ul class="dropdown-menu">
                              <li style="margin-bottom:3px !important;"><a href="#">HTML</a></li>
                              <li style="margin-bottom:3px !important;"><a href="#">CSS</a></li>
                              <li style="margin-bottom:3px !important;"><a href="#">JavaScript</a></li>
                            </ul>
                          </div>
                        </li>
                        <li class="float-left" style="margin-bottom:-3px;"><a href="#"><i class="far fa-user-circle"></i></a></li>
                      </ul>
                      <div class="cont-ent">
                        <ul class="list-inline">
                          <li class="float-left" style="margin-top:15px;"><i class="far fa-clock"></i> 3h</li>
                          <li class="float-left"><a href="#"><i class="far fa-user-circle"></i> Created this board</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="searchbox" class="form-in mt-3">
          <ul class="list-inline">
            <li class="float-left"><a href="#"><i class="fas fa-angle-down"></i></a></li>
            <li class="float-left"></li>
            <li class="float-left" style="width:40%;">
              <input type="text" placeholder="Search Everything.." name="search">
            </li>
            <li class="float-left drop-menu">
              <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> <i class="fas fa-ellipsis-h" style="margin-right:10px;"></i></button>
                <ul class="dropdown-menu">
                  <li>Filter your board<span>Tital 5 pulses</span> <span><a href="#">Export to Excel</a></span><span><a href="#">Clear</a></span></li>
                  <li><a href="#">
                    <div class="head">
                      <p>Groups</p>
                      <ul class="list-unstyled">
                        <li>Group Titile<span>1</span></li>
                        <li>Group Titile <span>2</span></li>
                      </ul>
                    </div>
                    <div class="head">
                      <p>Person</p>
                      <ul class="list-unstyled">
                        <li>Group Titile<span>1</span></li>
                        <li>Group Titile <span>2</span></li>
                      </ul>
                    </div>
                    <div class="head">
                      <p>Status</p>
                      <ul class="list-unstyled statuss">
                        <li><span class="Wornt"></span>Work on it</li>
                        <li><span class="Wornt"></span>Work on it </li>
                      </ul>
                    </div>
                    </a></li>
                </ul>
              </div>
            </li>
            <li><a href="#"><i class="fas fa-user-circle"></i></a></li>
          </ul>
        </div>
        
     


        
     
          
      