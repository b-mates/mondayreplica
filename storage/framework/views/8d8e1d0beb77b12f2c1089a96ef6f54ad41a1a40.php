<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
  <head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>77</title>
    <link rel="shortcut icon" href="<?php echo e(asset('images/favicon.png')); ?>"/>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <![endif]-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <link href="css/style.css" type="text/css" rel="stylesheet">
    <link href="css/mystyle.css" type="text/css" rel="stylesheet">
      
      <script src="<?php echo e(asset('js/jquery.min.js')); ?>"  type="text/javascript"></script> 
      <script src="<?php echo e(asset('js/Popper.js')); ?>" type="text/javascript"></script>
      <script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"  type="text/javascript"></script>
      <script src="<?php echo e(asset('js/app.js')); ?>"  type="text/javascript"></script> 
      <script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"  type="text/javascript"></script>  
      <script src="<?php echo e(asset('js/app.js')); ?>"  type="text/javascript"></script>  

      <script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>

      <script src="<?php echo e(asset('js/scripts.js')); ?>"  type="text/javascript"></script>  
      <script src="<?php echo e(asset('js/custom.js')); ?>" type="text/javascript"></script> 

      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <script src="js/inline-edit.jquery.js"></script>
      <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
      <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

      
     

  </head>
    

<body>






<?php echo $__env->yieldContent('content'); ?>


<?php echo $__env->yieldContent('script'); ?>


 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  



<!--<left side new.....>-->

</body>
</html>
