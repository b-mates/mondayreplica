<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$appurl=env('APP_URL');
Route::group(['domain' => '{sub}.'.env('APP_URL'),'middleware' => ['chksubdomain', 'appconfigs']], function () 
{        
        

        Route::get('/forgotpassword', 'LoginController@forgotpassword')->name('forgotpassword');


       // Route::get('/logout','ProfileController@logout');
        
       Route::get('/getSubmenu', 'LoginController@changepassword')->name('changepassword');

       
   

});

Route::get('LeftMenu', 'WidgetsController@LeftMenu')->name('LeftMenu')->middleware('checkauth');

Route::get('/logout','ProfileController@logout')->middleware('checkauth');

Route::group(['domain' => '{sub}.'.env('APP_URL'),'middleware' => [ 'appconfigs']], function ($appurl) 
{  

    
  
    Route::get('/login', 'LoginController@login')->name('login')->middleware('appconfigs');
   
   
   
    Route::get('/profile', 'ProfileController@index')->name('profile')->middleware('checkauth');

    /*Get specific board details */
    Route::get('profile/BoardDetails','ProfileController@boarddetails')->name('BoardDetails')->middleware('checkauth');
     /*Get specific board details */
     Route::get('profile/Themescategorylist','ProfileController@Themescategorylist')->name('Themescategorylist')->middleware('checkauth');
     Route::get('profile/Themeslist','ProfileController@Themeslist')->name('Themeslist')->middleware('checkauth');

     Route::get('profile/getThemeslist','ProfileController@getThemeslist')->name('getThemeslist')->middleware('checkauth');
     
});  


Route::group(['prefix' => 'widget'], function() {
    //
    
    Route::post('whichinput',"WidgetsController@chooseControl" );
    Route::post('saveContent',"WidgetsController@saveComponent" );
    Route::post('getStatuscolumns',"WidgetsController@getStatuscolumns" );
    
});


Route::group(['prefix' => 'template'], function() {
    //
    
    Route::post('/createtemplate','TemplateController@createtemplate');
    Route::get('/boardrowsandcolums','TemplateController@boardrowsandcolums');
    Route::get('/populatedata','TemplateController@populatedata');
    Route::get('/populateheading','TemplateController@populateheading');

    Route::post('/addPulse','TemplateController@addPulse');

    Route::post('/managePulse','TemplateController@managePulse');
    Route::post('/postEditpulse','TemplateController@postEditpulse');
    Route::post('/boardStatuschange','TemplateController@boardStatuschange');
    


    Route::post('/duplicateGroup','TemplateController@duplicateGroup' );  //added by Athira on 28.02.2019
    Route::post('/addGroup','TemplateController@addGroup' );  //added by Athira on 01.03.2019
    Route::get('/getremainingColumns','TemplateController@getremainingColumns');  //added by Athira on 01.03.2019
    Route::post('/moveGrouptoBoard','TemplateController@moveGrouptoBoard');  //added by Athira on 06.03.2019
    
    
});
Route::group(['prefix' => 'columns'], function() {
    
    Route::post('newBoardColumn','boards\columncontroller@newBoardColumn' );
    
});



Route::post('/profile/createboard', 'ProfileController@createboard')->name('profile')->middleware('checkauth');

Route::group(['domain' => '{sub}.'.env('APP_URL'),'middleware' => [ 'appconfigs'],'prefix'=>'profile'], function ($appurl) 
{     
    Route::post('/inviteteam', 'ProfileController@inviteteam')->name('profile')->middleware('checkauth');
});  








Route::get('/changepassword', 'LoginController@changepassword')->name('changepassword');

Route::post('/postlogin', 'LoginController@postlogin')->name('postlogin');

Route::post('/postforgotpassword', 'LoginController@postforgotpassword')->name('postforgotpassword');

Route::post('/postresetpassword', 'LoginController@postresetpassword')->name('postresetpassword');



Route::get('/', function() {
    //
    $httphost=str_replace("www.","",$_SERVER['HTTP_HOST']) ;
    $subdomainname = Request::instance()->query('subdomainname');
   
    
    $subdomainhost= str_replace(env('APP_URL'),'',$httphost) ;
    $subdomainhostlength=strlen($subdomainhost);
    if($subdomainhostlength>0)
    {
        $subdomainname=substr($subdomainhost,0,intval($subdomainhostlength)-1);
    }
    
    $data["subdomainname"]=$subdomainname;

    return view("default")->with($data);
})->middleware(['appconfigs']);




//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'signup','middleware' => ['appconfigs']], function() {
    //
    Route::get('/','SignupController@index');  
    Route::get('/step1','SignupController@step1')->name('signup')->middleware('outsideauth','appconfigs');
    Route::get('/checkverification','SignupController@checkverificationmail')->name("checkverification");
    Route::get('/sendverification','SignupController@sendverificationmail');
    Route::post('/verifysignupemail','SignupController@verifysignupemail');
    Route::post('/checkaccounturl','SignupController@checkaccounturl');

    Route::get('/signup','SignupController@createuser')->middleware('outsideauth','appconfigs');
    Route::post('/postsignup','SignupController@createuserpost')->name('postsignup');

    Route::get('/step2','SignupController@step2')->middleware('outsideauth','appconfigs');
    Route::post('/poststep2','SignupController@poststep2')->name('poststep2');

    Route::get('/step3','SignupController@step3')->middleware('outsideauth','appconfigs');
    Route::post('/poststep3','SignupController@poststep3')->name('poststep3');

    Route::get('/step4','SignupController@step4')->middleware('outsideauth','appconfigs');
    Route::post('/poststep4','SignupController@poststep4')->name('poststep4');

    Route::get('/registerinvited/{returnlink}','SignupController@registerinvited');

    
    
});

Route::group(['prefix' => 'signup'], function() {
    Route::get('/profilecompletion','SignupController@profilecompletion');
});



/**---- Admin Control Panel started */
//Auth::routes();
Route::group(['prefix' => 'mondaymaster'], function() {
    Route::get('/login', ['as' =>'login', 'uses' =>'Auth\AdminLoginController@showLoginForm']);
    Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('mondaymaster.login');  
    Route::get('/logout', ['as'=>'logout','uses'=>'Auth\AdminLoginController@logout']);
    Route::post('/login','Auth\AdminLoginController@login')->name('login');    
    Route::get('/account','AdminController@account')->name('account');    
    
    // Registration Routes...
    Route::get('/mondaymaster/register', 'AdminRegisterController@showRegistrationForm')->name('register');
    Route::post('/mondaymaster/register', 'AdminRegisterController@register')->name('register');      
    // Route::get('LeftMenu', 'WidgetsController@LeftMenuforAdmin')->name('changepassword')->middleware('checkauth');
    Route::group(['middleware' => ['admin']], function () {
        Route::get('/dashboard', ['as'=>'masterDashboard','uses'=>'AdminRegisterController@showDashboard']);
        Route::get('/company', ['as'=>'companyList','uses'=>'AdminRegisterController@showCompany']); 
        Route::get('/Users/{id}', ['as'=>'usersList','uses'=>'AdminRegisterController@showUsers']);
        Route::get('/themeCategory', ['as'=>'themeCategory','uses'=>'AdminRegisterController@showthemeCategory']);
        Route::get('/themes/{id}',['as'=>'themeList','uses'=>'AdminRegisterController@showThemes']);    
        Route::get('/template/{catid}/{themeid}',['as'=>'themeLists','uses'=>'AdminRegisterController@showTemplate']); 

        Route::post('admin/newThemeColumn','AdminRegisterController@newThemeColumn')->name('boardtemplate');
        Route::post('admin/updateTemplateColumn','AdminRegisterController@updateTemplateColumn')->name('boardtemplate');
        Route::post('admin/updateTemplateRowValues','AdminRegisterController@updateTemplateColumnRowvalues')->name('boardtemplate');
    });
});
/**---Admin End */

